<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Admin
 *
 * @property int $id
 * @property string $name
 * @property string $mobile
 * @property string $email
 * @property string|null $phone
 * @property string $password
 * @property string $status
 * @property string|null $address
 * @property string|null $type
 * @property int $status_access
 * @property string|null $last_login
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Auction[] $auctions
 * @property-read int|null $auctions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Inquiry[] $inquiries
 * @property-read int|null $inquiries_count
 * @property-read \App\Models\Role $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tender[] $tenders
 * @property-read int|null $tenders_count
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin query()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLastLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereStatusAccess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereUpdatedAt($value)
 */
	class Admin extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AdminMessage
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminMessage whereUpdatedAt($value)
 */
	class AdminMessage extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Auction
 *
 * @property int $id
 * @property string $title
 * @property string $type
 * @property string|null $slug
 * @property string|null $city
 * @property string|null $address
 * @property string $auction_code
 * @property string $title_advertiser
 * @property string|null $source_adv
 * @property int $setad
 * @property string $invitation_date
 * @property string|null $view_deadline_date
 * @property string|null $request_deadline_date
 * @property string|null $winner_announced_date
 * @property string $initial_publish_date
 * @property string $publish_date
 * @property string $free_date
 * @property string|null $commodity_num
 * @property string|null $commodity_description
 * @property string|null $base_price
 * @property string|null $source_auction_num
 * @property string $body_adv
 * @property int $status_adv
 * @property string|null $img_url
 * @property string|null $link_url
 * @property string|null $asantender
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $central_organ_id
 * @property int $admin_id
 * @property-read \App\Models\Admin $admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CentralOrgan[] $centralorgan
 * @property-read int|null $centralorgan_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Province[] $provinces
 * @property-read int|null $provinces_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkGroup[] $workgroups
 * @property-read int|null $workgroups_count
 * @method static \Database\Factories\AuctionFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Auction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Auction query()
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereAsantender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereAuctionCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereBasePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereBodyAdv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereCentralOrganId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereCommodityDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereCommodityNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereFreeDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereImgUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereInitialPublishDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereInvitationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereLinkUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction wherePublishDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereRequestDeadlineDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereSetad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereSourceAdv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereSourceAuctionNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereStatusAdv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereTitleAdvertiser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereViewDeadlineDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auction whereWinnerAnnouncedDate($value)
 */
	class Auction extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Banner
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property string $link
 * @property string|null $image_file
 * @property int $click_count
 * @property string|null $start_date
 * @property string|null $expire_date
 * @property int $hasButton
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Banner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner query()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereClickCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereExpireDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereHasButton($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereImageFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereUpdatedAt($value)
 */
	class Banner extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Blog
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $keywords
 * @property string $priority
 * @property string $meta
 * @property string $description
 * @property string $body
 * @property string $admin_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\BlogFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Blog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Blog query()
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereMeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereUpdatedAt($value)
 */
	class Blog extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CentralOrgan
 *
 * @property int $id
 * @property string $name
 * @property int|null $parent_id
 * @property int $status
 * @property int $priority
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Auction $auction
 * @property-read \App\Models\Inquiry $inquiry
 * @property-read \App\Models\Tender $tender
 * @method static \Illuminate\Database\Eloquent\Builder|CentralOrgan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CentralOrgan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CentralOrgan query()
 * @method static \Illuminate\Database\Eloquent\Builder|CentralOrgan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CentralOrgan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CentralOrgan whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CentralOrgan whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CentralOrgan wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CentralOrgan whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CentralOrgan whereUpdatedAt($value)
 */
	class CentralOrgan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Config
 *
 * @property int $id
 * @property string $length_advertise_admin_show
 * @property string $pagination_advertise_admin
 * @property string $welcome_message_user_dashboard
 * @property string $welcome_message_admin_dashboard
 * @property string $url_header_user_site
 * @property string $url_header_user_dashboard
 * @property string $url_footer_user_site
 * @property string $url_footer_user_dashboard
 * @property string $initial_work_groups_change_user_subscribed
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Config newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Config newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Config query()
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereInitialWorkGroupsChangeUserSubscribed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereLengthAdvertiseAdminShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config wherePaginationAdvertiseAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereUrlFooterUserDashboard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereUrlFooterUserSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereUrlHeaderUserDashboard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereUrlHeaderUserSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereWelcomeMessageAdminDashboard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Config whereWelcomeMessageUserDashboard($value)
 */
	class Config extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ConfigAdmin
 *
 * @property int $id
 * @property int|null $paginate_show_admins
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigAdmin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigAdmin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigAdmin query()
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigAdmin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigAdmin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigAdmin wherePaginateShowAdmins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigAdmin whereUpdatedAt($value)
 */
	class ConfigAdmin extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ConfigUser
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigUser whereUpdatedAt($value)
 */
	class ConfigUser extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\DiscountCode
 *
 * @property int $id
 * @property string $name
 * @property string $serial
 * @property int $expire
 * @property int $count
 * @property int $percent
 * @property int $work_groups_changes
 * @property int $status
 * @property int $plan_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Plan $plan
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode whereExpire($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode whereSerial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscountCode whereWorkGroupsChanges($value)
 */
	class DiscountCode extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Inquiry
 *
 * @property int $id
 * @property string $title
 * @property string $type
 * @property string|null $slug
 * @property string|null $city
 * @property string|null $address
 * @property string $invitation_code
 * @property string $title_advertiser
 * @property string|null $source_adv
 * @property int $setad
 * @property string|null $link_url
 * @property string|null $img_url
 * @property string $body_adv
 * @property int $commodity_service
 * @property int $partial_medium
 * @property int $status_adv
 * @property string $free_date
 * @property string $invitation_date
 * @property string|null $submission_deadline_date
 * @property string|null $minimum_price_validity_date
 * @property string|null $requirement_date
 * @property string $initial_publish_date
 * @property string $publish_date
 * @property string|null $asantender
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $central_organ_id
 * @property int $admin_id
 * @property-read \App\Models\Admin $admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CentralOrgan[] $centralorgan
 * @property-read int|null $centralorgan_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Province[] $provinces
 * @property-read int|null $provinces_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkGroup[] $workgroups
 * @property-read int|null $workgroups_count
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry query()
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereAsantender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereBodyAdv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereCentralOrganId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereCommodityService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereFreeDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereImgUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereInitialPublishDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereInvitationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereInvitationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereLinkUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereMinimumPriceValidityDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry wherePartialMedium($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry wherePublishDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereRequirementDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereSetad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereSourceAdv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereStatusAdv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereSubmissionDeadlineDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereTitleAdvertiser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inquiry whereUpdatedAt($value)
 */
	class Inquiry extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Order
 *
 * @property int $id
 * @property string $title
 * @property string $price
 * @property string|null $description
 * @property int $has_discount
 * @property int $status
 * @property int $user_id
 * @property int $plan_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Plan $plan
 * @property-read Order $user
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereHasDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUserId($value)
 */
	class Order extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PaymentLog
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentLog whereUpdatedAt($value)
 */
	class PaymentLog extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Permission
 *
 * @property int $id
 * @property int $read
 * @property int $write
 * @property int $update
 * @property int $delete
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereUpdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereWrite($value)
 */
	class Permission extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Plan
 *
 * @property int $id
 * @property string $title
 * @property int $priority
 * @property int $price
 * @property int $status
 * @property int|null $off_percent
 * @property int $work_group_limit
 * @property int $work_group_change_limit
 * @property int|null $count
 * @property int|null $time
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DiscountCode[] $discountcodes
 * @property-read int|null $discountcodes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Plan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Plan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Plan query()
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereOffPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereWorkGroupChangeLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereWorkGroupLimit($value)
 */
	class Plan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Post
 *
 * @property int $id
 * @property string $meta_tag_description
 * @property string $meta_tag_keywords
 * @property string $title_tag
 * @property string $author
 * @property string $title
 * @property string $division
 * @property string $body
 * @property int $views
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDivision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereMetaTagDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereMetaTagKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTitleTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereViews($value)
 */
	class Post extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Province
 *
 * @property int $id
 * @property string $name
 * @property string|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Auction[] $auctions
 * @property-read int|null $auctions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Inquiry[] $inquiries
 * @property-read int|null $inquiries_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tender[] $tenders
 * @property-read int|null $tenders_count
 * @method static \Database\Factories\ProvinceFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Province newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Province newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Province query()
 * @method static \Illuminate\Database\Eloquent\Builder|Province whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Province whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Province whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Province whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Province whereUpdatedAt($value)
 */
	class Province extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string $label
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Admin[] $admins
 * @property-read int|null $admins_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SiteLog
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SiteLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SiteLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SiteLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|SiteLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteLog whereUpdatedAt($value)
 */
	class SiteLog extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Tender
 *
 * @property int $id
 * @property string $title
 * @property string $type
 * @property string|null $slug
 * @property string|null $city
 * @property string|null $address
 * @property string $invitation_code
 * @property string $title_advertiser
 * @property string|null $source_adv
 * @property int $setad
 * @property string $invitation_date
 * @property string|null $document_deadline_date
 * @property string|null $request_deadline_date
 * @property string|null $winner_announced_date
 * @property string $initial_publish_date
 * @property string $publish_date
 * @property string $free_date
 * @property string $body_adv
 * @property int $status_adv
 * @property string|null $img_url
 * @property string|null $asantender
 * @property string|null $link_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $central_organ_id
 * @property int $admin_id
 * @property-read \App\Models\Admin $admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CentralOrgan[] $centralorgan
 * @property-read int|null $centralorgan_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Province[] $provinces
 * @property-read int|null $provinces_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkGroup[] $workgroups
 * @property-read int|null $workgroups_count
 * @method static \Illuminate\Database\Eloquent\Builder|Tender newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tender newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tender query()
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereAsantender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereBodyAdv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereCentralOrganId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereDocumentDeadlineDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereFreeDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereImgUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereInitialPublishDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereInvitationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereInvitationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereLinkUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender wherePublishDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereRequestDeadlineDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereSetad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereSourceAdv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereStatusAdv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereTitleAdvertiser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tender whereWinnerAnnouncedDate($value)
 */
	class Tender extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Ticket
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string|null $division
 * @property string $body
 * @property string|null $answer
 * @property int $status
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket query()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereDivision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereUserId($value)
 */
	class Ticket extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $mobile
 * @property string|null $email
 * @property string|null $city
 * @property string|null $address
 * @property string $type
 * @property string|null $company_name
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $mobile_verified_at
 * @property string|null $subscription_date
 * @property string|null $subscription_title
 * @property int $subscription_count
 * @property int $work_groups_changes
 * @property int $count_adv
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Plan[] $plans
 * @property-read int|null $plans_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ticket[] $tickets
 * @property-read int|null $tickets_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkGroup[] $workgroups
 * @property-read int|null $workgroups_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCountAdv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereMobileVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSubscriptionCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSubscriptionDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSubscriptionTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereWorkGroupsChanges($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Visit
 *
 * @property int $id
 * @property int $site
 * @property int $posts
 * @property int $plans
 * @property int $auction
 * @property int $tender
 * @property int $inquiry
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Visit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Visit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Visit query()
 * @method static \Illuminate\Database\Eloquent\Builder|Visit whereAuction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Visit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Visit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Visit whereInquiry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Visit wherePlans($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Visit wherePosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Visit whereSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Visit whereTender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Visit whereUpdatedAt($value)
 */
	class Visit extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\WorkGroup
 *
 * @property int $id
 * @property string $title
 * @property string|null $image
 * @property string $type
 * @property int $status
 * @property string $priority
 * @property string|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Auction[] $auctions
 * @property-read int|null $auctions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Inquiry[] $inquiries
 * @property-read int|null $inquiries_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tender[] $tenders
 * @property-read int|null $tenders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|WorkGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WorkGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WorkGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|WorkGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkGroup whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkGroup whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkGroup wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkGroup whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkGroup whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkGroup whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkGroup whereUpdatedAt($value)
 */
	class WorkGroup extends \Eloquent {}
}

