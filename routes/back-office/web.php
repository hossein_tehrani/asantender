<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', \App\Http\Livewire\Auth\AdminLogin::class)->name('login');

Route::middleware(['auth:admin'])->group(function () {
    Route::get('/', \App\Http\Livewire\Index::class);
    Route::get('/create-tender',\App\Http\Livewire\CreateTender::class);
    Route::get('/writer',\App\Http\Livewire\Writer::class);
    Route::get('/post/{slug}',\App\Http\Livewire\SinglePost::class);
    Route::get('/create-auction',\App\Http\Livewire\CreateAuction::class);
    Route::get('/create-inquiry',\App\Http\Livewire\CreateInquiry::class);
    Route::get('/details', \App\Http\Livewire\Details::class);
    Route::get('/register', \App\Http\Livewire\Auth\AdminRegister::class);
    Route::get('/plan', \App\Http\Livewire\Plan\CreatePlan::class);
    Route::get('/discounts', \App\Http\Livewire\Discount\CreateDiscount::class);
    Route::get('/update/{admin}', \App\Http\Livewire\Auth\AdminUpdate::class)->name('AdminUpdate');
    Route::get('/register-role', \App\Http\Livewire\CreateRole::class)->middleware('can:create,App\Models\Role');
    Route::get('/edit-permission', \App\Http\Livewire\EditPermissions::class);
    Route::get('/users', \App\Http\Livewire\User\Users::class);
    Route::get('/users/user/{userId}', \App\Http\Livewire\User\User::class)->name('UserProfile');
    Route::get('/create-work-group',\App\Http\Livewire\CreateWorkGroup::class);
    Route::get('/create-central-organ',\App\Http\Livewire\CreateCentralOrgan::class);
});


