<?php

use App\Http\Controllers\api\admin\AdminController;
use App\Http\Controllers\Api\Admin\Advertiser\AuctionController;
use App\Http\Controllers\Api\Admin\Advertiser\InquiryController;
use App\Http\Controllers\Api\Admin\Advertiser\TenderController;
use App\Http\Controllers\Api\Admin\Plan\PlanController;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Dashboard\DashboardController;
use App\Http\Controllers\Api\Site\SiteController;
use App\Http\Controllers\Api\Site\SearchController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', [SiteController::class, 'index'])->name('Index')->middleware('cors');
Route::get('blog',\App\Http\Livewire\Blog::class);
Route::post('initial-auth', [AuthController::class, 'initialAuth']);
Route::post('check-mobile', [AuthController::class, 'checkMobile']);
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::get('plans', [PlanController::class, 'index']);
Route::get('reject-order', [PlanController::class, 'reject_order']);
Route::get('success-order', [PlanController::class, 'success_order']);
Route::get('adv/single/{type}/{slug}', [SiteController ::class, 'show_single_adv']);
Route::post('search', [SearchController::class, 'search']);
Route::get('work-groups', [SiteController::class, 'getWorkGroups']);
Route::get('adv-by-wg/{id}', [SiteController::class, 'advByWorkGroups']);

Route::group(['middleware' => 'auth.api'], function () {


    Route::get('logout', [AuthController::class, 'logout']);
    Route::get('get-user', [AuthController::class, 'getUser']);

    Route::get('update-user', [AuthController::class, 'updateUser']);

    Route::prefix('plans')->group(function () {
        Route::post('check-discount', [PlanController::class, 'checkDiscountCode']);
        Route::post('buy', [PlanController::class, 'buy']);
    });


    Route::prefix('dashboard')->group(function () {
        Route::get('', [DashboardController ::class, 'index']);
        Route::post('search', [DashboardController::class, 'search']);
        Route::post('select', [DashboardController::class, 'select_fav']);
        Route::get('adv/single/{type}/{slug}', [DashboardController ::class, 'show_single_adv']);
        Route::get('update/{id}', [DashboardController::class, 'getUpdate']);
        Route::get('revert', [PlanController::class, 'revert']);
        Route::post('update', [DashboardController::class, 'update']);
    });

    Route::post('choose-work-groups', [DashboardController::class, 'choose_work_groups']);
});


//Route::post('auction-search',[SearchController::class,'auctionSearch']);
//Route::get('tender-search',[SearchController::class,'tenderSearch']);
//Route::get('inquiry-search',[SearchController::class,'inquirySearch']);

Route::prefix('advertise')->group(function () {
    Route::get('tender/{id}', [TenderController ::class, 'single_adv']);
    Route::get('auction/{id}', [AuctionController::class, 'single_adv']);
    Route::post('inquiry/{id}', [InquiryController::class, 'single_adv']);
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
