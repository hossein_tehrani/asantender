<div style="overflow-x: hidden;">
    <div class="container-fluid">
        @livewire('admin-header')
    </div>
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>
    <div>
        <div class="container">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">شماره</th>
                    <th scope="col">نام دسترسی</th>
                    <th scope="col">شرح دسترسی</th>
                </tr>
                </thead>
                <tbody>
                @foreach($allPermissions as $permission)
                    <tr>
                        <th scope="row">{{$permission->id}}</th>
                        <td>{{$permission->name}}</td>
                        <td>{{$permission->label}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

