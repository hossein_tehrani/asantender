<div>
    <div class="container-fluid">
        @livewire('admin-header')
    </div>
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label for="title">عنوان طرح</label>
                    <input type="text" class="form-control @error('plan.title') is-invalid @enderror " id="title"
                           wire:model="plan.title">
                    @error('plan.title')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
            </div>
            <div class="col-1">
                <div class="form-group">
                    <label for="priority">اولویت</label>
                    <input type="number" class="form-control @error('plan.priority') is-invalid @enderror" id="priority"
                           wire:model="plan.priority">
                    @error('plan.priority')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="work_group_limit">محدودیت گروه کاری</label>
                    <input type="number" class="form-control @error('plan.work_group_limit') is-invalid @enderror"
                           id="work_group_limit" wire:model="plan.work_group_limit">
                    @error('plan.work_group_limit')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="work_group_change_limit">محدودیت تغییر گروه کاری</label>
                    <input type="number"
                           class="form-control @error('plan.work_group_change_limit') is-invalid @enderror"
                           id="work_group_change_limit"
                           wire:model="plan.work_group_change_limit">
                    @error('plan.work_group_change_limit')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
            </div>
            <div class="col-1">
                <div class="form-group">
                    <label for="count">تعداد</label>
                    <input type="number" class="form-control @error('plan.count') is-invalid @enderror" id="count"
                           wire:model="plan.count">
                    @error('plan.count')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
            </div>
            <div class="col-1">
                <div class="form-group">
                    <label for="time">زمان</label>
                    <input type="number" class="form-control @error('plan.time') is-invalid @enderror" id="time"
                           wire:model="plan.time">
                    @error('plan.time')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-2">
                <div class="form-group">
                    <label for="price">قیمت</label>
                    <input type="number" class="form-control @error('plan.price') is-invalid @enderror" id="price"
                           wire:model="plan.price">
                    @error('plan.price')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
            </div>
            <div class="col-1">
                <div class="form-group">
                    <label for="offPercent">درصد تخفیف</label>
                    <input type="number" class="form-control @error('plan.off_percent') is-invalid @enderror"
                           id="offPercent" wire:model="plan.off_percent">
                    @error('plan.off_percent')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
            </div>

            <div class="col-1">
                <div class="form-group mt-4">
                    <label for="status">وضعیت</label>
                    <input type="checkbox" id="status" wire:model="plan.status">
                </div>
            </div>
            <div class="col-3 m-4">
                <button wire:click="storePlan" class="btn btn-outline-success">
                    دخیره طرح
                </button>
            </div>
        </div>
    </div>
    <hr>
    <div class="row justify-content-center">
        <div class="col-2">
            <div class="form-group">
                <label for="exampleInputEmail1">عبارت مورد نظر را تایپ کنید</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                       wire:model="search.words">
            </div>
        </div>
        <div class="col-1">
            <label for="number">تعداد رکورد</label>
            <input type="text" id="number" class="form-control" wire:model="search.number">
        </div>
        <div class="col-2 mt-2">
            <label for="orderBy">دسته بندی بر اساس</label>
            <select id="orderBy" wire:model="search.orderBy">
                <option value="price">قیمت</option>
                <option value="off_percent">درصد تخفیف</option>
                <option value="priority" >اولویت</option>
                <option value="work_group_limit">محدودیت گروه کاری</option>
                <option value="work_group_change_limit">محدودیت تغییر گروه کاری</option>
                <option value="count">تعداد</option>
                <option value="time">زمان</option>
            </select>
        </div>
        <div class="col-3 mt-2">
            <label for="status2">دسته بندی بر اساس وضعیت پلن</label>
            <select id="status2" wire:model="search.active">
                <option value="all">نمایش تمام پلن ها</option>
                <option value="deactive">نمایش تمام پلن های غیر فعال</option>
                <option value="active" >نمایش تمام پلن های فعال</option>
            </select>
        </div>
        <div class="col-1 mt-4">
            <button class="btn btn-outline-dark" wire:click="search">جستجو</button>
        </div>
    </div>
    <hr>
    <div class="row">
        <div>
            @if($search['plans'])
                <div class="container-fluid">
                    <table class="table table-warning">
                        <thead>
                        <tr>
                            <th scope="col">نتایج شماره</th>
                            <th scope="col">عنوان طرح</th>
                            <th scope="col">اولویت</th>
                            <th scope="col">قیمت</th>
                            <th scope="col">درصد تخفیف</th>
                            <th scope="col">محدودیت گروه کاری</th>
                            <th scope="col">محدودیت تغییر گروه کاری</th>
                            <th scope="col">تعداد</th>
                            <th scope="col">زمان</th>
                            <th scope="col">وضعیت</th>
                            <th scope="col">ابذار</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($search['plans']['data'] as $item)
                            <tr>
                                <th scope="row">{{$i++}}</th>
                                <th scope="row">{{$item['title']}}</th>
                                <th scope="row">{{$item['priority']}}</th>
                                <th scope="row">{{$item['price']}}تومان</th>
                                <th scope="row">{{$item['off_percent']}}٪</th>
                                <th scope="row">{{$item['work_group_limit']}}</th>
                                <th scope="row">{{$item['work_group_change_limit']}}</th>
                                <th scope="row">{{$item['count']}}-آگهی</th>
                                <th scope="row">{{$item['time']}}روز</th>
                                @if($item['status'] == 1)
                                    <th scope="row">فعال</th>
                                @else
                                    <th scope="row">غیر فعال</th>
                                @endif
                                <th scope="row">
                                    @if($item['status'] == 1 )
                                        <button class="btn btn-outline-warning"
                                                wire:click="togglePlan({{$item['id']}},'Deactive')">
                                            غیر
                                            فعال
                                        </button>
                                    @else
                                        <button class="btn btn-outline-success"
                                                wire:click="togglePlan({{$item['id']}},'Active')">فعال
                                        </button>
                                    @endif
                                    <button class="btn btn-outline-danger" wire:click="delete({{$item['id']}})">حذف
                                    </button>
                                </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>
    <hr>
    <div class="container-fluid">
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">عنوان طرح</th>
                <th scope="col">اولویت</th>
                <th scope="col">قیمت</th>
                <th scope="col">درصد تخفیف</th>
                <th scope="col">محدودیت گروه کاری</th>
                <th scope="col">محدودیت تغییر گروه کاری</th>
                <th scope="col">تعداد</th>
                <th scope="col">زمان</th>
                <th scope="col">وضعیت</th>
                <th scope="col">ابذار</th>
            </tr>
            </thead>
            <tbody>
            @foreach($plans as $item)
                <tr>
                    <th scope="row">{{$i++}}</th>
                    <th scope="row">{{$item->title}}</th>
                    <th scope="row">{{$item->priority}}</th>
                    <th scope="row">{{$item->price}}تومان</th>
                    <th scope="row">{{$item->off_percent}}٪</th>
                    <th scope="row">{{$item->work_group_limit}}</th>
                    <th scope="row">{{$item->work_group_change_limit}}</th>
                    <th scope="row">{{$item->count}}-آگهی</th>
                    <th scope="row">{{$item->time}}روز</th>
                    @if($item->status == 1)
                        <th scope="row">فعال</th>
                    @else
                        <th scope="row">غیر فعال</th>
                    @endif
                    <th scope="row">
                        @if($item->status == 1 )
                            <button class="btn btn-outline-warning" wire:click="togglePlan({{$item->id}},'Deactive')">
                                غیر
                                فعال
                            </button>
                        @else
                            <button class="btn btn-outline-success" wire:click="togglePlan({{$item->id}},'Active')">فعال
                            </button>
                        @endif
                        <button class="btn btn-outline-danger" wire:click="delete({{$item->id}})">حذف</button>
                    </th>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$plans->links()}}
    </div>
</div>

