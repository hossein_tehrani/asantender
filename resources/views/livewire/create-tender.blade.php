<div>
    @livewire('admin-header')
    <div class="row">
        <div id="messages">
            <div id="successMessages">@if (session()->has('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
            <div id="errorMessages">@if (session()->has('error'))
                    <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
            <div id="messages">@if (session()->has('message'))
                    <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
        </div>
        <div class="form-group col-lg-2" id="tender-title">
            <label for="title">عنوان مناقصه</label>
            <input type="text" id="title" class="form-control col-lg-2 @error('title') is-invalid @enderror"
                   wire:model="title">
            @error('title')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-2" wire:ignore>
            <label for="province">استان</label>
            <select type="text" name="province" id="province" class="form-control col-lg-2" multiple autocomplete="off"
            >
                <option value="all">تمامی استان ها</option>
                @foreach($allProvinces as $province)
                    <option value="{{$province->id}}">{{$province->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-lg-2">
            <label for="tender-city">شهر</label>
            <input type="text" name="city" id="tender-city"
                   class="form-control col-lg-2 @error('city') is-invalid @enderror" wire:model="city">
            @error('city')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group col-lg-2">
            <label for="tender-address">آدرس</label>
            <input type="text" name="address" id="tender-address"
                   class="form-control col-lg-2 @error('address') is-invalid @enderror" wire:model="address">
            @error('address')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group col-lg-2">
            <label for="invitation_code">کد فراخوان</label>
            <input type="text" id="invitation_code" name="invitation_code"
                   class="form-control col-lg-2 @error('invitation_code') is-invalid @enderror"
                   wire:model="invitation_code">
            @error('invitation_code')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-2">
            <label for="title_advertiser">عنوان آگهی گذار</label>
            <input type="text" name="title_advertiser" id="title_advertiser"
                   class="form-control col-lg-2 @error('title_advertiser') is-invalid @enderror"
                   wire:model="title_advertiser">
            @error('title_advertiser')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-2">
            <label for="source_adv">منبع آگهی</label>
            <input type="text" class="form-control col-lg-2  @error('source_adv') is-invalid @enderror"
                   name="source_adv" id="source_adv" wire:model="source_adv">
            @error('source_adv')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-2" wire:ignore>
            <label for="invitation_date">تاریخ فراخوان</label>
            <input type="text" id="invitation_date" name="invitation_date"
                   class="form-control col-lg-2 @error('invitation_date') is-invalid @enderror" autocomplete="off">
            @error('invitation_date')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-2" wire:ignore>
            <label for="document_deadline_date">تاریخ مهلت دریافت اسناد</label>
            <input type="text" id="document_deadline_date" name="document_deadline_date" class="form-control col-lg-2"
                   autocomplete="off">
        </div>
        <div class="form-group col-lg-2" wire:ignore>
            <label for="request_deadline_date">تاریخ مهلت ارسال پیشنهاد</label>
            <input type="text" id="request_deadline_date" name="request_deadline_date" class="form-control col-lg-2"
                   autocomplete="off">
        </div>
        <div class="form-group col-lg-2" wire:ignore>
            <label for="winner_announced_date">تاریخ اعلام برنده</label>
            <input type="text" id="winner_announced_date" name="winner_announced_date" class="form-control col-lg-2"
                   autocomplete="off">
        </div>
        <div class="form-group col-lg-2" wire:ignore>
            <label for="initial_publish_date">تاریخ ثبت در آسان تندر</label>
            <input type="text" id="initial_publish_date" name="initial_publish_date" class="form-control col-lg-2"
                   autocomplete="off">
        </div>
        <div class="form-group col-lg-2" wire:ignore>
            <label for="publish_date">تاریخ انتشار</label>
            <input type="text" id="publish_date" name="publish_date" class="form-control col-lg-2" autocomplete="off">
        </div>
        <div class="form-group col-lg-2" wire:ignore>
            <label for="free_date">تاریخ رایگان</label>
            <input type="text" id="free_date" name="free_date" class="form-control col-lg-2" autocomplete="off">
        </div>
        <div class="form-group col-lg-2">
            <label for="img_url"> عکس آگهی</label>
            <input type="file" id="img_url" name="img_url"
                   class="form-control col-lg-2 @error('img_url') is-invalid @enderror" wire:model="img_url"
                   autocomplete="off">
            @error('img_url')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-2">
            <label for="link_url">آدرس لینک آگهی</label>
            <input type="text" id="link_url" name="link_url"
                   class="form-control col-lg-2 @error('link_url') is-invalid @enderror" wire:model="link_url">
            @error('link_url')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-2">
            <label for="central_organ_id">دستگاه مرکزی</label>
            <select name="central_organ_id" id="central_organ_id" class="form-control col-lg-2"
                    wire:model="central_organ_id">
                @foreach($central_organs as $central_organ)
                    <option value="{{$central_organ->id}}">{{$central_organ->name}}</option>
                @endforeach
            </select>
        </div>
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary col-lg-3" data-toggle="modal" data-target="#exampleModalCenter">
            گروه های کاری
        </button>
        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered container-fluid" role="document">
                <div class="modal-content col-lg-12">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">گروه های کاری</h5>
                        <button type="button" class="btn btn-primary close" data-dismiss="modal" aria-label="Close">
                            &times;
                        </button>
                    </div>
                    <div class="modal-body row">
                        @foreach($work_groups['root'] as $work_group)
                            @if(isset($work_groups[$work_group->id]))
                                <div class="col-lg-4">
                                    <a class="btn btn-primary" data-toggle="collapse" href="#wg{{$work_group->id}}"
                                       role="button" aria-expanded="false" aria-controls="collapseExample">
                                        {{$work_group->title}}
                                    </a>
                                    <div class="collapse" id="wg{{$work_group->id}}">
                                        <div class="card card-body">
                                            @foreach($work_groups[$work_group->id] as $child)
                                                <label for="child{{$child->id}}">{{$child->title}}</label>
                                                <input type="checkbox" id="child{{$child->id}}" value="{{$child->id}}"
                                                       wire:model.defer="workGroups">
                                            @endforeach
                                        </div>
                                    </div>
                                    <br>
                                </div>
                                <br><br>
                            @endif
                        @endforeach
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">بستن</button>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-lg-2">
            <label class="form-check-label" for="setad">ستادی یا غیر ستادی</label>
            <input type="checkbox" name="setad" class="form-check-input col-lg-5" wire:model="setad">
        </div>
        <div class="form-group col-lg-3">
            <label class="form-check-label" for="status_adv">وضعیت اگهی</label>
            <input type="checkbox" name="status_adv" id="status_adv" class="form-check-input col-lg-5"
                   wire:model="status_adv">
        </div>
    </div>
    <div class="row">
        <div class="form-group col-lg-5">
            <label for="body_adv">شرح آگهی</label>
            <textarea name="body_adv" id="body_adv" rows="8"
                      class="form-control col-lg-6 mt-3 p-2 @error('body_adv') is-invalid @enderror"
                      wire:model="body_adv"></textarea>
            @error('body_adv')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-2">
            <button class="btn btn-danger" wire:click="ResetFilter">پاک کردن فیلد ها</button>
        </div>
        <div class="form-group col-lg-1">
            <button class="btn btn-primary" wire:click.prevent="StoreTender">ثبت</button>
        </div>
        <div class="file-group col-lg-3">
            <input type="file" class="btn btn-outline-success" wire:model="import_excel">
            <button class="btn btn-outline-success" id="customFileLang" wire:click.prevent="StoreExcel">ارسال با اکسل
            </button>
        </div>
        <div class="form-group col-lg-1">
            <button class="btn btn-success" wire:click="ExportExcel">دریافت اکسل</button>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="row">
            <div class="col-2">
                <div class="form-group">
                    <label for="exampleInputEmail1">عبارت مورد نظر را تایپ کنید</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                           wire:model="search.words">
                </div>
            </div>
            <div class="col-1">
                <label for="number">تعداد رکورد</label>
                <input type="text" id="number" class="form-control" wire:model="search.number">
            </div>
            <div class="col-2">
                <label for="orderBy">دسته بندی بر اساس</label>
                <select id="orderBy" wire:model="search.orderBy">
                    <option value="setad">ستادی</option>
                    <option value="invitation_date">تاریخ فراخوان</option>
                    <option value="document_deadline_date">تاریخ مهلت دریافت اسناد</option>
                    <option value="request_deadline_date">تاریخ مهلت ارسال پیشنهاد</option>
                    <option value="winner_announced_date">تاریخ اعلام برنده</option>
                    <option value="initial_publish_date">تاریخ ثبت در آسان تندر</option>
                    <option value="publish_date">تاریخ انتشار</option>
                    <option value="free_date">تاریخ رایگان</option>
                </select>
            </div>
            <div class="col-2" wire:ignore>
                <label for="status3">استان</label>
                <select id="status3" multiple>
                    <option value="{{0}}"> تمام استان ها</option>
                    @foreach($allProvinces as $item)
                        <option value="{{$item->id}}">
                            {{$item->name}}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="col-2">
                <label for="status2">فیلتر بر اساس وضعیت</label>
                <select id="status2" wire:model="search.active">
                    <option value="{{ 2 }}">تمامی آگهی ها</option>
                    <option value="{{ 1 }}">منتشر شده</option>
                    <option value="{{ 0 }}">در انتظار انتشار</option>
                </select>
            </div>
            <div class="col-2">
                <label for="timeLimit">فیلتر بر اساس بازه زمانی</label>
                <select id="timeLimit" wire:model="search.limit_time_type">
                    <option value="invitation_date">تاریخ فراخوان</option>
                    <option value="document_deadline_date">تاریخ مهلت دریافت اسناد</option>
                    <option value="request_deadline_date">تاریخ مهلت ارسال پیشنهاد</option>
                    <option value="winner_announced_date">تاریخ اعلام برنده</option>
                    <option value="initial_publish_date">تاریخ ثبت در آسان تندر</option>
                    <option value="publish_date">تاریخ انتشار</option>
                    <option value="free_date">تاریخ رایگان</option>
                </select>
            </div>
            <div class="col-2 mt-2" wire:ignore>
                <label for="start_limit">شروع</label>
                <input id="start_limit" type="text">
            </div>
            <div class="col-2 mt-2" wire:ignore>
                <label for="end_limit">پایان</label>
                <input id=end_limit type="text">
            </div>
            <div class="col-1 mt-4">
                <button class="btn btn-outline-dark" wire:click="search('search')">جستجو</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div>
            @if($search['tenders'] != null)
                <div class="container-fluid">
                    <table class="table table-warning">
                        <thead>
                        <tr>
                            <th scope="col">شناسه</th>
                            <th scope="col">کد آسان تندر</th>
                            <th scope="col">کد فراخوان</th>
                            <th scope="col">عنوان آگهی</th>
                            <th scope="col">تاریخ انتشار</th>
                            <th scope="col">آگهی گذار</th>
                            <th scope="col">وضعیت انتشار</th>
                            <th scope="col">ابزار</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($search['tenders']['data'] as $item)
                            <tr>
                                <th scope="row">{{$i++}}</th>
                                <th scope="row">{{$item['asantender']}}</th>
                                <th scope="row">{{$item['invitation_code']}}</th>
                                <td>{{\Illuminate\Support\Str::limit($item['title'],$config->length_advertise_admin_show)}}</td>
                                <td>{{\Morilog\Jalali\Jalalian::forge($item['publish_date'])->format('%B %d، %Y')}}</td>
                                <td>{{\Illuminate\Support\Str::limit($item['title_advertiser'],$config->length_advertise_admin_show)}}</td>
                                <td>{{$item['status_adv']}}</td>
                                @if($item['status_adv'] == 1)
                                    <th scope="row">فعال</th>
                                @else
                                    <th scope="row">غیر فعال</th>
                                @endif
                                <th scope="row">
                                    @if($item['status_adv'] == 1)
                                        <button class="btn btn-outline-warning"
                                                wire:click="toggleTender({{$item['id']}},'Deactive')">
                                            غیر
                                            فعال
                                        </button>
                                    @else
                                        <button class="btn btn-outline-success"
                                                wire:click="toggleTender({{$item['id']}},'Active')">فعال
                                        </button>
                                    @endif
                                    <button class="btn btn-outline-danger" wire:click="delete({{$item['id']}})">حذف
                                    </button>
                                </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>
    <div class="container-fluid">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">شناسه</th>
                <th scope="col">کد آسان تندر</th>
                <th scope="col">کد فراخوان</th>
                <th scope="col">عنوان آگهی</th>
                <th scope="col">تاریخ انتشار</th>
                <th scope="col">آگهی گذار</th>
                <th scope="col">وضعیت انتشار</th>
                <th scope="col">ابزار</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tenders as $tender)
                <tr>
                    <th scope="row">{{$tender->id}}</th>
                    <td>{{$tender->asantender}}</td>
                    <td>{{$tender->invitation_code}}</td>
                    <td>{{\Illuminate\Support\Str::limit($tender->title,$config->length_advertise_admin_show)}}</td>
                    <td>{{\Morilog\Jalali\Jalalian::forge($tender->publish_date)->format('%B %d، %Y')}}</td>
                    <td>{{\Illuminate\Support\Str::limit($tender->title_advertiser,$config->length_advertise_admin_show)}}</td>
                    @if($tender->status_adv == 0)
                        <td>غیر فعال</td>
                    @else
                        <td> فعال</td>
                    @endif
                    <td>
                        @if($tender->status_adv == 0)
                            <button class="btn btn-outline-success"
                                    wire:click="toggleTender({{$tender->id}},'Active')">فعال
                            </button>
                        @else
                            <button class="btn btn-outline-warning"
                                    wire:click="toggleTender({{$tender->id}},'Deactive')">
                                غیر
                                فعال
                            </button>
                        @endif
                        <button class="btn btn-outline-danger" wire:click="delete({{$tender['id']}})">حذف
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @push('scripts')
        <script>
            $(document).ready(function () {
                kamaDatepicker('document_deadline_date');
                kamaDatepicker('request_deadline_date');
                kamaDatepicker('winner_announced_date');
                kamaDatepicker('initial_publish_date');
                kamaDatepicker('publish_date');
                kamaDatepicker('free_date');
                kamaDatepicker('invitation_date');
                kamaDatepicker('start_limit');
                kamaDatepicker('end_limit');

                $('#start_limit').on('change', function () {
                @this.search_limit_time_start
                    = $(this).val();
                })
                $('#end_limit').on('change', function () {
                @this.search_limit_time_end
                    = $(this).val();
                })
                $('#invitation_date').on('change', function () {
                @this.invitation_date
                    = $(this).val();
                })
                $('#document_deadline_date').on('change', function () {
                @this.document_deadline_date
                    = $(this).val();
                })
                $('#request_deadline_date').on('change', function () {
                @this.request_deadline_date
                    = $(this).val();
                })
                $('#winner_announced_date').on('change', function () {
                @this.winner_announced_date
                    = $(this).val();
                })
                $('#initial_publish_date').on('change', function () {
                @this.initial_publish_date
                    = $(this).val();
                })
                $('#publish_date').on('change', function () {
                @this.publish_date
                    = $(this).val();
                })
                $('#free_date').on('change', function () {
                @this.free_date
                    = $(this).val();
                })
                $('#province').select2();
                $('#province').on('change', function () {
                @this.provinces
                    = $(this).val();
                })
                $('#status3').select2();
                $("#free_date").attr("autocomplete", "off");
                $('#status3').on('change', function () {
                @this.selected_provinces
                    = $(this).val();
                })
            });

        </script>
    @endpush
</div>

