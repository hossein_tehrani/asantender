<div>
    <div class="container-fluid">
        @livewire('admin-header')
    </div>
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>

    <div class="row">
        <div class="form-group col-lg-2">
            <label for="name">تیتر</label>
            <input type="text" name="name" id="name"
                   class="form-control col-lg-2 @error('discount.name') is-invalid @enderror"
                   wire:model="discount.name">
            @error('discount.name')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-2">
            <label for="name">سریال تخفیف</label>
            <input type="text" name="serial" id="serial"
                   class="form-control col-lg-2 @error('discount.serial') is-invalid @enderror"
                   wire:model="discount.serial">
            @error('discount.serial')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-1">
            <label for="expire">تاریخ انقضا+</label>
            <input type="text" name="expire" id="expire"
                   class="form-control col-lg-2 @error('discount.expire') is-invalid @enderror"
                   wire:model="discount.expire">
            @error('discount.expire')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-1">
            <label for="count">تعدادبازدید+</label>
            <input type="text" name="count" id="count"
                   class="form-control col-lg-2 @error('discount.count') is-invalid @enderror"
                   wire:model="discount.count">
            @error('discount.count')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-1">
            <label for="percent">درصد تخفیف</label>
            <input type="text" name="percent" id="percent"
                   class="form-control col-lg-2 @error('discount.percent') is-invalid @enderror"
                   wire:model="discount.percent">
            @error('discount.percent')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-1">
            <label for="work_groups_changes">تعداد بازدید +</label>
            <input type="text" name="work_groups_changes" id="work_groups_changes"
                   class="form-control col-lg-2 @error('discount.work_groups_changes') is-invalid @enderror"
                   wire:model="discount.work_groups_changes">
            @error('discount.work_groups_changes')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group col-lg-1">
            <label for="status">فعال</label>
            <input type="checkbox" name="status" id="status"
                   class="col-lg-2" wire:model="discount.status">
        </div>
        <div class="form-group col-lg-3">
            <label for="plan_id">plan_id</label>
            <select type="checkbox" name="plan_id" id="plan_id"
                    class="col-lg-2" wire:model="discount.plan_id">
                @foreach($plans as $item)
                    <option value="{{$item['id']}}">{{$item['title']}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-lg-1">
            <button class="btn btn-outline-success" wire:click="store">
                ثبت
            </button>
        </div>

    </div>


    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">عنوان کد تخفیف</th>
            <th scope="col">سریال</th>
            <th scope="col">اعتبار تا</th>
            <th scope="col">تعداد بازدید +</th>
            <th scope="col">درصد تخفیف</th>
            <th scope="col">محدودیت تغییر گروه کاری +</th>
            <th scope="col">وضعیت</th>
            <th scope="col">مربوط به</th>
            <th scope="col">ابذار</th>
        </tr>
        </thead>
        <tbody>
        @foreach($discounts as $item)
            <tr>
                <th scope="row">{{$i++}}</th>
                <th scope="row">{{$item->name}}</th>
                <th scope="row">{{$item->serial}}</th>
                <th scope="row">{{$item->expire}}روز دیگر</th>
                <th scope="row">{{$item->count}}آگهی</th>
                <th scope="row">{{$item->percent}}٪</th>
                <th scope="row">{{$item->work_groups_changes}}-آگهی</th>
                @if($item->status == 1)
                    <th scope="row">فعال</th>
                @else
                    <th scope="row">غیر فعال</th>
                @endif
                <th scope="row">{{\App\Models\Plan::find($item->plan_id)->title}}</th>
                <th scope="row">
                    @if($item->status == 1 )
                        <button class="btn btn-outline-warning" wire:click="toggleDiscounts({{$item->id}},'Deactive')">
                            غیر
                            فعال
                        </button>
                    @else
                        <button class="btn btn-outline-success" wire:click="toggleDiscounts({{$item->id}},'Active')">
                            فعال
                        </button>
                    @endif
                    <button class="btn btn-outline-danger" wire:click="delete({{$item->id}})">حذف</button>
                </th>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$discounts->links()}}

</div>
