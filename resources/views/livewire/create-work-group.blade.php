<div>
    @livewire('admin-header')
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="form-group col-lg-3">
                <label for="workgroup-title">عنوان گروه کاری</label>
                <input type="text" id="workgroup-title" class="form-control col-lg-3" name="title" wire:model="title">
            </div>
            <div class="form-group col-lg-2">
                <label for="type">نوع</label>
                <select class="form-control" id="type" name="type" wire:model="type">
                    <option value="AUCTION">مزایده</option>
                    <option value="TENDER">مناقصه</option>
                    <option value="INQUIRY">استعلام</option>
                </select>
            </div>
            <div class="form-group col-lg-1">
                <label for="workgroup-priority">اولویت</label>
                <input type="number" class="form-control col-lg-3" id="workgroup-priority" name="priority"
                       wire:model="priority">
            </div>
            <div class="form-group col-lg-1">
                <label for="workgroup-parent-id">سرگروه</label>
                <input type="number" class="form-control col-lg-3" id="workgroup-parent-id" name="parent_id"
                       wire:model="parent_id">
            </div>
            <div class="form-group col-lg-1 m-4">
                <label for="workgroup-status">وضعیت نمایش</label>
                <input type="checkbox" class="custom-checkbox col-lg-3" id="workgroup-status" name="status"
                       wire:model="status">
            </div>
            <div class="file-group col-lg-3">
                <label for="customFileLang">انتخاب آیکون</label>
                <input type="file" class="btn btn-outline-primary" name="image" wire:model="image">
            </div>

            <div style="direction: ltr!important;">
                <button class="btn btn-primary mt-4 col-lg-1" wire:click="StoreWorkGroup">ذخیره</button>
            </div>
            <br>
            <br>
        </div>
    </div>
    <br>
    <div class="container">

        <div class="row">
            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">شماره</th>
                    <th scope="col">نام گروه کاری</th>
                    <th scope="col">نوع</th>
                    <th scope="col"> تعداد زیر مجموعه</th>
                    <th scope="col">سر گروه</th>
                    <th scope="col">وضعیت</th>
                    <th scope="col">اولویت</th>
                    <th scope="col">ابذار</th>
                </tr>
                </thead>
                <tbody>
                @foreach($work_groups as $item)
                    <tr>
                        <th scope="row">{{$i++}}</th>
                        <td>{{$item->title}}</td>
                        @if($item->type == 'AUCTION')
                            <td>مزایده</td>
                        @endif
                        @if($item->type == 'TENDER')
                            <td>مناقصه</td>
                        @endif
                        @if($item->type == 'INQUIRY')
                            <td>استعلام</td>
                        @endif
                        @if(\App\Models\WorkGroup::where('parent_id',$item->id)->count() != 0)
                            <td>{{\App\Models\WorkGroup::where('parent_id',$item->id)->count()}}</td>
                        @else
                            <td>بدون زیر مجموعه</td>
                        @endif
                        @if(isset($item->parent_id) && $item->parent_id != "")
                            <td>{{\App\Models\WorkGroup::where('id',$item->parent_id)->get()->first()->title}}</td>
                        @else
                           <td> بدون سرگروه</td>
                        @endif
                        <td>
                            @if($item->status == 1)
                                فعال
                            @else
                                غیر فعال
                            @endif
                        </td>
                        <td>
                            {{$item->priority}}
                        </td>
                        <td>
                            <button class="btn btn-outline-danger" wire:click="delete({{$item->id}})">
                                حذف
                            </button>
                            @if($item->status == 1 )
                                <button class="btn btn-outline-warning"
                                        wire:click="toggleActivation({{$item->id}},'setDeactive')">
                                    غیر فعال
                                </button>
                            @else
                                <button class="btn btn-outline-success"
                                        wire:click="toggleActivation({{$item->id}},'setActive')">
                                    فعال
                                </button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <p>{{ $work_groups->links() }}</p>

        </div>

    </div>

</div>
