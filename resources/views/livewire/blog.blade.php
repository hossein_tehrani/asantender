<div>
    <div class="header">
        <div id="blogHeadLine"><br></div>
        <div id="blogHeader">
            <div class="row" id="blogHeader">
                <div class="col-3">
                    <img src="/logo.png" class="m-1">
                </div>
                <div class="col-3 mt-2">
                    اطلاع رسانی آنلاین مناقصات و مزایدات کشور
                </div>
                <div class="col-2">
                    <p>
                        ---
                    </p>
                </div>
                <div class="col-2">
                    <p style="font-family: sahel">
                        {{\Morilog\Jalali\Jalalian::forge(Now())->format('%Y |%B %d ')}}
                    </p>
                </div>
            </div>
        </div>
        <div id="blogHeadLine"><br></div>
    </div>
    <div class="body" id="blogBody">
        <div class="card-body" id="card-body">
            آخرین مقالات :
            @foreach($posts as $item)
                <br><a href="/post/{{$item->slug}}">{{$item->title}}</a>
            @endforeach
        </div>
    </div>
</div>
