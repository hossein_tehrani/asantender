<div class="cont" style="overflow-x: hidden;">
    <div class="container-fluid">
        @livewire('admin-header')
    </div>
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>
    <div id="searchArea">
        <div id="searchButtons" class="">
            <div class="row">

                <input class="form-control me-2 col-lg-2 col-5 ml-4" type="search" placeholder="Search"
                       aria-label="Search" wire:model="content">

                <label for="orderBy"></label>
                <select name="orderBy" class="form-control col-lg-2 col-5 mr-2 ml-2" id="orderBy">
                    <option>
                        به ترتیب تاریخ ثبت نام
                    </option>
                    <option>
                        به ترتیب حروف الفبا
                    </option>
                </select>
                <div class="mt-2">
                    <span class="ml-4">فقط کاربران دارای: </span>
                    <label for="haveCreditUser" class="mr-2 ml-2">اعتبار</label>
                    <input type="checkbox" class="mr-3" id="haveCreditUser" name="haveCreditUser">

                    <label for="haveCreditUser">فقط کاربران دارای | اعتبار</label>
                    <input type="checkbox" id="haveCreditUser" name="haveCreditUser">

                    <label for="havePlanUser">, پلان </label>
                    <input type="checkbox" id="havePlanUser" name="havePlanUser">

                    <label for="onlyOrganizationUser">, سازمانی</label>
                    <input type="checkbox" id="onlyOrganizationUser" name="onlyOrganizationUser"
                           wire:click="search({{true}})">
                    <label for="onlyOrganizationUser" class="mr-2 ml-1"> سازمانی</label>
                    <input type="checkbox" class="mr-2" id="onlyOrganizationUser" name="onlyOrganizationUser">

                    <label for="onlyCompanyUser" class="mr-2 ml-1"> شرکتی</label>
                    <input type="checkbox" class="mr-2" id="onlyCompanyUser" name="onlyCompanyUser">

                    <label for="onlyPersonalUser" class="mr-2 ml-2"> شخصی</label>
                    <input type="checkbox" class="mr-3" id="onlyPersonalUser" name="onlyPersonalUser">
                </div>

                <button class="btn btn-outline-primary btn-pri" wire:click="search">جستجو</button>
            </div>
        </div>

        @isset($searchedUser)
            <div id="searchContent" class="container">
                <table class="table" wire:poll>
                    <thead>
                    <tr>
                        <th scope="col">شناسه</th>
                        <th scope="col">نوع کاربر</th>
                        <th scope="col">نام کاربر</th>
                        <th scope="col">شماره همراه</th>
                        <th scope="col">ایمیل</th>
                        <th scope="col">وضعیت دسترسی</th>
                        <th scope="col">میزان اعتبار</th>
                        <th scope="col">پلان های فعال</th>
                        <th scope="col">صفحه کاربر</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($searchedUser as $user)
                        <tr>
                            <th scope="row">{{$user->id}}</th>
                            <th>
                                @foreach($user->roles as $item)
                                    {{$item->label}}
                                @endforeach
                            </th>
                            <td>{{$user->full_name}}</td>
                            <td>{{$user->mobile}}</td>
                            <td>{{$user->email}}</td>
                            {{--                            $user->active==1 || $user->status_access==true--}}
                            {{--                            $user->active==0 || $user->active==false--}}
                            @if($user->active)
                                <td>
                                    <button type="button" class="btn btn-success"
                                            wire:click="inactive({{$user->id}})">
                                        فعال
                                    </button>
                                </td>
                            @else
                                <td>
                                    <button type="button" class="btn btn-danger"
                                            wire:click="active({{$user->id}})">
                                        غیر فعال
                                    </button>
                                </td>
                            @endif
                            <td>
                                @foreach($user->account as $item)
                                    {{number_format($item->amount)."T"}}
                                @endforeach
                            </td>

                            <td>
                                @if(count($user->userPlans))
                                    @foreach($user->userPlans as $item)
                                        @if(isset($item->plan->title))
                                            {{$item->plan->title}}|
                                        @endif
                                    @endforeach
                                @endif
                                @if(!count($user->userPlans))
                                    فاقد پلان
                                @endif
                            </td>

                            <td>
                                <a href="#" wire:click="showProfile({{$user->id}})">
                                    پروفایل کاربر
                                </a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endisset

    </div>
    <div>
        <table class="table" wire:poll>
            <thead>
            <tr>
                <th scope="col">شناسه</th>
                <th scope="col">نوع کاربر</th>
                <th scope="col">نام کاربر</th>
                <th scope="col">شماره همراه</th>
                <th scope="col">ایمیل</th>
                <th scope="col">وضعیت دسترسی</th>
                <th scope="col">میزان اعتبار</th>
                <th scope="col">پلان های فعال</th>
                <th scope="col">صفحه کاربر</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{$user->id}}</th>
                    <th>
                        @foreach($user->roles as $item)
                            {{$item->label}}
                        @endforeach
                    </th>
                    <td>{{$user->full_name}}</td>
                    <td>{{$user->mobile}}</td>
                    <td>{{$user->email}}</td>
                    {{--                            $user->active==1 || $user->status_access==true--}}
                    {{--                            $user->active==0 || $user->active==false--}}
                    @if($user->active)
                        <td>
                            <button type="button" class="btn btn-success"
                                    wire:click="inactive({{$user->id}})">
                                فعال
                            </button>
                        </td>
                    @else
                        <td>
                            <button type="button" class="btn btn-danger"
                                    wire:click="active({{$user->id}})">
                                غیر فعال
                            </button>
                        </td>
                    @endif
                    <td>
                        @foreach($user->account as $item)
                            {{number_format($item->amount)."T"}}
                        @endforeach
                    </td>

                    <td>
                        @if(count($user->userPlans))
                            @foreach($user->userPlans as $item)
                                @if(isset($item->plan->title))
                                    {{$item->plan->title}}|
                                @endif
                            @endforeach
                        @endif
                        @if(!count($user->userPlans))
                            فاقد پلان
                        @endif
                    </td>

                    <td>
                        <a href="#" wire:click="showProfile({{$user->id}})">
                            پروفایل
                        </a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @push('scripts')
        <script>
        </script>
    @endpush
</div>
