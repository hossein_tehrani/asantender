<div>
    <div class="container-fluid">
        @livewire('admin-header')
    </div>
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">نام کاربر</th>
            <th scope="col">آیدی کاربر</th>
            <th scope="col">عنوان پیام</th>
            <th scope="col">متن پیام</th>
            <th scope="col">تاریخ و زمان</th>
            <th scope="col">وضعیت</th>
            <th scope="col">ابزار</th>
        </tr>
        </thead>
        <tbody>
        @foreach($allMessages as $key => $message)
            <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$message->user->full_name}}</td>
                <td>{{$message->user_id}}</td>
                <td>{{\Illuminate\Support\Str::limit($message->title,15)}}</td>
                <td>{{\Illuminate\Support\Str::limit($message->body,30)}}</td>
                <td>{{\Morilog\Jalali\Jalalian::forge($message->created_at)->format('%d|%B|%y|H:i')}}</td>
                @if($message->seen == 1)
                    <td>دیده شده</td>
                @else
                    <td>دیده نشده</td>
                @endif

                <td>
                    <button class="btn btn-outline-danger" wire:click="remove({{$message->id}})">حذف</button>
                    |
                    <button class="btn btn-outline-success" data-toggle="modal"
                            data-target="#exampleModal" wire:click="show({{$message->id}})">مشاهده
                    </button>

                    <!-- Modal -->
                    <div wire:ignore class="modal fade" id="exampleModal" tabindex="-1"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group mx-sm-3 mb-2">
                                        <p>{{$message->title}}</p>
                                        <p>
                                            {{$message->body}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div style="display:table!important;margin:auto!important;">{{$allMessages->links()}}</div>


</div>
