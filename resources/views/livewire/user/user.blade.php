<div>
    <div class="container-fluid">
        @livewire('admin-header')
    </div>
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>
    <div wire:ignore class="modal fade" id="blockUser" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body mt-3">

                    <p>آیا از مسدود سازی کاربر مطمعن هستید؟</p>

                </div>
                <div class="d-flex justify-content-end">
                    <button type="button" class="btn btn-danger m-2 px-4" data-dismiss="modal">خیر</button>
                    <button type="button" class="btn btn-success m-2 px-4" data-dismiss="modal"
                            wire:click="blockUser({{$user->id}})">بله
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore class="modal fade" id="sendcreidt" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">انتقال اعتبار</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body mt-3">

                    <label for="sendC" class="pl-5 ml-5"> لطفا مقدار اعتبار ارسالی را به تومان وارد کنید
                    </label>

                    <div class="form-group d-flex justify-content-center">
                        <input type="number" id="sendC" class="form-control col-lg-8"
                               placeholder="مقدار اعتبار ارسالی..." wire:model="amount">

                    </div>

                </div>
                <div class="d-flex justify-content-end">
                    <button type="button" class="btn btn-danger m-2" data-dismiss="modal">انصراف</button>
                    <button type="button" class="btn btn-success m-2" data-dismiss="modal"
                            wire:click="transferCredit({{$user->id}})">ارسال
                    </button>
                </div>
            </div>
        </div>
    </div>

    {{-- userDetailModal --}}
    <div wire:ignore class="modal fade" id="userDetailModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle" style="font-weight: bold">اطلاعات کاربر</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body mt-3">
                    @switch($user->roles()->first()->name)
                        @case('organization')
                        <div>
                            @if(!empty($userDetail))
                                <div class="row">

                                    <div class="col-md-6">

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> تابعیت و مالکیت : </label>
                                            <label>
                                                {{$userDetail->ownership}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> نوع مالکیت : </label>
                                            <label>
                                                {{\App\Models\User::convertOwnership($userDetail->ownership_type)}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> شناسه ملی : </label>
                                            <label>
                                                {{$userDetail->national_id}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> آدرس : </label>
                                            <label>
                                                {{$userDetail->address}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> تلفن : </label>
                                            <label>
                                                {{$userDetail->phone}}
                                            </label>
                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> نام مدیر : </label>
                                            <label>
                                                {{$userDetail->manager_name}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> سمت مدیر : </label>
                                            <label>
                                                {{$userDetail->manager_title}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> تلفن مدیر : </label>
                                            <label>
                                                {{$userDetail->manager_phone}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> فکس : </label>
                                            <label>
                                                {{$userDetail->fax}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> کد پستی : </label>
                                            <label>
                                                {{$userDetail->postal_code}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> وبسایت : </label>
                                            <label>
                                                <a href={{$userDetail->website_url}} target="_blank">{{$userDetail->website_url}}</a>
                                            </label>
                                        </div>

                                    </div>
                                </div>

                                <hr>
                                <div class="col">
                                    <label for="exampleFormControlTextarea1">درباره سازمان</label>
                                    <textarea class="form-control" readonly id="exampleFormControlTextarea1"
                                              rows="4">{{$userDetail->about}}</textarea>
                                </div>

                                <hr>
                                <div class="col">
                                    <a href={{config('app.url') . $userDetail->organization_logo}} download>
                                        <p style="text-align: center">لوگو سازمانی</p>
                                    </a>

                                    <img src={{$userDetail->organization_logo}} class="img-fluid" alt="لوگو سازمانی">
                                    <hr>
                                    <a href={{config('app.url') . $userDetail->organization_logo}} download>
                                        <p style="text-align: center">نامه رسمی درخواست خدمات با امضای مجاز سازمان</p>
                                    </a>
                                    <img src={{$userDetail->organization_logo}} class="img-fluid" alt="نامه رسمی ">
                                </div>

                            @else
                                <div class="alert alert-danger" role="alert">
                                    اطلاعاتی از کاربر یافت نشد
                                </div>
                            @endisset
                        </div>

                        @break
                        @case('personal')

                        @if(!empty($userDetail))
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> کد ملی : </label>
                                        <label>
                                            {{$userDetail->id_card}}
                                        </label>
                                    </div>

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> آدرس : </label>
                                        <label>
                                            {{$userDetail->address}}
                                        </label>
                                    </div>

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> رشته تحصیلی : </label>
                                        <label>
                                            {{$userDetail->study_field}}
                                        </label>
                                    </div>

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> ضمینه شغلی : </label>
                                        <label>
                                            {{$userDetail->job_title}}
                                        </label>
                                    </div>

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> نام محل کار : </label>
                                        <label>
                                            {{$userDetail->work_name}}
                                        </label>
                                    </div>

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> تلفن کار : </label>
                                        <label>
                                            {{$userDetail->work_phone_number}}
                                        </label>
                                    </div>

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;">زبان خارجه : </label>
                                        @if($userDetail->lang)
                                            <label>
                                                میداند
                                            </label>
                                        @else
                                            <label>
                                                میداند
                                            </label>
                                        @endif
                                    </div>

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> سطح زبان/زبان ها : </label>
                                        @if(!empty($userDetail->lang_level))
                                            @switch($userDetail->lang_level)
                                                @case ('excellent')
                                                <label> عالی</label>
                                                @break
                                                @case( 'good')
                                                <label> خوب</label>
                                                @break
                                                @case ('normal')
                                                <label> معمولی</label>
                                                @break
                                            @endswitch
                                        @endif


                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> تاریخ تولد : </label>
                                        <label>
                                            {{$userDetail->birth_date}}
                                        </label>
                                    </div>

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> تلفن : </label>
                                        <label>
                                            {{$userDetail->phone_number}}
                                        </label>
                                    </div>

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> آخرین مدرک تحصیلی : </label>
                                        <label>
                                            {{$userDetail->last_degree}}
                                        </label>
                                    </div>

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> جایگاه شغلی : </label>
                                        <label>
                                            {{$userDetail->job_position}}
                                        </label>
                                    </div>

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> وب سایت : </label>
                                        <label>
                                            <a href={{$userDetail->website_url}} target="_blank">{{$userDetail->website_url}}</a>
                                        </label>
                                    </div>

                                    <div class="col col-md-12">
                                        <label style=" font-weight: bold;"> نام زبان/زبان ها : </label>
                                        <label>
                                            {{$userDetail->lang_name}}
                                        </label>
                                    </div>

                                </div>
                            </div>
                            <hr>
                            <div class="col">

                                <label for="exampleFormControlTextarea1">شرح مختصر از سوابق حرفه ای، علمی و جایگاه
                                    اجتماعی : </label>
                                <textarea class="form-control" readonly id="exampleFormControlTextarea1"
                                          rows="4">{{$userDetail->about}}</textarea>

                                <label for="exampleFormControlTextarea1">شرح مختصر پیرامون دلیل نیاز به خدمات این سامانه
                                    : </label>
                                <textarea class="form-control" readonly id="exampleFormControlTextarea1"
                                          rows="4">{{$userDetail->description}}</textarea>

                            </div>

                            <hr>
                            <div class="col">
                                <a href={{config('app.url') . $userDetail->image_url}} download>
                                    <p style="text-align: center">عکس</p>
                                </a>
                                <img src={{$userDetail->image_url}} class="img-fluid" alt="عکس کاربر">
                                <hr>
                                <a href={{config('app.url') . $userDetail->national_url}} download>
                                    <p style="text-align: center">تصویر کارت ملی</p>
                                </a>
                                <img src={{$userDetail->national_url}} class="img-fluid" alt="تصویر کارت ملی">
                                <hr>
                                <a href={{config('app.url') . $userDetail->second_image_url}} download>
                                    <p style="text-align: center"> تصویر کارت هویت حرفه ای</p>
                                </a>
                                <img src={{$userDetail->second_image_url}} class="img-fluid"
                                     alt="تصویر کارت هویت حرفه ای">
                            </div>

                        @else
                            <div class="alert alert-danger" role="alert">
                                اطلاعاتی از کاربر یافت نشد
                            </div>
                        @endisset
                        @break
                        @case('company')
                        <div>
                            @if(!empty($userDetail))
                                <div class="row">

                                    <div class="col-md-6">

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> ایمیل سازمانی : </label>
                                            <label>
                                                {{$userDetail->company_email}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> ضمینه فعالیت : </label>
                                            <label>
                                                {{$userDetail->activity_context}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> کد اقتصادی : </label>
                                            <label>
                                                {{$userDetail->economic_code}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> صادرات: </label>
                                            @if($userDetail->export)
                                                دارد
                                            @else
                                                ندارد
                                            @endif
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> خلاق : </label>
                                            @if($userDetail->creative)
                                                میباشد
                                            @else
                                                نمیباشد
                                            @endif
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> عضویت در اتاق بازرگانی : </label>
                                            @if($userDetail->is_commerce)
                                                میباشد
                                            @else
                                                نمیباشد
                                            @endif
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> نام مدیر: </label>
                                            <label>
                                                {{$userDetail->manager_name}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> فکس : </label>
                                            <label>
                                                {{$userDetail->fax}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> وبسایت : </label>
                                            <label>
                                                <a href={{$userDetail->website_url}} target="_blank">{{$userDetail->website_url}}</a>
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> مسئول راهبری حساب کاربری این سامانه
                                                : </label>
                                            <label>
                                                {{$userDetail->nickname}}
                                            </label>
                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;">نوع شرکت : </label>
                                            <label>
                                                {{$userDetail->company_type}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> شناسه ملی : </label>
                                            <label>
                                                {{$userDetail->national_id}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> تعداد پرسنل : </label>
                                            <label>
                                                {{$userDetail->members_count}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> دانش بنیان : </label>
                                            @if($userDetail->knowledge_base)
                                                میباشد
                                            @else
                                                نمیباشد
                                            @endif
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> دیگر مجامع صنفی و تشکل ها : </label>
                                            <label>
                                                {{$userDetail->other_commerce}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> آدرس : </label>
                                            <label>
                                                {{$userDetail->address}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> زبان وبسایت : </label>
                                            <label>
                                                {{$userDetail->website_lang}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> تلفن همراه راهبر سامانه : </label>
                                            <label>
                                                {{$userDetail->nickname_phone}}
                                            </label>
                                        </div>

                                        <div class="col col-md-12">
                                            <label style=" font-weight: bold;"> سمت سازمانی راهبر سامانه : </label>
                                            <label>
                                                {{$userDetail->nickname_title}}
                                            </label>
                                        </div>

                                    </div>
                                </div>

                                <hr>
                                <div class="col">
                                    <label for="exampleFormControlTextarea1">درباره شرکت</label>
                                    <textarea class="form-control" readonly id="exampleFormControlTextarea1"
                                              rows="4">{{$userDetail->about}}</textarea>
                                </div>

                                <hr>
                                <div class="col">
                                    <a href={{config('app.url') . $userDetail->official_newspaper}} download>
                                        <p style="text-align: center">روزنامه رسمی </p>
                                    </a>
                                    <img src={{$userDetail->official_newspaper}} class="img-fluid" alt="روزنامه رسمی ">
                                    <hr>
                                    <a href={{config('app.url') . $userDetail->license}} download>
                                        <p style="text-align: center">تصویر مجوزهای رسمی فعالیت</p>
                                    </a>
                                    <img src={{$userDetail->license}} class="img-fluid" alt="تصویر مجوزهای رسمی فعالیت">
                                    <hr>
                                    <a href={{config('app.url') . $userDetail->company_logo}} download>
                                        <p style="text-align: center">لوگو شرکت</p>
                                    </a>
                                    <img src={{$userDetail->company_logo}} class="img-fluid" alt="لوگو شرکت">
                                    <hr>
                                    <a href={{config('app.url') . $userDetail->organization_logo}} download>
                                        <p style="text-align: center">لوگو سازمانی</p>
                                    </a>
                                    <img src={{$userDetail->organization_logo}} class="img-fluid" alt="لوگو سازمانی">
                                    <hr>
                                    <a href={{config('app.url') . $userDetail->official_letter}} download>
                                        <p style="text-align: center">نامه رسمی</p>
                                    </a>
                                    <img src={{$userDetail->official_letter}} class="img-fluid" alt="نامه رسمی">
                                    <hr>


                                </div>

                            @else
                                <div class="alert alert-danger" role="alert">
                                    اطلاعاتی از کاربر یافت نشد
                                </div>
                            @endisset
                        </div>

                        @break
                    @endswitch
                </div>
                <hr>
                <div class="d-flex justify-content-end">
                    <button type="button" class="btn btn-danger m-2" data-dismiss="modal">بستن</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="card col-lg-5 mt-3 ml-5">
                <div class="h5 pl-4 pt-3" style="font-weight: bold">اطلاعات کاربر</div>
                <div class="card-body">
                    <div class="container">

                        <div>
                            <div>
                                نام کاربر:

                            {{$user->full_name}}
                            <!-- Button trigger modal -->
                                <button type="button" class="badge btn-outline-dark" data-toggle="modal"
                                        data-target="#exampleModal" style="font-size:15px!important">
                                    ارسال پیام
                                </button>

                                <!-- Modal -->
                                <div wire:ignore class="modal fade" id="exampleModal" tabindex="-1"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">ارسال پیام</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group mx-sm-3 mb-2">
                                                    <label for="title" class="sr-only">عنوان</label>
                                                    <input type="text"
                                                           class="form-control @error('title') is-invalid @enderror"
                                                           required id="title"
                                                           wire:model="title"
                                                           placeholder="عنوان پیام">
                                                    @error('title')
                                                    <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                            </span>
                                                    @enderror
                                                    <div class="input-group">
                                                        <textarea
                                                            wire:model="body"
                                                            class="form-control @error('body') is-invalid @enderror"
                                                            required
                                                            id="body"
                                                            aria-label="With textarea"
                                                            placeholder="متن پیام را وارد کنید"></textarea>
                                                        @error('body')
                                                        <p class="invalid-feedback">
                                                            <strong>{{ $message }}</strong>
                                                        </p>
                                                        @enderror
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger"
                                                                data-dismiss="modal">
                                                            بستن
                                                        </button>
                                                        <button wire:click="submitMessage({{$user->id}})"
                                                                class="btn btn-primary" data-dismiss="modal">ارسال
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p wire:poll.3s>
                                موجودی:
                                @foreach($user->account as $item)
                                    {{number_format($item->amount)}}
                                    <span class="badge badge-pill badge-light">
                                        {{$item->accountType->wallet->title}}
                                    </span>

                                @endforeach
                            </p>
                        </div>
                        <div>
                            <p>
                                ایمیل:

                                {{$user->email}}
                            </p>
                        </div>
                        <div>
                            <p>
                                شماره همراه:

                                {{$user->mobile}}
                            </p>
                        </div>
                        <div>
                            <p>
                                نوع کاربر:

                                @foreach($user->roles as $item)
                                    {{$item->label}}
                                @endforeach
                            </p>
                        </div>
                        {{--                        <div>--}}
                        {{--                            <p>--}}
                        {{--                                پلان های کاربر:--}}

                        {{--                                @foreach($user->userPlans as $item)--}}
                        {{--                                    {{$item->plan->title}}--}}
                        {{--                                @endforeach--}}
                        {{--                            </p>--}}
                        {{--                        </div>--}}
                        {{--                        <div>--}}
                        {{--                            <p>--}}
                        {{--                                لیست تراکنش های کاربر:--}}
                        {{--                                @foreach($user->transactions as $item)--}}
                        {{--                                    {{$item->id}}--}}
                        {{--                                @endforeach--}}
                        {{--                            </p>--}}
                        {{--                        </div>--}}

                        <div class="d-flex justify-content-end">
                            @if($user->locked==false && $show_locked_button==true)
                                <button type="button" class="btn btn-danger mx-1" data-toggle="modal"
                                        data-target="#blockUser">مسدود سازی
                                </button>
                            @endif
                            @if($show_unlocked_button==true)
                                <p style="font-size:13px;margin:4px;" class="btn btn-outline-danger">حساب
                                    کاربری {{$user->full_name}} مسدود است</p>
                                <button type="button" class="btn btn-outline-success mx-1"
                                        wire:click="unblockUser({{$user->id}})">رفع مسدودی
                                </button>
                            @endif
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#sendcreidt">
                                انتقال اعتبار
                            </button>
                            <button type="button" class="btn btn-dark mx-1" data-toggle="modal"
                                    data-target="#userDetailModal">اطلاعات کاربر
                            </button>

                        </div>

                    </div>
                </div>
            </div>
            <div class="card col-lg-5 mt-3 ml-5">
                <div class="h5 pl-4 pt-3" style="font-weight: bold">تراکنش های مالی کاربر</div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>وضعیت</th>
                            <th>نوع</th>
                            <th>تاریخ</th>
                            <th>مبلغ</th>
                            <th>کدپیگیری</th>

                        </tr>
                        </thead>
                        <tbody>

                        @isset($transactions)
                            @foreach($transactions as $item)
                                <tr>
                                    @if(!empty($item->paid_at))
                                        <td>
                                            <span class="badge badge-success">موفق</span>
                                        </td>
                                    @else
                                        <td>
                                            <span class="badge badge-danger">ناموفق</span>
                                        </td>
                                    @endif

                                    <td>{{\Modules\Wallet\Entities\Transaction::convertType($item->type)}}</td>

                                    @if(!empty($item->paid_at))
                                        <td>{{\App\Helper\toJalali($item->paid_at)['date']}}</td>
                                    @else
                                        <td>
                                            <span class="badge badge-pill badge-warning">بدون تاریخ</span>
                                        </td>
                                    @endif

                                    <td>{{$item->amount}} تومان</td>

                                    @if(!empty($item->factor_number))
                                        <td>
                                            <span class="badge badge-light">{{$item->factor_number}}</span>
                                        </td>
                                    @else
                                        <td>
                                            <span class="badge badge-pill badge-warning">بدون فاکتور</span>
                                        </td>
                                    @endif

                                </tr>
                            @endforeach
                        @endisset
                        </tbody>
                    </table>
                    <p style="font-size:18.2px;">{{$transactions->links()}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card col-lg-11 ml-4 mt-3">
                <div class="h5 pl-4 pt-3" style="font-weight: bold">پلان های کاربر</div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>نام</th>
                            <th>حساسیت</th>
                            <th>زمان تحویل</th>
                            <th>مدت اعتبار</th>
                            <th>عکس</th>
                            <th>ویدویو</th>
                            <th>کلمات</th>
                            <th>مطالب خبری</th>
                            <th>تاریخ انقضا</th>

                        </tr>
                        </thead>
                        <tbody wire:poll>

                        @isset($plans)
                            @foreach($user->userPlans as $item)
                                <tr>
                                    <td>{{$item->plan->title}}</td>
                                    @switch($item->sensitivity)
                                        @case('normal')
                                        <td>
                                            <span class="badge badge-light">عادی</span>
                                        </td>
                                        @break
                                        @case('sensitive')
                                        <td>
                                            <span class="badge badge-warning">حساس</span>
                                        </td>
                                        @break
                                    @endswitch

                                    @switch($item->delivery_time)
                                        @case('normal')
                                        <td>
                                            <span class="badge badge-light">عادی</span>
                                        </td>
                                        @break
                                        @case('urgent')
                                        <td>
                                            <span class="badge badge-warning">فوری</span>
                                        </td>
                                        @break
                                        @case('super_urgent')
                                        <td>
                                            <span class="badge badge-danger">فوق فوری</span>
                                        </td>
                                        @break
                                    @endswitch

                                    <td>{{$item->plan->plan_duration}}<span class="badge badge-light">روز</span></td>

                                    @if(!empty($item->text_photo_count))
                                        <td>{{$item->text_photo_count}} <span class="badge badge-light">عکس</span></td>
                                    @else
                                        <td><span class="badge badge-secondary">ندارد</span></td>
                                    @endif

                                    @if(!empty($item->video_duration))
                                        <td>{{$item->video_duration}} <span class="badge badge-light">دقیقه</span></td>
                                    @else
                                        <td><span class="badge badge-secondary">ندارد</span></td>
                                    @endif

                                    @if(!empty($item->word_count))
                                        <td>{{$item->word_count}} <span class="badge badge-light">کلمه</span></td>
                                    @else
                                        <td><span class="badge badge-secondary">ندارد</span></td>
                                    @endif

                                    @if(!empty($item->news_content_count))
                                        <td>{{$item->news_content_count}} <span class="badge badge-light">خبر</span>
                                        </td>
                                    @else
                                        <td><span class="badge badge-secondary">ندارد</span></td>
                                    @endif

                                    <td>{{\App\Helper\toJalali($item->expired_date)['date']}}</td>
                                </tr>
                            @endforeach
                        @endisset
                        </tbody>
                    </table>
                    <p class="my-auto px-auto" style="font-size:18.2px;">{{$plans->links()}}</p>
                </div>
            </div>
        </div>
    </div>

</div>
@push('scripts')
    <script>
    </script>
@endpush
