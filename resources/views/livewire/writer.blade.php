<div>
    @livewire('admin-header')
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>
    <div class="row">
        <div class="col-5">
            <div class="form-group">
                <label for="title">عنوان پست</label>
                <input type="text" class="form-control" id="title" wire:model="title">
            </div>
        </div>
        <div class="col-5">
            <div class="form-group">
                <label for="keywords">کلمات کلیدی</label>
                <input type="text" class="form-control" id="keywords" wire:model="keywords">
            </div>
        </div>
        <div class="col-2">
            <div class="form-group">
                <label for="priority">اولویت</label>
                <input type="text" class="form-control" id="priority" wire:model="priority">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <label for="meta">متاتگ ها ( تگ کامل نوشته شود)</label>
            <textarea id="meta" class="form-control" wire:model="meta"></textarea>
        </div>
        <div class="col-6">
            <label for="description">توضیحات</label>
            <textarea class="form-control" id="description" rows="3" wire:model="description"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <label for="body">متن پست</label>
            <textarea class="form-control" id="body" rows="3" wire:model="body"></textarea>
        </div>
        <div class="col-1">
            <button class="btn btn-outline-success" wire:click="create">ثبت</button>
        </div>
    </div>
</div>
