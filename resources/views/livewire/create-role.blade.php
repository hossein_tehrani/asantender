<div style="overflow-x: hidden;">
    <div class="container-fluid">
        @livewire('admin-header')
    </div>
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">ایجاد سمت جدید</div>
                    <div class="card-body row">
                        <div class="form-group col-lg-4">
                            <label for="name" class="form-label text-md-right">نام سمت</label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autofocus wire:model="name">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <p id="attentionMessage">(نام سمت با حروف انگلیسی پر شود)</p>
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="label" class="form-label text-md-right">شرح سمت</label>

                            <input id="label" type="text" class="form-control @error('label') is-invalid @enderror" name="label" required autofocus wire:model="label">
                            @error('label')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group col-lg-4 col-lg-3" wire:ignore>
                            <label class="form-label text-md-right" for="permissions">دسترسی ها
                            </label>
                            <select class="form-control" id="permissions" multiple>
                                @foreach($allPermissions as $permission)
                                <option value="{{$permission->id}}">
                                    {{$permission->name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-flex col-lg-12 justify-content-end">
                            <button class="btn btn-outline-primary " wire:click="createRole">ذخیره</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">شناسه</th>
                    <th scope="col">نام سمت</th>
                    <th scope="col">شرح سمت</th>
                    <th scope="col">دسترسی</th>
                </tr>
            </thead>
            <tbody>
                @foreach($roles as $role)
                <tr>
                    <th scope="row">{{$role->id}}</th>
                    <td>{{$role->name}}</td>
                    <td>{{$role->label}}</td>
                    <td>@foreach($role->permissions as $permission){{$permission->name}}|@endforeach</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @push('scripts')
    <script>
        $('#permissions').select2();
        $('#permissions').on('change', function() {
            @this.permissions = $(this).val();
        })
    </script>
    @endpush
</div>
