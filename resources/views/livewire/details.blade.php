<div>
    @livewire('admin-header')
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>
    <div>
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            گزارشات کاربران
                        </button>
                    </h2>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                     data-parent="#accordionExample">
                    <div class="card-body">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">نام و نام خانوادگی</th>
                                <th scope="col">شماره همراه</th>
                                <th scope="col"> آخرین طرح</th>
                                <th scope="col">تاریخ انقضای آخرین طرح</th>
                                <th scope="col">تعداد گروه های کاری انتخاب شده</th>
                                <th scope="col">تعداد طرح های خریداری شده</th>
                                <th scope="col">تاریخ ثبت نام</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                @foreach($users as $item)
                                    <th scope="row">1</th>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->mobile}}</td>
                                    @if(count($item->plans()->get()))
                                        <td>{{$item->plans()->get()->last()->title}}</td>
                                    <td>{{$item->plans ? \Morilog\Jalali\Jalalian::forge(($item->plans()->get()->last()->created_at)->addDay($item->plans()->get()->last()->time))->format('%d|%B|%y') : 'کاربر طرح فعال ندارد' }}</td>
                                    @else
                                        <td>پلان ندارد</td>
                                        <td>پلان ندارد</td>
                                    @endif
                                    <td>{{count($item->workgroups()->get())}}</td>
                                    <td>{{count($item->plans()->get()) }}</td>
                                    <td>{{\Morilog\Jalali\Jalalian::forge($item->created_at)->format('%d|%B|%y')}}</td>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            گزارشات کارمندان
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th>شناسه</th>
                                <th>نام کارمند</th>
                                <th>شماره همراه</th>
                                <th>ایمیل</th>
                                <th>وضعیت دسترسی</th>
                                <th>تعداد کل ارسال آگهی</th>
                                <th>تعداد ارسال آگهی روز گذشته</th>
                                <th>تاریخ و زمان آخرین ورود</th>
                                <th>سمت ها</th>
                                <th>ابزار</th>
                            </tr>
                            @foreach($admins as $admin)
                                <tbody>
                                <tr>
                                    <th scope="row">{{$admin->id}}</th>
                                    <td>{{$admin->name}}</td>
                                    <td>{{$admin->mobile}}</td>
                                    <td>{{$admin->email}}</td>
                                    @if($admin->status_access=1 || $admin->status_access=true)
                                        <td style="width: 150px">فعال</td>
                                    @endif
                                    @if($admin->status_access=0 || $admin->status_access=false)
                                        <td>غیر فعال</td>
                                    @endif
                                    <td>{{count($admin->tenders()->get())}}T|{{count($admin->Auctions()->get())}}
                                        A|{{count($admin->Inquiries()->get())}}i|
                                    </td>
                                    <td>{{count($admin->tenders()->whereDate('created_at','>=',\Carbon\Carbon::yesterday()->toDateString())->get())}}
                                        T|{{count($admin->Auctions()->get())}}A|{{count($admin->Inquiries()->get())}}i|
                                    </td>
                                    <td style="width: 200px">{{$admin->last_login ? \Morilog\Jalali\Jalalian::forge($admin->last_login)->format('%d|%B|%y|H:i') : 'کاربر وارد حساب نشده'}}</td>
                                    <td>
                                        <p class="semat-pri">@foreach($admin->roles as $role)
                                                , {{$role->label}} @endforeach</p>
                                    </td>
                                    <td style="width: 130px"><a href="#" wire:click="update({{$admin->id}})">
                                            ویرایش </a><a href="#"
                                                          wire:click="delete({{$admin->id}})">|
                                            حذف</a></td>
                                </tr>
                                @endforeach
                                </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            گزارشات فروش و مالی
                        </button>
                    </h2>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th>شناسه</th>
                                <th>نام کارمند</th>
                                <th>شماره همراه</th>
                                <th>ایمیل</th>
                                <th>وضعیت دسترسی</th>
                                <th>تعداد کل ارسال آگهی</th>
                                <th>تعداد ارسال آگهی روز گذشته</th>
                                <th>تاریخ و زمان آخرین ورود</th>
                                <th>سمت ها</th>
                                <th>ابزار</th>
                            </tr>
                            @foreach($orders as $item)
                                <tbody>
                                <tr>
                                    <th scope="row">{{$item->id}}</th>
                                    <td>{{$item->title}}</td>
                                    <td>{{$item->price}}</td>
                                    <td>{{$item->description}}</td>
                                    <td>{{$item->has_discount}}</td>
                                     <td>{{$item->status}}</td>
                                        <td>{{\App\Models\User::find($item->user_id)->name}}</td>
                                        <td>{{\App\Models\Plan::find($item->plan_id)->title}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
