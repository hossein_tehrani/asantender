<div>
    <div class="container-fluid">
        @livewire('admin-header')
    </div>
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">ویرایش مشخصات کارمند</div>

                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">نام و نام خانوادگی</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                       name="name" required autofocus wire:model="name">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="mobile" class="col-md-4 col-form-label text-md-right">شماره موبایل</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text"
                                       class="form-control @error('mobile') is-invalid @enderror"
                                       name="mobile" required autofocus wire:model="mobile">

                                @error('mobile')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email"
                                   class="col-md-4 col-form-label text-md-right">ایمیل</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                       name="email" value="{{ old('email') }}" required autocomplete="email"
                                       wire:model="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">تلفن ثابت</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror"
                                       name="phone" required autofocus wire:model="phone">

                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">آدرس</label>

                            <div class="col-md-6">
                                <input id="address" type="text"
                                       class="form-control @error('address') is-invalid @enderror"
                                       name="address" required autofocus wire:model="address">

                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password"
                                   class="col-md-4 col-form-label text-md-right">رمز عبور</label>
                            <div class="col-md-6">
                                <input id="password" type="password"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       required autocomplete="new-password" wire:model="password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group col-lg-3">

                            <label for="type" class="form-label text-md-right">نوع کاربر</label>
                            <select id="type" class="form-control @error('type') is-invalid @enderror" name="type"
                                    required
                                    wire:model="type">
                                <option value="internal_staff">
                                    کارکنان داخلی
                                </option>
                                <option value="logical_staff">
                                    پرسنل تخصصی
                                </option>
                            </select>
                            @error('type')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-2" wire:ignore>
                            <label class="form-label text-md-right" for="rolesSelect2">
                                انتخاب سمت ها
                            </label>
                            <select id="rolesSelect2" name="rolesSelect2"
                                    multiple>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">
                                        {{$role->label}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success" wire:click="updateAdmin({{$admin}})">
                                    ویرایش
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">شناسه</th>
                <th scope="col">نام کارمند</th>
                <th scope="col">شماره همراه</th>
                <th scope="col">ایمیل</th>
                <th scope="col">وضعیت دسترسی</th>
                <th scope="col">تاریخ و زمان آخرین ورود</th>
                <th scope="col">سمت ها</th>
                <th scope="col">ابزار</th>
            </tr>
            </thead>
            <tbody>
            @foreach($admins as $item)
                <tr>
                    <th scope="row">{{$item->id}}</th>
                    <td>{{$item->name}}</td>
                    <td>{{$item->mobile}}</td>
                    <td>{{$item->email}}</td>
                    @if($item->status_access=1 || $item->status_access=true)
                        <td>فعال</td>
                    @endif
                    @if($item->status_access=0 || $item->status_access=false)
                        <td>غیر فعال</td>
                    @endif
                    <td>{{$item->last_login ? \Morilog\Jalali\Jalalian::forge($item->last_login)->format('%d|%B|%y|H:i') : 'کاربر هنوز وارد حساب کاربری نشده'}}</td>
                    <td><p>@foreach($item->roles as $role) | {{$role->label}} @endforeach</p></td>
                    <td><a href="" wire:click="update({{$item->id}})"> ویرایش </a><a href=""
                                                                                     wire:click="delete({{$item->id}})">|
                            حذف</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @push('scripts')
        <script>
            $('#rolesSelect2').select2();
            $('#rolesSelect2').on('change', function () {
            @this.admin_roles
                = $(this).val();
            });
        </script>
    @endpush
</div>
