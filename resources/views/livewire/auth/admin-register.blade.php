<div>
    <div class="container-fluid">
        @livewire('admin-header')
    </div>
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>
    <div>
        <div class="row justify-content-center" style="overflow-x: hidden;">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">ثبت نام کارمند</div>

                    <div class="card-body row">

                        <div class="form-group col-lg-3">

                            <label for="name" class="form-label text-md-right">نام و نام خانوادگی</label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                   name="name" required autofocus wire:model="name">

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="mobile" class="form-label">شماره موبایل</label>
                            <input id="mobile" type="text" class="form-control @error('mobile') is-invalid @enderror"
                                   name="mobile" required autofocus wire:model="mobile">

                            @error('mobile')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>


                        <div class="form-group col-lg-3">

                            <label for="email" class="form-label text-md-right">ایمیل</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                   name="email" value="{{ old('email') }}" required autocomplete="email"
                                   wire:model="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group col-lg-3">

                            <label for="phone" class="form-label text-md-right">تلفن ثابت</label>
                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror"
                                   name="phone" required autofocus wire:model="phone">

                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group col-lg-3">

                            <label for="address" class="form-label text-md-right">آدرس</label>
                            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror"
                                   name="address" required autofocus wire:model="address">

                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group col-lg-3">

                            <label for="password" class="form-label text-md-right">رمز عبور</label>
                            <input id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror" name="password" required
                                   autocomplete="new-password" wire:model="password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group col-lg-3">

                            <label for="type" class="form-label text-md-right">نوع کاربر</label>
                            <select id="type" class="form-control @error('type') is-invalid @enderror" name="type"
                                    required
                                    wire:model="type">
                                <option value="internal_staff">
                                    کارکنان داخلی
                                </option>
                                <option value="logical_staff">
                                    پرسنل تخصصی
                                </option>
                            </select>
                            @error('type')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-2" wire:ignore>
                            <label class="form-label text-md-right" for="rolesSelect2">
                                انتخاب سمت ها
                            </label>
                            <select id="rolesSelect2" name="rolesSelect2"
                                    multiple>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">
                                        {{$role->label}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="mt-4 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary" wire:click="addAdmin">
                                    ذخیره
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-pri">
            <table class="table">
                <tr>
                    <th>شناسه</th>
                    <th>نام کارمند</th>
                    <th>شماره همراه</th>
                    <th>ایمیل</th>
                    <th>وضعیت دسترسی</th>
                    <th>تاریخ و زمان آخرین ورود</th>
                    <th>سمت ها</th>
                    <th>ابزار</th>
                </tr>
                @foreach($admins as $admin)
                    <tbody>
                    <tr>
                        <th scope="row">{{$admin->id}}</th>
                        <td>{{$admin->name}}</td>
                        <td>{{$admin->mobile}}</td>
                        <td>{{$admin->email}}</td>
                        @if($admin->status_access=1 || $admin->status_access=true)
                            <td style="width: 150px">فعال</td>
                        @endif
                        @if($admin->status_access=0 || $admin->status_access=false)
                            <td>غیر فعال</td>
                        @endif
                        <td style="width: 200px">{{$admin->last_login ? \Morilog\Jalali\Jalalian::forge($admin->last_login)->format('%d|%B|%y|H:i') : 'کاربر وارد حساب نشده'}}</td>
                        <td>
                            <p class="semat-pri">@foreach($admin->roles as $role) , {{$role->label}} @endforeach</p>
                        </td>
                        <td style="width: 130px"><a href="#" wire:click="update({{$admin->id}})"> ویرایش </a><a href="#"
                                                                                                                wire:click="delete({{$admin->id}})">|
                                حذف</a></td>
                    </tr>
                    @endforeach
                    </tbody>
            </table>
        </div>
    </div>
    @push('scripts')
        <script>
            $('#rolesSelect2').select2();
            $('#rolesSelect2').on('change', function () {
            @this.admin_roles
                = $(this).val();
            });
        </script>
    @endpush
</div>
