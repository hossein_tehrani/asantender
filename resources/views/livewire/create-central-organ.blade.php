<div class="container">
    @livewire('admin-header')
    <div id="messages">
        <div id="successMessages">@if (session()->has('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif</div>
        <div id="errorMessages">@if (session()->has('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif</div>
        <div id="messages">@if (session()->has('message'))
                <div class="alert alert-primary">{{ session('message') }}</div>@endif</div>
    </div>
    <div class="row">
        <div class="form-group col-lg-3">
            <label for="name">نام دستگاه مرکزی</label>
            <input type="text" class="form-control col-lg-3" id="name" name="name" wire:model="name">
        </div>

        <div class="form-group col-lg-3">
            <label for="priority">اولویت</label>
            <input type="number" id="priority" class="form-control col-lg-3" name="priority" wire:model="priority">
        </div>

        <div class="form-group col-lg-3 mt-4">
            <label for="status">وضعیت انتشار</label>
            <input type="checkbox" id="status" class="custom-checkbox col-lg-3" name="status" wire:model="status">
        </div>

        <div style="direction: ltr!important;">
            <button class="btn btn-primary mt-4 col-lg-1" wire:click="StoreCentralOrgan">ذخیره</button>
        </div>
    </div>
    <br><br>
    <div class="container-fluid">
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">نام دستگاه</th>
                <th scope="col">اولویت</th>
                <th scope="col">وضعیت</th>
                <th scope="col">سرگروه</th>
                <th scope="col">ابذار</th>
            </tr>
            </thead>
            <tbody>
            @foreach($central_organs as $item)
                <tr>
                    <th scope="row">{{$i++}}</th>
                    <td>{{$item->name}}</td>
                    @if($item->priority == 0)
                        <td>بدون اولویت</td>
                    @else
                        <td>{{$item->priority}}</td>
                    @endif
                    <td>
                        @if($item->status == 1 )
                            فعال
                        @else
                            غیر فعال
                        @endif
                    </td>
                    @if($item->parent_id != null || '')
                        <td>{{\App\Models\CentralOrgan::findOrFail($item->parent_id)->name}}</td>
                    @else
                        <td>-</td>
                    @endif
                    <td>
                        @if($item->status == 0)
                            <button class="btn btn-outline-success" wire:click="toggleActivation({{$item->id}},'setActive')">
                                فعال
                            </button>
                        @else
                            <button class="btn btn-outline-warning" wire:click="toggleActivation({{$item->id}},'setDeactive')">
                                غیر فعال
                            </button>
                        @endif
                        <button class="btn btn-outline-danger" wire:click="delete({{$item->id}})">
                            حذف
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$central_organs->links()}}
    </div>

</div>
