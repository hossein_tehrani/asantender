<div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light" id="adminHeader">
        <a class="navbar-brand" href="{{route('Index')}}">آسان تندر</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        آگهی
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <button class="dropdown-item" wire:click="createTender">مناقصه</button>
                        <a class="dropdown-item" wire:click="createAuction">مزایده</a>
                        <a class="dropdown-item" wire:click="createInquiry">استعلام</a>
                        <a class="dropdown-item" wire:click="createWorkGroup">گروه کاری</a>
                        <a class="dropdown-item" wire:click="createCentralOrgan">دستکاه مرکزی</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" wire:click="Details">
                        گزارشات
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" wire:click="manageStaff"> کارمندان</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" wire:click="managePlan">طرح های اشتراکی</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" wire:click="manageDiscount">کد تخفیف</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" wire:click="manageBlog">بلاگ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">تنظیمات سایت</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://www.asantender.com">صفحه اصلی سایت</a>
                </li>
                @auth
                    <li class="nav-item">
                        <a class="nav-link" href="#" wire:click="signout">خروج<span class="sr-only">(current)</span></a>
                    </li>
                @endauth
            </ul>
        </div>
    </nav>
    <p wire:model="message">
        {{$message}}
    </p>
</div>
