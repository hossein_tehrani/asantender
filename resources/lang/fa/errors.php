<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    '400' => 'test 400',
    'no_plan' => 'طرح اشتراکی یافت نشد ! برای خرید طرح از منو طرح های اشتراک افدام نمایید.',
    'sms_cant_sent' => 'Next &sasdasd;',
    'discounts.invalid' => 'کد تخفیف شما معتبر نمیباشد',
    'not_confirmed_otp' => 'کد ارسالی اشتباه میباشد',
    'not_work_group_selected' => 'مشکلی پیش آمده و گروه های کاری انتخاب نشدند لطفا با پشتیبانی تماس بگیرید',
    'work_groups_change_maximum' => 'تعداد مجاز تعویض گروه های کاری شما به اتمام رسیده است',
    'maximum_request_work_groups' => 'تعداد انتخاب گروه های  کاری شما بیشتر از تعداد مجاز است',
    'not_search_successful' => 'دیتایی برای نمایش وجود ندارد ',
    'expired_plans' => 'طرح خریداری شده شما منقضی شده است',

];
