<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    '200' => 'test 200',
    'sms_sent' => 'sms_sent 200',
    'teste' => 'Next &raquo;',
    'work_group_selected' => 'گروه کاری شما با موفقیت انتخاب شد',
    'confirmed_otp' => 'شماره همراه شما به درستی اعتبار سنجی شد',
    'discounts.valid' => 'کد تخفیف شما معتبر میباشد',
    'buy.successfully' => 'طرح عضویت  مورد نظر شما با موفقیت خریداری شد',
    'search_successful' => 'دیتای مورد نظر با موفقیت جستجو شد',

];
