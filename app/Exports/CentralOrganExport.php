<?php

namespace App\Exports;

use App\Models\CentralOrgan;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CentralOrganExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return CentralOrgan::all();
    }

    public function headings(): array
    {
        return [
            'id',
            'name',
            'parent_id',
            'status',
            'priority',
        ];
    }
}
