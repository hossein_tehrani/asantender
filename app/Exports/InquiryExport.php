<?php

namespace App\Exports;

use App\Models\Inquiry;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InquiryExport implements FromArray
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function array(): array
    {
        $inquiries = Inquiry::with('provinces', 'workgroups')->get()->toArray();
        $array = [];
        $provinces = [];
        $workGroups = [];
        foreach ($inquiries as $item) {
            foreach ($item['provinces'] as $province) {
                array_push($provinces, $province['id']);
            }
            foreach ($item['workgroups'] as $workgroup) {
                array_push($workGroups, $workgroup['id']);
            }
            array_push($array, [
                $item['id'],
                $item['title'],
                $item['city'],
                $item['address'],
                $item['invitation_code'],
                $item['title_advertiser'],
                $item['source_adv'],
                $item['setad'],
                $item['link_url'],
                $item['img_url'],
                $item['body_adv'],
                $item['commodity_service'],
                $item['partial_medium'],
                $item['status_adv'],
                $item['free_date'],
                $item['invitation_date'],
                $item['submission_deadline_date'],
                $item['minimum_price_validity_date'],
                $item['requirement_date'],
                $item['initial_publish_date'],
                $item['publish_date'],
                $item['asantender'],
                $item['central_organ_id'],
                $item['admin_id'],
                $provinces,
                $workGroups,
            ]);
        }
        return $array;
    }
}
