<?php

namespace App\Exports;

use App\Models\Tender;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TenderExport implements FromArray
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function array(): array
    {
        $tenders = Tender::with('provinces','workgroups')->get()->toArray();
        $array = [];
        $provinces=[];
        $workGroups=[];
        foreach ($tenders as $item) {
            foreach($item['provinces'] as $province){
                array_push($provinces,$province['id']);
            }
            foreach($item['workgroups'] as $workgroup){
                array_push($workGroups,$workgroup['id']);
            }
            array_push($array, [
                $item['id'],
                $item['title'],
                $item['slug'],
                $item['city'],
                $item['address'],
                $item['invitation_code'],
                $item['title_advertiser'],
                $item['source_adv'],
                $item['setad'],
                $item['invitation_date'],
                $item['document_deadline_date'],
                $item['request_deadline_date'],
                $item['winner_announced_date'],
                $item['initial_publish_date'],
                $item['publish_date'],
                $item['free_date'],
                $item['body_adv'],
                $item['status_adv'],
                $item['img_url'],
                $item['asantender'],
                $item['link_url'],
                $item['central_organ_id'],
                $item['admin_id'],
                $provinces,
                $workGroups,
            ]);
        }
        return $array;
    }
}
