<?php

namespace App\Exports;

use App\Models\Auction;
use Maatwebsite\Excel\Concerns\FromArray;

class AuctionExport implements FromArray
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function array(): array
    {
        $auctions = Auction::with('provinces', 'workgroups')->get()->toArray();
        $array = [];
        $provinces = [];
        $workGroups = [];
        foreach ($auctions as $item) {
            foreach ($item['provinces'] as $province) {
                array_push($provinces, $province['id']);
            }
            foreach ($item['workgroups'] as $workgroup) {
                array_push($workGroups, $workgroup['id']);
            }
            array_push($array, [
                $item['id'],
                $item['title'],
                $item['city'],
                $item['address'],
                $item['auction_code'],
                $item['title_advertiser'],
                $item['source_adv'],
                $item['setad'],
                $item['invitation_date'],
                $item['view_deadline_date'],
                $item['request_deadline_date'],
                $item['winner_announced_date'],
                $item['initial_publish_date'],
                $item['publish_date'],
                $item['free_date'],
                $item['commodity_num'],
                $item['commodity_description'],
                $item['base_price'],
                $item['source_auction_num'],
                $item['body_adv'],
                $item['status_adv'],
                $item['img_url'],
                $item['link_url'],
                $item['asantender'],
                $item['admin_id'],
                $item['central_organ_id'],
                $provinces,
                $workGroups,
            ]);
        }
        return $array;

    }
}
