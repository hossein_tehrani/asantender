<?php

namespace App\Exports;

use App\Models\Ticket;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TicketExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Ticket::all();
    }
    public function headings(): array
    {
        return [
            'id',
            'name',
            'title',
            'division',
            'body',
            'answer',
            'status',
            'user_id',
        ];
    }
}
