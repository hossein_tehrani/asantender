<?php

namespace App\Exports;

use App\Models\Banner;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class BannerExport implements FromCollection , WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Banner::all();
    }
    public function headings(): array
    {
        return [
            'id',
            'title',
            'description',
            'link',
            'image_file',
            'click_count',
            'start_date',
            'expire_date',
            'hasButton',
        ];
    }
}
