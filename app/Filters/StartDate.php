<?php


namespace App\Filters;


class StartDate extends Filter
{
    protected function applyFilter($builder,$matching)
    {
        if(\request($this->filterName())==''){
            return $builder->whereIn('id',$matching);
        }
        return $builder->whereDate('publish_date','>=',\request($this->filterName()))->whereIn('id',$matching);
    }
}
