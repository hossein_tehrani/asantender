<?php


namespace App\Filters;


use App\Models\Auction;

class Province extends Filter
{
    protected function applyFilter($builder,$matching)
    {
        if(\request()->province[0]=="all"){
            return $builder->whereIn('id',$matching);
        }
        $provinces=\request()->province;

        return $builder->whereIn('province',$provinces)->whereIn('id',$matching);
    }
}
