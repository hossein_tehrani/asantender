<?php


namespace App\Filters;


use App\Models\Auction;
use Illuminate\Support\Str;

abstract class Filter
{
    public function handle($request , \closure $next)
    {
        if (!\request()->has($this->filterName())){
            return $next($request);
        }
        $builder=$next($request);
        if(\request()->search==""){
            $matching=Auction::all()->pluck('id');
            return $this->applyFilter($builder,$matching);
        }
        $matching = Auction::search(\request()->search)->get()->pluck('id');
        return $this->applyFilter($builder,$matching);
    }
    protected abstract function applyFilter($builder,$matching);
    protected function filterName(){
        return Str::snake(class_basename($this));
    }

}
