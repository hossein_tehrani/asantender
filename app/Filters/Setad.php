<?php


namespace App\Filters;


use App\Models\Auction;

class Setad extends Filter
{
    protected function applyFilter($builder,$matching)
    {
        if(\request()->setad=="all"){
            return $builder->whereIn('id',$matching);
        }
        return $builder->where('setad',\request($this->filterName()))->whereIn('id',$matching);
    }
}
