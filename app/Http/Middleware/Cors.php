<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request)
            ->header('Access-Control-Allow-Headers', 'Origin,Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With')
            ->header('Access-Control-Allow-Credentials', 'true')
            ->header('Access-Control-Allow-Methods', 'DELETE, POST, GET, OPTIONS')
//            ->header('Access-Control-Allow-Origin', 'https://tender.iran.liara.run');
          ->header('Access-Control-Allow-Origin', 'http://localhost:8080');
    }
}
