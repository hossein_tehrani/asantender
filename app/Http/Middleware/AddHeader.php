<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AddHeader
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->headers->add(['Access-Control-Allow-Origin' => '*']);
        return $next($request);
    }
}
