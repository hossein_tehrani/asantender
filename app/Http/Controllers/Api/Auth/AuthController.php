<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Traits\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    use Response;

    public function getCookieDetails($token)
    {
        return [
            'name' => 'token',
            'value' => $token,
            'minutes' => 45690,
            'path' => '/',
            'domain' => '',
            'secure' => false,
            'httpOnly' => false,
            'sameSite' => 'none',
        ];
    }

    //Initial authentication using Mobile Number of User
    public function initialAuth(Request $request)
    {
        Cache::flush();
        $randomNumber = rand(1000,9999);
        Cache::add(
            'verify_code_' . $request->mobile, // key
            $randomNumber, //value
            1500 //Time
        );
        $data = [
            'receptor' => $request->mobile,
            'message' => 'به آسان تندر خوش آمدید . رمز عبور شما ' . $randomNumber . ' میباشد ',
        ];
        if ($response = Http::post("https://api.kavenegar.com/v1/https:/api.kavenegar.com/v1/78466B41486D46737338324E53344B3334696974306B48794F7637747970323775325A45635561667A52453D/verify/lookup.json?receptor=$request->mobile&token=$randomNumber&template=asantender", $data)) {
            return $this->successResponse('sms_sent', $response, '200');
        } else {
            return $this->errorResponse('sms_cant_sent', '400');
        }
    }

    // register a new user (first check cache and if cache was true and your phone number is true too , then you can register
    public function checkMobile(Request $request)
    {
        if (Cache::has('verify_code_' . $request->mobile) && ($request->registration_code == Cache::get('verify_code_' . $request->mobile))) {
            return $this->successResponse('confirmed_otp', null, '200');
        }
        if (!Cache::has('verify_code_' . $request->mobile) || ($request->registration_code != Cache::get('verify_code_' . $request->mobile))) {
            Cache::forget('verify_code_' . $request->mobile);
            return $this->errorResponse('not_confirmed_otp', 400);
        }
    }

    public function updateUser(Request $request)
    {
        $user=Auth::user()->update([$request]);
    }

    public function register(RegisterRequest $request)
    {
        if (!Cache::has('verify_code_' . $request->mobile) || ($request->registration_code != Cache::get('verify_code_' . $request->mobile))) {
            abort(401, 'کد وارد شده صحیح نمیباشد یا مهلت آن به پایان رسیده،لطفا 60 ثانیه دیگر دوبار امتحان کنید');
        }
        $request->validated();
        $validatedData = $request->validated();
        $validatedData['password'] = bcrypt($request->password);
        if ($user = User::create($validatedData)) {
            $accessToken = $user->createToken('asantender')->accessToken;
            $cookie = $this->getCookieDetails($accessToken);
            return response()->Json([
                'user' => $user,
                'token' => $accessToken,
            ], 201)->cookie(
                $cookie['name'], $cookie['value'],
                $cookie['minutes'], $cookie['path'],
                $cookie['domain'], $cookie['secure'],
                $cookie['httpOnly'],$cookie['sameSite']);
        } else {
            return response()->Json([
                'message' => "you cant register at moment"
            ], 401);
        }
    }

//login with attempt function and login data comes from Request Validation
    public function login(LoginRequest $request)
    {
        if (Auth::user()) {
            return response()->json(['message' => 'you are logged in']);
        }
        $loginData = $request->validated();
        if (!auth()->attempt($loginData)) {
            return response(['message' => 'کاربر مورد نظر یافت نشد ، لطفا اطلاعات وارد شده را چک کنید'], 400);
        }
        $accessToken = auth()->user()->createToken('asantender')->accessToken;
        $cookie = $this->getCookieDetails($accessToken);
        return response(['user' => auth()->user(), 'token' => $accessToken])->cookie(
            $cookie['name'], $cookie['value'],
            $cookie['minutes'], $cookie['path'],
            $cookie['domain'], $cookie['secure'],
            $cookie['httpOnly'],$cookie['sameSite']);
    }

//log out Function that revoke your tokens ! and maybe you want using "auth()->logout()" for clear the cache and cookies from tokens or secret keys
    public function logout()
    {
        Auth::user()->token()->revoke();
        \Cookie::forget('token');
        return response()->json('شما با موفقیت خارج شدید.');
    }

    public function getUser()
    {
        $user2=Auth::user()->toArray();
        return $this->successResponse('200',['user2'=>$user2],200);
    }
}
