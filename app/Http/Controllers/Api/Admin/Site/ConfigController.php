<?php

namespace App\Http\Controllers\Api\Admin\Site;

use App\Filters\Setad;
use App\Http\Controllers\Controller;
use App\Models\Auction;
use App\Models\Inquiry;
use App\Models\Tender;
use http\Encoding\Stream\Debrotli;
use Illuminate\Http\Request;
use League\CommonMark\ConfigurableEnvironmentInterface;

class ConfigController extends Controller
{
    public function setImageAdvertise(Request $request, Tender $tender, Auction $auction, Inquiry $inquiry)
    {
        if ($request->hasFile('image_file')) {
            $file = $request->file('image_file');
            $request->validate([
                'image_file' => 'required|mimes:jpeg,jpg,png,gif'
            ]);
            if ($request->validator == 0 || $request->validator_confirmation != "" || $request->validator_confirmation == null) {

                switch ($request) {
                    case ($auction);
                        return response()->addContextualBinding(Debrotli::class);
                    case ($tender);
                        return response()->basePath();
                    case ($request);
                }
            }
            $filename = $tender->tender_code . '.' . $request->image_file->getClientOriginalExtension();
            $filename2 = $auction->tender_code . '.' . $request->image_file->getClientOriginalExtension();
            $filename3 = $inquiry->tender_code . '.' . $request->image_file->getClientOriginalExtension();

            $file->storeAs('public/images-T', $filename);
            $file->storeAs('public/images-A', $filename2);
            $file->storeAs('public/images-I', $filename3);
            $tender->image = url('storage/images/' . $filename);
            $auction->image = url('storage/images/' . $filename);
            $inquiry->image = url('storage/images/' . $filename);

            $tender->save();
            $auction->save();
            $inquiry->save();

            return response()->json([
                $tender->image,
                $auction->image,
                $inquiry->image,
            ]);
        } else {
            return response()->json(['message' => 'مشکلی پیش آمده لطفا مجددا تلاش کنید']);
        }
    }

}
