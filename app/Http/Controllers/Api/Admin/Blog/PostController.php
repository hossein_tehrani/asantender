<?php

namespace App\Http\Controllers\Api\Admin\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function store(PostRequest $request)
    {
        $validatedData=$request->validated();
        if (Post::create($validatedData)) {
            return response()->json(['message' => 'پست شما  منتشر شد']);
        } else {
            return response()->json(['message' => 'مشکلی پیش آمده ، لطفا ورودی های فرم را کنترل کنبد']);
        }
    }

    public function update(PostRequest $request)
    {
        $validatedData = $request->validated();
        $post=Post::find($request->id);
        $post->update($validatedData);
        return response()->json(['message' => 'پست مورد نظر ویرایش گردید']);
    }

    public function delete($id)
    {
        $post = Post::find($id);
        try {
            $post->delete();
        } catch (\Exception $e) {
        }
        return response()->json(['message' => 'پست مورد نظر حذف گردید']);
    }
}
