<?php

namespace App\Http\Controllers\Api\Admin\Advertiser;

use App\Exports\TenderExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\TenderRequest;
use App\Http\Resources\CentralOrganResource;
use App\Http\Resources\WorkGroupResource;
use App\Imports\TenderImport;
use App\Models\CentralOrgan;
use App\Models\Tender;
use App\Models\WorkGroup;
use Excel;
use Illuminate\Http\Request;
use Morilog\Jalali\Jalalian;

class TenderController extends Controller
{
    public function single_adv($id)
    {
        $tender=Tender::findOrFail($id);
        return response()->json(['data'=>$tender]);
    }
    // you can add new tender with this Function,your Data comes from  Request/tenderRequest --- validatedData is a variable of our inputs ! and i changed our Date Formats from Jalalian To InterNationalTime INT -- be careful about / in datetime format ! this not - !

    public function getStore()
    {
        $central_organs = CentralOrgan::all();
        $parent_work_groups= WorkGroup::where('parent_id', '')->get();
        $work_groups = WorkGroup::all();

        return view('admin.advertiser.tender.create', ['central_organs' => $central_organs, 'work_groups' => $work_groups, 'parent_work_groups' => $parent_work_groups]);
    }
//you can get specific tender

    public function store(TenderRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['invitation_date'] = $request->invitation_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->invitation_date)->toCarbon();
        $validatedData['document_deadline_date'] = $request->document_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->document_deadline_date)->toCarbon();
        $validatedData['request_deadline_date'] = $request->request_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->request_deadline_date)->toCarbon();
        $validatedData['winner_announced_date'] = $request->winner_announced_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->winner_announced_date)->toCarbon();
        $validatedData['initial_publish_date'] = $request->initial_publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->initial_publish_date)->toCarbon();
        $validatedData['publish_date'] = $request->publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->publish_date)->toCarbon();
        $tender = Tender::create($validatedData);
        $tender->workgroups()->sync($request->work_groups);
        return response()->json(['message' => 'آگهی مناقصه مورد نظر ثبت گردید']);
    }
    //update or edit any Records with unique Id of tender

    public function getUpdate($id)
    {
        $tender = Tender::find($id);
        return response()->json($tender);
    }

    public function update(TenderRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['invitation_date'] = $request->invitation_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->invitation_date)->toCarbon();
        $validatedData['document_deadline_date'] = $request->document_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->document_deadline_date)->toCarbon();
        $validatedData['request_deadline_date'] = $request->request_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->request_deadline_date)->toCarbon();
        $validatedData['winner_announced_date'] = $request->winner_announced_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->winner_announced_date)->toCarbon();
        $validatedData['initial_publish_date'] = $request->initial_publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->initial_publish_date)->toCarbon();
        $validatedData['publish_date'] = $request->publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->publish_date)->toCarbon();
        $tender = Tender::find($request->id);
        $tender->update($validatedData);
        $tender->workgroups()->sync($request->work_groups);
        return response()->json(['message' => 'آگهی مناقصه مورد نظر ویرایش گردید']);
    }
    //function for Hard Delete -- (please Dont try to SoftDelete this Table)

    public function delete($id)
    {
        $tender = Tender::find($id);
        $tender->workgroups()->detach();
        try {
            $tender->delete();
        } catch (\Exception $e) {
        }
        return response()->json(['message' => 'آگهی مناقصه مورد نظر حذف گردید']);
    }
    public function import()
    {
        Excel::import(new TenderImport, request()->file('your_file'));

    }
    public function export()
    {
        return Excel::download(new TenderExport, 'TENDERS.xlsx');
    }
}

