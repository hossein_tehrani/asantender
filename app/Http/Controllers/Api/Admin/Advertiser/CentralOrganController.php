<?php

namespace App\Http\Controllers\Api\Admin\Advertiser;

use App\Exports\CentralOrganExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\CentralOrganRequest;
use App\Imports\CentralOrganImport;
use App\Models\CentralOrgan;
use Excel;
use Illuminate\Http\Request;

class CentralOrganController extends Controller
{
    public function store(CentralOrganRequest $request)
    {
        $validatedData=$request->validated();
        CentralOrgan::create($validatedData);
        return response()->json(['message'=>'دستگاه مرکزی مورد نظر ثبت گردید']);
    }
    public function getUpdate($id)
    {
        $central_organ=CentralOrgan::find($id);
        return view('admin.edit',['central_organ'=>$central_organ]);
    }
    public function update(CentralOrganRequest $request)
    {
        $validatedData=$request->validated();
        CentralOrgan::find($request->id)->update($validatedData);
        return response()->json(['message'=>'دستگاه مرکزی مورد نظر ویرایش گردید']);
    }
    public function delete($id)
    {
        try {
            CentralOrgan::find($id)->delete();
        } catch (\Exception $e) {
        }
        return response()->json(['message'=>'دستگاه مرکزی مورد نظر حذف گردید']);
    }
    public function import()
    {
        Excel::import(new CentralOrganImport, request()->file('your_file'));

    }
    public function export()
    {
        return Excel::download(new CentralOrganExport, 'CentralOrgan.xlsx');
    }
}
