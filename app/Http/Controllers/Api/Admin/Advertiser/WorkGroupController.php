<?php

namespace App\Http\Controllers\Api\Admin\Advertiser;

use App\Exports\WorkGroupExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\WorkGroupRequest;
use App\Imports\AuctionImport;
use App\Imports\WorkGroupImport;
use App\Models\WorkGroup;
use Excel;
use Illuminate\Http\Request;

class WorkGroupController extends Controller
{
    public function store(WorkGroupRequest $request)
    {
        $validatedData=$request->validated();
        WorkGroup::create($validatedData);
        return response()->json(['message'=>'گروه کاری مورد نظر ایجاد گردید']);
    }
    public function getUpdate($id){
        $work_group=WorkGroup::find($id);
        return view('admin.edit',['work_group'=>$work_group]);
    }
    public function update(WorkGroupRequest $request)
    {
        $validatedData=$request->validated();
        WorkGroup::find($request->id)->update($validatedData);
        return response()->json(['message'=>'گروه کاری مورد نظر ویرایش گردید']);
    }
    public function delete($id)
    {
        try {
            WorkGroup::find($id)->delete();
        } catch (\Exception $e) {
        }
        return response()->json(['message'=>'گروه کاری مورد نظر حذف گردید']);
    }
    public function import()
    {
        Excel::import(new WorkGroupImport, request()->file('your_file'));

    }
    public function export()
    {
        return Excel::download(new WorkGroupExport, 'WorkGroup.xlsx');
    }

}
