<?php

namespace App\Http\Controllers\Api\Admin\Advertiser;

use App\Exports\CentralOrganExport;
use App\Exports\InquiryExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\InquiryRequest;
use App\Http\Resources\CentralOrganResource;
use App\Http\Resources\WorkGroupResource;
use App\Imports\InquiryImport;
use App\Models\CentralOrgan;
use App\Models\Inquiry;
use App\Models\WorkGroup;
use Excel;
use Illuminate\Http\Request;
use Morilog\Jalali\Jalalian;

class InquiryController extends Controller
{
    public function single_adv($id)
    {
        $inquiry=Inquiry::findOrFail($id);
        return response()->json(['data'=>$inquiry]);
    }
    // you can add new inquiry with this Function,your Data comes from  Request/inquiryRequest --- validatedData is a variable of our inputs ! and i changed our Date Formats from Jalalian To InterNationalTime INT -- be careful about / in datetime format ! this not - !

    public function store(InquiryRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['invitation_date']=$request->invitation_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->invitation_date)->toCarbon();
        $validatedData['submission_deadline_date']=$request->submission_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->submission_deadline_date)->toCarbon();
        $validatedData['minimum_price_validity_date']=$request->minimum_price_validity_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->minimum_price_validity_date)->toCarbon();
        $validatedData['requirement_date']=$request->requirement_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->requirement_date)->toCarbon();
        $validatedData['publish_date']=$request->publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->publish_date)->toCarbon();
        $validatedData['initial_publish_date']=$request->initial_publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->initial_publish_date)->toCarbon();
        $inquiry= Inquiry::create($validatedData);
        $inquiry->workgroups()->sync($request->work_groups);
        return response()->json(['message'=>'آگهی استعلام مورد نظر ثبت گردید']);
    }
//you can get specific inquiry

    public function getUpdate($id)
    {
        $inquiry=Inquiry::find($id);
        return response()->json($inquiry);
    }
    //update or edit any Records with unique Id of inquiry

    public function update(InquiryRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['invitation_date']=$request->invitation_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->invitation_date)->toCarbon();
        $validatedData['submission_deadline_date']=$request->submission_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->submission_deadline_date)->toCarbon();
        $validatedData['minimum_price_validity_date']=$request->minimum_price_validity_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->minimum_price_validity_date)->toCarbon();
        $validatedData['requirement_date']=$request->requirement_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->requirement_date)->toCarbon();
        $validatedData['publish_date']=$request->publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->publish_date)->toCarbon();
        $validatedData['initial_publish_date']=$request->initial_publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->initial_publish_date)->toCarbon();
        $inquiry= Inquiry::find($request->id);
        $inquiry->update($validatedData);
        $inquiry->workgroups()->sync($request->work_groups);
        return response()->json(['message'=>'آگهی استعلام مورد نظر ثبت گردید']);
    }
    //function for Hard Delete -- (please Dont try to SoftDelete this Table)
    public function delete($id)
    {
        $inquiry=Inquiry::find($id);
        $inquiry->workgroups()->detach();
        try {
            $inquiry->delete();
        } catch (\Exception $e) {
        }
        return response()->json(['message'=>'آگهی استعلام مورد نظر حذف گردید']);
    }
    public function import()
    {
        Excel::import(new InquiryImport, request()->file('your_file'));

    }
    public function export()
    {
        return Excel::download(new InquiryExport, 'Inquiry.xlsx');
    }
}
