<?php

namespace App\Http\Controllers\Api\Admin\Advertiser;

use App\Exports\AuctionExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuctionRequest;
use App\Http\Resources\AuctionResource;
use App\Http\Resources\CentralOrganResource;
use App\Http\Resources\WorkGroupResource;
use App\Imports\AuctionImport;
use App\Models\Auction;
use App\Models\CentralOrgan;
use App\Models\WorkGroup;
use Excel;
use Morilog\Jalali\Jalalian;
use Illuminate\Http\Request;

class AuctionController extends Controller
{
    public function single_adv($id)
    {
        $auction=Auction::findOrFail($id);
        return response()->json(['data'=>$auction]);
    }
// you can add new Auction with this Function,your Data comes from  Request/AuctionRequest --- validatedData is a variable of our inputs ! and i changed our Date Formats from Jalalian To InterNationalTime INT -- be careful about / in datetime format ! this not - !
    public function store(AuctionRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['invitation_date'] = $request->invitation_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->invitation_date)->toCarbon();
        $validatedData['view_deadline_date'] = $request->view_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->view_deadline_date)->toCarbon();
        $validatedData['request_deadline_date'] = $request->request_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->request_deadline_date)->toCarbon();
        $validatedData['winner_announced_date'] = $request->winner_announced_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->winner_announced_date)->toCarbon();
        $validatedData['initial_publish_date'] = $request->initial_publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->initial_publish_date)->toCarbon();
        $validatedData['publish_date'] = $request->publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->publish_date)->toCarbon();
        if ($auction = Auction::create($validatedData)) {
            $auction->workgroups()->sync($request->work_groups);
            return response()->json(['message' => 'آگهی مزایده مورد نظر ثبت گردید']);
        } else {
            return response()->json(['message' => 'مشکلی پیش آمده ، لطفا ورودی های فرم را کنترل کنبد']);
        }

    }
//you can get specific Auction
    public function getUpdate($id)
    {
        $auction = Auction::find($id);
        return response()->json($auction);
    }
//update or edit any Records with unique Id of Auction
    public function update(AuctionRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['invitation_date'] = $request->invitation_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->invitation_date)->toCarbon();
        $validatedData['view_deadline_date'] = $request->view_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->view_deadline_date)->toCarbon();
        $validatedData['request_deadline_date'] = $request->request_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->request_deadline_date)->toCarbon();
        $validatedData['winner_announced_date'] = $request->winner_announced_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->winner_announced_date)->toCarbon();
        $validatedData['initial_publish_date'] = $request->initial_publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->initial_publish_date)->toCarbon();
        $validatedData['publish_date'] = $request->publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->publish_date)->toCarbon();
        $auction = Auction::find($request->id);
        $auction->update($validatedData);
        $auction->workgroups()->sync($request->work_groups);
        return response()->json(['message' => 'آگهی مزایده مورد نظر ویرایش گردید']);
    }
//function for Hard Delete -- (please Dont try to SoftDelete this Table)
    public function delete($id)
    {
        $auction = Auction::find($id);
        $auction->workgroups()->detach();
        try {
            $auction->delete();
        } catch (\Exception $e) {
        }
        return response()->json(['message' => 'آگهی مزایده مورد نظر حذف گردید']);
    }

    public function import()
    {
        Excel::import(new AuctionImport, request()->file('your_file'));

    }
    public function export()
    {
        return Excel::download(new AuctionExport, 'Auctions.xlsx');
    }
}
