<?php

namespace App\Http\Controllers\Api\Admin\Discount;

use App\Http\Controllers\Controller;
use App\Http\Requests\DiscountRequest;
use App\Models\DiscountCode;
use Illuminate\Http\Request;

class DiscountCodeController extends Controller
{
    public function store(DiscountRequest $request)
    {
        $validatedData = $request->validated();
        if (DiscountCode::create($validatedData)) {
            return response()->json(['message' => 'کد تخفیف شما ثبت گردید']);
        }
        return response()->json(['message' => 'کد تخفیف شما ثبت نشد لطفا ورودی ها را چک کنید']);

    }

    public function getUpdate($id)
    {
        $discountCode = DiscountCode::find($id);
        return response()->json(['data' => $discountCode]);
    }

    public function update(DiscountRequest $request)
    {
        $validatedData = $request->validated();
        if (DiscountCode::find($request->id)->update($validatedData)) {
            return response()->json(['message' => 'کد تخفیف مورد نظر ویرایش گردید']);
        }
        return response()->json(['message' => 'کد تخفیف مورد نظر ویرایش نشد ، لطفا ورودی ها را چک کنید']);

    }

    public function delete($id)
    {
        try {
            DiscountCode::find($id)->delete();
        } catch (\Exception $e) {
        }
        return response()->json(['message' => 'کد تخفیف مورد نظر حذف گردید']);

    }
}
