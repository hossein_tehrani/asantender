<?php

namespace App\Http\Controllers\Api\Admin\Payment;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PaymentController extends Controller
{
    private $rejectUrl = "https://asantender.com/sale-plan";
    private $redirectUrl = "https://asantender.com/success-order/";
    private $errorUrl = "https://asantender.com/reject-order/",
        $mobile, $subId;
    private $terminalID = '02045649';
    private $password = '17BAA3320B083015';
    private $acceptorId = '992180002045649';
    private $pub_key = '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCLLskOEYGJ7gYJMY/B42CqKTsV
lsyvIhw1y3CD3ZC8AlGrP5oqU3Ai+1a1+DQqEBRmtD9pPoYcOD+zCI0pbUvUmYxk
xP06IuE22Lewy8OpIKehKNGLNvbMmnNgZvE7QoxSL5+/pjiS5koHj+cenBsDKCfK
L68xPL9BqzH2cU1odwIDAQAB
-----END PUBLIC KEY-----';


    public function generateAuthenticationEnvelope($pub_key, $terminalID, $password, $amount)
    {
        $data = $terminalID . $password . str_pad($amount, 12, '0', STR_PAD_LEFT) . '00';
        $data = hex2bin($data);
        $AESSecretKey = openssl_random_pseudo_bytes(16);
        $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($data, $cipher, $AESSecretKey, $options = OPENSSL_RAW_DATA, $iv);
        $hmac = hash('sha256', $ciphertext_raw, true);
        $crypttext = '';

        openssl_public_encrypt($AESSecretKey . $hmac, $crypttext, $pub_key);

        return array(
            "data" => bin2hex($crypttext),
            "iv" => bin2hex($iv),
        );
    }

    public function ToBank(Request $request)
    {
        $subscription = Subscription::query()->where("id", "=", $request["sub_id"])->first();
        $user = User::query()->where("mobile", "=", $request["mobile"])->first();
        $invoice = Invoice::query()->create([
            "user_id" => $user->id,
            "subscription_id" => $subscription->id
        ]);
        $amount = (int)$subscription->cost * 10;
        $token = $this->generateAuthenticationEnvelope($this->pub_key, $this->terminalID, $this->password, $amount);
        $data = [];
        $data["request"] = [
            "acceptorId" => $this->acceptorId,
            "amount" => $amount,
            "billInfo" => null,
//            "additionalParameters" => $params,
            "paymentId" => (string)$invoice["id"],
            "requestId" => uniqid(),
            "requestTimestamp" => time(),
            "revertUri" => "https://api.asantender.com/api/site/revert",
            "terminalId" => $this->terminalID,
            "transactionType" => "Purchase"
        ];
        $data['authenticationEnvelope'] = $token;
        $data_string = json_encode($data);
        $ch = curl_init('https://ikc.shaparak.ir/api/v3/tokenization/make');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string)
        ));


        $result = curl_exec($ch);

        curl_close($ch);

        $response = json_decode($result, JSON_OBJECT_AS_ARRAY);
        if ($response["responseCode"] != "00") {
            return redirect($this->errorUrl.trans($response["responseCode"]));
//            echo $response["description"];
//            exit;
        }

        return view('redirect', compact('response'));

    }

    public function Revert()
    {
        if (isset($_POST['token']) && $_POST['token'] != "") {
            if ($_POST['responseCode'] != "00") {

                return redirect($this->errorUrl.trans("error_bank.".$_POST['responseCode']));
//                echo "failed: code " . $_POST['responseCode'];
//                exit;
            }
            $data = array(
                "terminalId" => $this->terminalID,
                "retrievalReferenceNumber" => $_POST['retrievalReferenceNumber'],
                "systemTraceAuditNumber" => $_POST['systemTraceAuditNumber'],
                "tokenIdentity" => $_POST['token'],
            );

            $data_string = json_encode($data);


            $ch = curl_init('https://ikc.shaparak.ir/api/v3/confirmation/purchase');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            ));


            $result = curl_exec($ch);
            if ($result === false) {
                echo curl_error($ch);
                return redirect($this->rejectUrl);
//                exit;
            }
            curl_close($ch);

            $response = json_decode($result, JSON_OBJECT_AS_ARRAY);

            if ($response['responseCode'] == "00") {

                $invoice = Invoice::query()->where("id", "=", (int)$_POST["paymentId"])->first();
                $invoice->isPaid = 1;
                $invoice->save();

                Salary::query()->create([
                    "user_id" => $invoice->user_id,
                    "subscription_id" => $invoice->subscription_id,
                    "amount" => $_POST["amount"],
                    "date" => Carbon::now()->format('Y-m-d'),
                    "token" => $_POST["token"],
                    'reference_number' => $_POST["retrievalReferenceNumber"],
                    'masked_pan' => $_POST["maskedPan"],
                    'trace_audit_number' => $_POST["systemTraceAuditNumber"],
                    "acceptor_id" => $_POST["acceptorId"],
                    "status" => $_POST["responseCode"],
                ]);

                (new UserProfileController())->takePlane_sale($invoice->user , $invoice->subscription);
                return redirect($this->redirectUrl.$_POST["token"]);
            } else {
                return redirect($this->rejectUrl);
            }
        }
    }
}
