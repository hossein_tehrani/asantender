<?php

namespace App\Http\Controllers\Api\Admin;

use App\Exports\UserExport;
use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Http\Resources\AuctionResource;
use App\Http\Resources\BannerResource;
use App\Http\Resources\CentralOrganResource;
use App\Http\Resources\InquiryResource;
use App\Http\Resources\PlanResource;
use App\Http\Resources\TenderResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\WorkGroupResource;
use App\Models\Auction;
use App\Models\Banner;
use App\Models\CentralOrgan;
use App\Models\Inquiry;
use App\Models\Plan;
use App\Models\Tender;
use App\Models\WorkGroup;
use Excel;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $central_organs=CentralOrganResource::collection(CentralOrgan::all());
        $work_groups=WorkGroupResource::collection(WorkGroup::all());
        $work_groups_parent=Workgroup::where('parent_id', '')->get();
        $inquiries=InquiryResource::collection(Inquiry::paginate(10));
        $auctions=AuctionResource::collection(Auction::paginate(10));
        $tenders=TenderResource::collection(Tender::paginate(10));
        $users=UserResource::collection(User::paginate(10));
        $plans=PlanResource::collection(Plan::all());
        $banners=BannerResource::collection(Banner::paginate(5));
        return [
            'work_groups_parent'=>$work_groups_parent,
            'central_organs'=>$central_organs,
            'work_groups'=>$work_groups,
            'inquiries'=>$inquiries,
            'auctions'=>$auctions,
            'tenders'=>$tenders,
            'users'=>$users,
            'plans'=>$plans,
            'banners'=>$banners
        ];
    }
    public function getAdvertiserPanel()
    {
        return view('admin.Advertiser');
    }
    public function export()
    {
        return Excel::download(new UserExport, 'Auctions.xlsx');
    }
}
