<?php

namespace App\Http\Controllers\Api\Admin\Plan;

use App\Http\Controllers\Controller;
use App\Http\Requests\PlanRequest;
use App\Models\DiscountCode;
use App\Models\Order;
use App\Models\Plan;
use Auth;
use Carbon\Carbon;
use App\Traits\Response;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    private $rejectUrl = "https://asantender.com/plans";
    private $redirectUrl = "https://asantender.com/success-order/";
    private $errorUrl = "https://asantender.com/reject-order/",
        $mobile, $subId;
    private $terminalID = '02045649';
    private $password = '17BAA3320B083015';
    private $acceptorId = '992180002045649';
    private $pub_key = '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCLLskOEYGJ7gYJMY/B42CqKTsV
lsyvIhw1y3CD3ZC8AlGrP5oqU3Ai+1a1+DQqEBRmtD9pPoYcOD+zCI0pbUvUmYxk
xP06IuE22Lewy8OpIKehKNGLNvbMmnNgZvE7QoxSL5+/pjiS5koHj+cenBsDKCfK
L68xPL9BqzH2cU1odwIDAQAB
-----END PUBLIC KEY-----';
    use Response;

    public function generateAuthenticationEnvelope($pub_key, $terminalID, $password, $amount)
    {
        $data = $terminalID . $password . str_pad($amount, 12, '0', STR_PAD_LEFT) . '00';
        $data = hex2bin($data);
        $AESSecretKey = openssl_random_pseudo_bytes(16);
        $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($data, $cipher, $AESSecretKey, $options = OPENSSL_RAW_DATA, $iv);
        $hmac = hash('sha256', $ciphertext_raw, true);
        $crypttext = '';
        openssl_public_encrypt($AESSecretKey . $hmac, $crypttext, $pub_key);

        return array(
            "data" => bin2hex($crypttext),
            "iv" => bin2hex($iv),
        );
    }

    public function index()
    {
        $plans = Plan::where('status', 1)->get()->toArray();
        return $this->successResponse('plans.index', $plans, 200);
    }

    public function checkDiscountCode(Request $request)
    {
        $plan = Plan::with('discountcodes')->findOrFail($request->id);
        $verifiedCode = $plan->discountcodes;
        foreach ($verifiedCode as $item) {
            if ($item->serial == $request->discount_code) {
                return $this->successResponse('discounts.valid',$item);
            } else {
                return $this->errorResponse('discounts.invalid', 400);
            }
        }
    }
    public function buy(Request $request)
    {
        $user = Auth::user();
        $plan = Plan::findorfail($request->id);
        if ($request->has('discount_code')) {
            $discounts = DiscountCode::all();
            foreach ($discounts as $discount) {
                if ($request->discount_code == $discount->serial) {
                    $plan->price = ($plan->price) - ($plan->price * $discount->percent / 100);
                    $order = new Order([
                        'user_id' => $user->id,
                        'plan_id' => $plan->id,
                        'title' => 'ثبت ' . $plan->title,
                        'price' => $plan->price,
                        'description' => $plan->title . 'با کد نخفیف ' . $discount->serial . 'با تیتر ' . $discount->name . 'ثبت شد ',
                        'has_discount' => true,
                        'status' => false,
                    ]);
                    $user->subscription_count += 1;
                    $user->subscription_date = Carbon::today()->toDateString();
                    $user->subscription_title = $plan->title;
                    if ($plan->count > 0) {
                        $user->count_adv += $plan->count;
                    }
                    $user->work_groups_changes = 0;
                    $order->status=true;
                    $order->save();
                    $user->save();
                    $user->orders()->save($order);
                    $user->plans()->save($plan);
                    return $this->successResponse('buy.successfully', $plan, 200);
                }
            }
            foreach ($discounts as $discount) {
                if ($request->discount_code != $discount->serial) {
                    return $this->errorResponse('discounts.invalid', 400);
                }
            }
        } elseif (!$request->has('discount_code')) {
            $order = new Order([
                'user_id' => $user->id,
                'plan_id' => $plan->id,
                'title' => 'ثبت پلن ' . $plan->title,
                'price' => $plan->price,
                'description' => $plan->title . ' بدون تخفیف ' . ' ثبت شد ',
                'has_discount' => false,
                'status' => false,
            ]);
            $user->subscription_count += 1;
            $user->subscription_date = Carbon::today()->toDateString();
            $user->subscription_title = $plan->title;
            if ($plan->count > 0) {
                $user->count_adv += $plan->count;
            }
            $user->work_groups_changes = 0;
            $order->status=true;
            $order->save();
            $user->save();
            $user->orders()->save($order);
            $user->plans()->save($plan);
            return $this->successResponse('buy.successfully', $plan, 200);
        }
    }



/*    public function buy(Request $request)
    {
        $user = Auth::user();
        $plan = Plan::findorfail($request->id);
        if ($request->has('discount_code')) {
            $discounts = DiscountCode::all();
            foreach ($discounts as $discount) {
                if ($request->discount_code == $discount->serial) {
                    $order = new Order([
                        'user_id' => $user->id,
                        'plan_id' => $plan->id,
                        'title' => 'ثبت ' . $plan->title,
                        'price' => $plan->price,
                        'description' => $plan->title . 'با کد نخفیف ' . $discount->serial . 'با تیتر ' . $discount->name . 'ثبت شد ',
                        'has_discount' => true,
                        'status' => false,
                    ]);
                    $user->orders()->save($order);
                    $plan->price = ($plan->price) - ($plan->price * $discount->percent / 100);
                    $amount = (int)$plan->price * 10;
                    $token = $this->generateAuthenticationEnvelope($this->pub_key, $this->terminalID, $this->password, $amount);
                    $data = [];
                    $data["request"] = [
                        "acceptorId" => $this->acceptorId,
                        "amount" => $amount,
                        "billInfo" => null,
                        "paymentId" => (string)$order->id,
                        "requestId" => uniqid(),
                        "requestTimestamp" => time(),
                        "revertUri" => "https://api.asantender.com/dashboard/revert",
                        "terminalId" => $this->terminalID,
                        "transactionType" => "Purchase"
                    ];
                    $data['authenticationEnvelope'] = $token;
                    $data_string = json_encode($data);
                    $ch = curl_init('https://ikc.shaparak.ir/api/v3/tokenization/make');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string)
                    ));
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $response = json_decode($result, JSON_OBJECT_AS_ARRAY);
                    if ($response["responseCode"] != "00") {
                        return redirect()->to($this->errorUrl, ['responseCode' => $response["responseCode"]]);
                    }
                }
            }
            foreach ($discounts as $discount) {
                if ($request->discount_code != $discount->serial) {
                    return $this->errorResponse('discounts.invalid', 400);
                }
            }
        } elseif (!$request->has('discount_code')) {
            $order = new Order([
                'user_id' => $user->id,
                'plan_id' => $plan->id,
                'title' => 'ثبت پلن ' . $plan->title,
                'price' => $plan->price,
                'description' => $plan->title . ' بدون تخفیف ' . ' ثبت شد ',
                'has_discount' => false,
                'status' => false,
            ]);
            $user->orders()->save($order);
            $amount = (int)$plan->price * 10;
            $token = $this->generateAuthenticationEnvelope($this->pub_key, $this->terminalID, $this->password, $amount);
            $data = [];
            $data["request"] = [
                "acceptorId" => $this->acceptorId,
                "amount" => $amount,
                "billInfo" => null,
                "paymentId" => (string)$order->id,
                "requestId" => uniqid(),
                "requestTimestamp" => time(),
                "revertUri" => "https://api.asantender.com/dashboard/revert",
                "terminalId" => $this->terminalID,
                "transactionType" => "Purchase"
            ];
            $data['authenticationEnvelope'] = $token;
            $data_string = json_encode($data);
            $ch = curl_init('https://ikc.shaparak.ir/api/v3/tokenization/make');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            ));
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, JSON_OBJECT_AS_ARRAY);
            if ($response["responseCode"] != "00") {
                return redirect()->to($this->errorUrl, ['responseCode' => $response["responseCode"]]);
            }
        }
    }

    public function Revert()
    {
        $user = Auth::user();
        if (isset($_POST['token']) && $_POST['token'] != "") {
            if ($_POST['responseCode'] != "00") {
                return redirect($this->errorUrl, ['message' => $_POST['responseCode']]);
            }
            $data = array(
                "terminalId" => $this->terminalID,
                "retrievalReferenceNumber" => $_POST['retrievalReferenceNumber'],
                "systemTraceAuditNumber" => $_POST['systemTraceAuditNumber'],
                "tokenIdentity" => $_POST['token'],
            );
            $data_string = json_encode($data);
            $ch = curl_init('https://ikc.shaparak.ir/api/v3/confirmation/purchase');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            ));
            $result = curl_exec($ch);
            if ($result === false) {
                echo curl_error($ch);
                return redirect($this->rejectUrl);
            }
            curl_close($ch);
            $response = json_decode($result, JSON_OBJECT_AS_ARRAY);
            if ($response['responseCode'] == "00") {
                $order=Order::findOrFail((int)$_POST["paymentId"]);
                $plan=Order::findOrFail($order->plan_id);
                $user->subscription_count += 1;
                $user->subscription_date = Carbon::today()->toDateString();
                $user->subscription_title = $plan->title;
                if ($plan->count > 0) {
                    $user->count_adv += $plan->count;
                }
                $user->work_groups_changes = 0;
                $order->status=true;
                $order->save();
                $user->save();
                $user->plans()->save($plan);
                return $this->successResponse('buy.successfully', $plan, 200);
            } else {
                return redirect($this->rejectUrl);
            }
        }
    }*/

    public function store(PlanRequest $request)
    {
        $validatedData = $request->validated();
        Plan::create($validatedData);
        return response()->json(['message' => 'پلن مورد نظر ثبت گردید']);
    }


    public function delete($id)
    {
        $plan = Plan::find($id);
        try {
            $plan->delete();
        } catch (\Exception $e) {
        }
        return response()->json(['message' => 'پلن مورد نظر حذف گردید']);
    }
}
