<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\Auction;
use App\Models\CentralOrgan;
use App\Models\Inquiry;
use App\Models\Plan;
use App\Models\Tender;
use App\Models\User;
use App\Models\WorkGroup;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Traits\Response;

class DashboardController extends Controller
{
    use Response;

    public function index()
    {
        $user = Auth::user();
        $purchased_plans = $user->plans();
        $my_plans= $purchased_plans->get()->toArray();
        $plans = Plan::all();
        $central_organs = CentralOrgan::all();
        $inquiry_work_groups = WorkGroup::where('type', 'INQUIRY')->where('parent_id', '')->get()->toArray();
        $tender_work_groups = WorkGroup::where('type', 'TENDER')->where('parent_id', '')->get()->toArray();
        $auction_work_groups = WorkGroup::where('type', 'AUCTION')->where('parent_id', '')->get()->toArray();
        $workGroups = WorkGroup::where('parent_id', '!=', '')->get()->toArray();
        $user_work_groups_ids = Auth::user()->workgroups()->pluck('work_group_id')->toArray();
        $my_work_groups = WorkGroup::whereIn('id', $user_work_groups_ids)->get()->toArray();
        $selectet_my_work_groups = 0;
        if (isset($my_work_groups)) {
            $selectet_my_work_groups = count($my_work_groups);
        }
        $last_plan=$purchased_plans->get()->last();
        $count_work_groups_limit=0;
        if(isset($last_plan)){$count_work_groups_limit=$last_plan->work_group_limit;}
        $remaining_change_work_group=0;
        if(isset($last_plan)){$remaining_change_work_group=$last_plan->work_group_change_limit - $user->work_groups_changes;}
        $auctions = Auction::whereHas('workgroups', function ($query) use ($user_work_groups_ids) {
            $query->whereIn('work_group_id', $user_work_groups_ids);
        })->get()->toArray();
        $tenders = Tender::whereHas('workgroups', function ($query) use ($user_work_groups_ids) {
            $query->whereIn('work_group_id', $user_work_groups_ids);
        })->get()->toArray();
        $inquiries = Inquiry::whereHas('workgroups', function ($query) use ($user_work_groups_ids) {
            $query->whereIn('work_group_id', $user_work_groups_ids);
        })->get()->toArray();
        return response([
            'user' => $user,
            'plans' => $plans,
            'purchased_plans' => $purchased_plans,
            'my_plans' => $my_plans,
            'central_organs' => $central_organs,
            'my_work_groups' => $my_work_groups,
            'work_groups' => $workGroups,
            'auctions' => $auctions,
            'tenders' => $tenders,
            'inquiries' => $inquiries,
            'auction_work_groups' => $auction_work_groups,
            'tender_work_groups' => $tender_work_groups,
            'inquiry_work_groups' => $inquiry_work_groups,
            'selectet_my_work_groups' => $selectet_my_work_groups,
            'count_work_groups_limit' => $count_work_groups_limit,
            'remaining_change_work_group' => $remaining_change_work_group,
        ]);
    }

    public function show_single_adv($type, $slug)
    {
        $user = Auth::user();
        $plans = $user->plans()->get();
        switch ($type) {
            case 'inquiry':
                if ($plans) {
                    $user_work_groups_ids = $user->workgroups()->pluck('work_group_id')->toArray();
                    foreach ($plans as $item) {
                        if ($item->created_at->addDay($item->time) >= Carbon::today()) {
                            $adv = Inquiry::whereHas('workgroups', function ($query) use ($user_work_groups_ids) {
                                $query->whereIn('work_group_id', $user_work_groups_ids);
                            })->where('slug', $slug)->get()->toArray();
                            if (!count($adv)) {
                                $adv = Inquiry::whereDate('free_date', '<=', Carbon::today()->toDateString())->where('slug', $slug)->get()->toArray();
                                if (!count($adv)) {
                                    $secret = Inquiry::where('slug', $slug)->first()->toArray();
                                    $adv = [];
                                    array_push($adv, ['asantender' => $secret['asantender'], 'title' => $secret['title'], 'city' => $secret['city'], 'initial_publish_date' => $secret['initial_publish_date']]);
                                }
                            }
                        } else {
                            return $this->errorResponse('expired_plans', 400);
                        }
                    }
                } else {
                    $adv = Inquiry::whereDate('free_date', '<=', Carbon::today()->toDateString())->where('slug', $slug)->get()->toArray();
                    if (!count($adv)) {
                        $secret = Inquiry::where('slug', $slug)->first()->toArray();
                        $adv = [];
                        array_push($adv, ['asantender' => $secret['asantender'], 'title' => $secret['title'], 'city' => $secret['city'], 'initial_publish_date' => $secret['initial_publish_date']]);
                    }
                }
                break;
            case 'tender':
                if ($plans) {
                    $user_work_groups_ids = $user->workgroups()->pluck('work_group_id')->toArray();
                    foreach ($plans as $item) {
                        if ($item->created_at->addDay($item->time) >= Carbon::today()) {
                            $adv = Tender::whereHas('workgroups', function ($query) use ($user_work_groups_ids) {
                                $query->whereIn('work_group_id', $user_work_groups_ids);
                            })->where('slug', $slug)->get()->toArray();
                            if (!count($adv)) {
                                $adv = Tender::whereDate('free_date', '<=', Carbon::today()->toDateString())->where('slug', $slug)->get()->toArray();
                                if (!count($adv)) {
                                    $secret = Tender::where('slug', $slug)->first()->toArray();
                                    $adv = [];
                                    array_push($adv, ['asantender' => $secret['asantender'], 'title' => $secret['title'], 'city' => $secret['city'], 'initial_publish_date' => $secret['initial_publish_date']]);
                                }
                            }
                        } else {
                            return $this->errorResponse('expired_plans', 400);
                        }
                    }
                } else {
                    $adv = Tender::whereDate('free_date', '<=', Carbon::today()->toDateString())->where('slug', $slug)->get()->toArray();
                    if (!count($adv)) {
                        $secret = Tender::where('slug', $slug)->first()->toArray();
                        $adv = [];
                        array_push($adv, ['asantender' => $secret['asantender'], 'title' => $secret['title'], 'city' => $secret['city'], 'initial_publish_date' => $secret['initial_publish_date']]);
                    }
                }
                break;
            case 'auction':
                if ($plans) {
                    $user_work_groups_ids = $user->workgroups()->pluck('work_group_id')->toArray();
                    foreach ($plans as $item) {
                        if ($item->created_at->addDay($item->time) >= Carbon::today()) {
                            $adv = Auction::whereHas('workgroups', function ($query) use ($user_work_groups_ids) {
                                $query->whereIn('work_group_id', $user_work_groups_ids);
                            })->where('slug', $slug)->get()->toArray();
                            if (!count($adv)) {
                                $adv = Auction::whereDate('free_date', '<=', Carbon::today()->toDateString())->where('slug', $slug)->get()->toArray();
                                if (!count($adv)) {
                                    $secret = Auction::where('slug', $slug)->first()->toArray();
                                    $adv = [];
                                    array_push($adv, ['asantender' => $secret['asantender'], 'title' => $secret['title'], 'city' => $secret['city'], 'initial_publish_date' => $secret['initial_publish_date']]);
                                }
                            }
                        } else {
                            return $this->errorResponse('expired_plans', 400);
                        }
                    }
                } else {
                    $adv = Auction::whereDate('free_date', '<=', Carbon::today()->toDateString())->where('slug', $slug)->get()->toArray();
                    if (!count($adv)) {
                        $secret = Auction::where('slug', $slug)->first()->toArray();
                        $adv = [];
                        array_push($adv, ['asantender' => $secret['asantender'], 'title' => $secret['title'], 'city' => $secret['city'], 'initial_publish_date' => $secret['initial_publish_date']]);
                    }
                }
                break;
        }
        return $this->successResponse('200', $adv, 200);
    }

    public function choose_work_groups(Request $request)
    {
        $user = Auth::user();
        $plans = $user->plans()->get();
        if (count($plans)) {
            foreach ($plans as $item) {
                if ($item->work_group_change_limit >= $user->work_groups_changes && $item->created_at->addDay($item->time) >= Carbon::today()) {
                    if ($item->work_group_limit >= count($request->workgroups)) {
                        if ($data = $user->workgroups()->sync($request->workgroups)) {
                            $user->update(['work_groups_changes' => $user->work_groups_changes += 1]);
                            return $this->successResponse('work_group_selected', $data, 200);
                        } else {
                            return $this->errorResponse('not_work_group_selected', 400);
                        }
                    } else {
                        return $this->errorResponse('maximum_request_work_groups', 400);
                    }
                } else {
                    return $this->errorResponse('work_groups_change_maximum', 400);
                }
            }
        } else {
            return $this->errorResponse('no_plan', 400);
        }
    }

    public function search(Request $request)
    {
        $user = Auth::user();
        $user_work_groups_ids = $user->workgroups()->pluck('work_group_id')->toArray();
        switch ($request->type) {
            case 'Tender':
                $searchedData = Tender::whereHas('provinces', function ($q) use ($request) {
                    $q->whereIn('province_id', $request->search['provinces']);
                })->whereHas('workgroups', function ($query) use ($user_work_groups_ids) {
                    $query->whereIn('work_group_id', $user_work_groups_ids);
                })->Where(function ($query) use ($request) {
                    $query
                        ->orwhere('title', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('city', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('address', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('invitation_code', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('title_advertiser', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('source_adv', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('body_adv', 'LIKE', '%' . $request->search['words'] . '%');
                })->whereDate($request->search['limit_time_type'], '>=', $request->search['start_date'])
                    ->whereDate($request->search['limit_time_type'], '<=', $request->search['end_date'])
                    ->orderBy($request->search['orderBy'])->paginate($request->search['number']);
                if (count($searchedData)) {
                    return $this->successResponse('search_successful', ['seachedData' => $searchedData], 200);
                } else {
                    return $this->errorResponse('not_search_successful', 400);
                }
            case 'Auction':
                $searchedData = Auction::whereHas('provinces', function ($q) use ($request) {
                    $q->whereIn('province_id', $request->search['provinces']);
                })->Where(function ($query) use ($request) {
                    $query
                        ->orwhere('title', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('city', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('address', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('invitation_code', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('title_advertiser', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('source_adv', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('body_adv', 'LIKE', '%' . $request->search['words'] . '%');
                })->whereDate($request->search['limit_time_type'], '>=', $request->search['start_date'])
                    ->whereDate($request->search['limit_time_type'], '<=', $request->search['end_date'])
                    ->orderBy($request->search['orderBy'])
                    ->paginate($request->search['number']);
                if (count($searchedData)) {
                    return $this->successResponse('search_successful', ['seachedData' => $searchedData], 200);
                } else {
                    return $this->errorResponse('not_search_successful', 400);
                }
            case 'Inquiry':
                $searchedData = Inquiry::whereHas('provinces', function ($q) use ($request) {
                    $q->whereIn('province_id', $request->search['provinces']);
                })->Where(function ($query) use ($request) {
                    $query
                        ->orwhere('title', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('city', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('address', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('invitation_code', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('title_advertiser', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('source_adv', 'LIKE', '%' . $request->search['words'] . '%')
                        ->orWhere('body_adv', 'LIKE', '%' . $request->search['words'] . '%');
                })->whereDate($request->search['limit_time_type'], '>=', $request->search['start_date'])
                    ->whereDate($request->search['limit_time_type'], '<=', $request->search['end_date'])
                    ->orderBy($request->search['orderBy'])
                    ->paginate($request->search['number']);
                if (count($searchedData)) {
                    return $this->successResponse('search_successful', ['seachedData' => $searchedData], 200);
                } else {
                    return $this->errorResponse('not_search_successful', 400);
                }
        }
    }

    public function update(RegisterRequest $request)
    {
        $user = User::findorfail($request->id);
        $validatedData = $request->validated();
        $user->update($validatedData);
    }

    public function select_fav()
    {
        return true;
    }
}
