<?php

namespace App\Http\Controllers\Api\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\BannerRequest;
use App\Http\Requests\PlanRequest;
use App\Models\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    public function store(BannerRequest $request)
    {
        $validatedData=$request->validated();
        Banner::create($validatedData);
        return redirect()->back()->with('BannerSubmitted','بنر تبلیغات شما با موفقیت ثبت گردید');
    }
    public function getUpdate($id)
    {
        $banner=Banner::findOrFail($id);
        return response()->json($banner);
    }

    public function update(BannerRequest $request)
    {
        $validatedData=$request->validated();
        $banner=Banner::findOrFail($request->id);
        $banner->update($validatedData);
        return redirect()->back()->with('BannerUpdated','بنر تبلیغاتی مورد نظر شما ویرایش گردید');
    }

    public function delete($id)
    {
        $banner=Banner::findOrFail($id);
        try {
            $banner->delete();
        } catch (\Exception $e) {
        }
        return redirect()->back()->with('BannerDeleted','بنر تبلیغاتی مورد نظر شما حذف گردید');
    }
}
