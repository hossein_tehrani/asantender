<?php

namespace App\Http\Controllers\Api\Site;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuctionResource;
use App\Http\Resources\InquiryResource;
use App\Http\Resources\TenderResource;
use App\Models\Auction;
use App\Models\CentralOrgan;
use App\Models\Inquiry;
use App\Models\Tender;
use App\Models\WorkGroup;
use Auth;
use App\Traits\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\In;
use Morilog\Jalali\Jalalian;

class SiteController extends Controller
{
    use Response;

    public function getWorkGroups()
    {
        $work_groups = WorkGroup::where('parent_id','' )->orWhere('parent_id',null)->get()->toArray();
        return $this->successResponse(200,['work_groups'=>$work_groups],200);
    }
    public function show_single_adv($type, $slug)
    {
        switch ($type) {
            case 'inquiry':
                $adv = Inquiry::whereDate('free_date', '<=', Carbon::today()->toDateString())->where('slug', $slug)->get()->toArray();
                if (!count($adv)) {
                    $secret = Inquiry::where('slug', $slug)->first()->toArray();
                    $adv = [];
                    array_push($adv, ['asantender' => $secret['asantender'], 'title' => $secret['title'], 'city' => $secret['city'], 'initial_publish_date' => $secret['initial_publish_date'], 'type' => 'sum']);
                }
                break;
            case 'tender':
                $adv = Tender::whereDate('free_date', '<=', Carbon::today()->toDateString())->where('slug', $slug)->get()->toArray();
                if (!count($adv)) {
                    $secret = Tender::where('slug', $slug)->first()->toArray();
                    $adv = [];
                    array_push($adv, ['asantender' => $secret['asantender'], 'title' => $secret['title'], 'city' => $secret['city'], 'initial_publish_date' => $secret['initial_publish_date'], 'type' => 'sum']);
                }
                break;
            case 'auction':
                $adv = Auction::whereDate('free_date', '<=', Carbon::today()->toDateString())->where('slug', $slug)->get()->toArray();
                if (!count($adv)) {
                    $secret = Auction::where('slug', $slug)->first()->toArray();
                    $adv = [];
                    array_push($adv, ['asantender' => $secret['asantender'], 'title' => $secret['title'], 'city' => $secret['city'], 'initial_publish_date' => $secret['initial_publish_date'], 'type' => 'sum']);
                }
                break;
        }
        foreach($adv as $key => $item){
            if(isset($item['invitation_date'])){
                $adv[$key]['invitation_date']=$adv[$key]['invitation_date'] == '' ? null : Jalalian::forge($adv[$key]['invitation_date'])->format('%B %d، %Y');
            }
            if(isset($item['document_deadline_date'])){
                $adv[$key]['document_deadline_date']=$adv[$key]['document_deadline_date'] == '' ? null : Jalalian::forge($adv[$key]['document_deadline_date'])->format('%B %d، %Y');
            }
            if(isset($item['request_deadline_date'])){
                $adv[$key]['request_deadline_date']=$adv[$key]['request_deadline_date'] == '' ? null : Jalalian::forge($adv[$key]['request_deadline_date'])->format('%B %d، %Y');
            }
            if(isset($item['winner_announced_date'])){
                $adv[$key]['winner_announced_date']=$adv[$key]['winner_announced_date'] == '' ? null : Jalalian::forge($adv[$key]['winner_announced_date'])->format('%B %d، %Y');
            }
            if(isset($item['initial_publish_date'])){
                $adv[$key]['initial_publish_date']=$adv[$key]['initial_publish_date'] == '' ? null : Jalalian::forge($adv[$key]['initial_publish_date'])->format('%B %d، %Y');
            }
            if(isset($item['publish_date'])){
                $adv[$key]['publish_date']=$adv[$key]['publish_date'] == '' ? null : Jalalian::forge($adv[$key]['publish_date'])->format('%B %d، %Y');
            }
            if(isset($item['free_date'])){
                $adv[$key]['free_date']=$adv[$key]['free_date'] == '' ? null : Jalalian::forge($adv[$key]['free_date'])->format('%B %d، %Y');
            }
            if(isset($item['view_deadline_date'])){
                $adv[$key]['view_deadline_date']=$adv[$key]['view_deadline_date'] == '' ? null : Jalalian::forge($adv[$key]['view_deadline_date'])->format('%B %d، %Y');
            }
            if(isset($item['submission_deadline_date'])){
                $adv[$key]['submission_deadline_date']=$adv[$key]['submission_deadline_date'] == '' ? null : Jalalian::forge($adv[$key]['submission_deadline_date'])->format('%B %d، %Y');
            }
            if(isset($item['minimum_price_validity_date'])){
                $adv[$key]['minimum_price_validity_date']=$adv[$key]['minimum_price_validity_date'] == '' ? null : Jalalian::forge($adv[$key]['minimum_price_validity_date'])->format('%B %d، %Y');
            }
            if(isset($item['requirement_date'])){
                $adv[$key]['requirement_date']=$adv[$key]['requirement_date'] == '' ? null : Jalalian::forge($adv[$key]['requirement_date'])->format('%B %d، %Y');
            }
        }

        return $this->successResponse('200', $adv, 200);
    }
    public
    function index()
    {
        $tenders=WorkGroup::where('type','TENDER')->where('parent_id','' || null)->get()->toArray();
        $auctions=WorkGroup::where('type','AUCTION')->where('parent_id','' || null)->get()->toArray();
        $inquiries=WorkGroup::where('type','INQUIRY')->where('parent_id','' || null)->get()->toArray();
        $advertisement=[];
        $advertisement['auctions']=Auction::whereDate('free_date', '<', Carbon::today()->toDateString())->paginate(5)->toArray();
        $advertisement['inquiries']=Inquiry::whereDate('free_date', '<', Carbon::today()->toDateString())->paginate(5)->toArray();
        $advertisement['tenders']=Tender::whereDate('free_date', '<', Carbon::today()->toDateString())->paginate(5)->toArray();

        $sa = Auction::whereDate('free_date', '>', Carbon::today()->toDateString())->paginate(5)->toArray()['data'];
        $summarize_auctions = [];
        foreach ($sa as $item) {
            array_push($summarize_auctions, ['id' => $item['id'],'type' => 'auction','asantender' => $item['asantender'], 'title' => $item['title'], 'slug' => $item['slug'],'city' => $item['city'], 'initial_publish_date' => $item['initial_publish_date'], 'created_at' => $item['created_at']]);
        }
        $st = Tender::whereDate('free_date', '>', Carbon::today()->toDateString())->paginate(5)->toArray()['data'];
        $summarize_tenders = [];
        foreach ($st as $item) {
            array_push($summarize_tenders, ['id' => $item['id'],'type' => 'tender','asantender' => $item['asantender'], 'title' => $item['title'], 'slug' => $item['slug'],'city' => $item['city'], 'initial_publish_date' => $item['initial_publish_date'],'created_at' => $item['created_at']]);
        }
        $si = Inquiry::whereDate('free_date', '>', Carbon::today()->toDateString())->paginate(5)->toArray()['data'];
        $summarize_inquiries = [];
        foreach ($si as $item) {
            array_push($summarize_inquiries, ['id' => $item['id'],'type' => 'inquiry','asantender' => $item['asantender'], 'title' => $item['title'], 'slug' => $item['slug'], 'city' => $item['city'], 'initial_publish_date' => $item['initial_publish_date'],'created_at' => $item['created_at']]);
        }
        $all_advertisement= array_merge($summarize_auctions,$summarize_inquiries,$summarize_tenders,$advertisement['tenders']['data'],$advertisement['auctions']['data'],$advertisement['inquiries']['data']);
        $all_advertisement = collect($all_advertisement)->sortBy('created_at')->reverse()->toArray();
        $all_advertisement=array_values($all_advertisement);
        foreach($all_advertisement as $key => $adv){
            if(isset($adv['invitation_date'])){
                $all_advertisement[$key]['invitation_date']=$all_advertisement[$key]['invitation_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['invitation_date'])->format('%B %d، %Y');
            }
            if(isset($adv['document_deadline_date'])){
                $all_advertisement[$key]['document_deadline_date']=$all_advertisement[$key]['document_deadline_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['document_deadline_date'])->format('%B %d، %Y');
            }
            if(isset($adv['request_deadline_date'])){
                $all_advertisement[$key]['request_deadline_date']=$all_advertisement[$key]['request_deadline_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['request_deadline_date'])->format('%B %d، %Y');
            }
            if(isset($adv['winner_announced_date'])){
                $all_advertisement[$key]['winner_announced_date']=$all_advertisement[$key]['winner_announced_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['winner_announced_date'])->format('%B %d، %Y');
            }
            if(isset($adv['initial_publish_date'])){
                $all_advertisement[$key]['initial_publish_date']=$all_advertisement[$key]['initial_publish_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['initial_publish_date'])->format('%B %d، %Y');
            }
            if(isset($adv['publish_date'])){
                $all_advertisement[$key]['publish_date']=$all_advertisement[$key]['publish_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['publish_date'])->format('%B %d، %Y');
            }
            if(isset($adv['free_date'])){
                $all_advertisement[$key]['free_date']=$all_advertisement[$key]['free_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['free_date'])->format('%B %d، %Y');
            }
            if(isset($adv['view_deadline_date'])){
                $all_advertisement[$key]['view_deadline_date']=$all_advertisement[$key]['view_deadline_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['view_deadline_date'])->format('%B %d، %Y');
            }
            if(isset($adv['submission_deadline_date'])){
                $all_advertisement[$key]['submission_deadline_date']=$all_advertisement[$key]['submission_deadline_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['submission_deadline_date'])->format('%B %d، %Y');
            }
            if(isset($adv['minimum_price_validity_date'])){
                $all_advertisement[$key]['minimum_price_validity_date']=$all_advertisement[$key]['minimum_price_validity_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['minimum_price_validity_date'])->format('%B %d، %Y');
            }
            if(isset($adv['requirement_date'])){
                $all_advertisement[$key]['requirement_date']=$all_advertisement[$key]['requirement_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['requirement_date'])->format('%B %d، %Y');
            }
        }
        return $this->successResponse('200',['all_advertisement'=>$all_advertisement,'tenders'=>$tenders,'auctions'=>$auctions,'inquiries'=>$inquiries],200);
    }

    public function advByWorkGroups($id)
    {
        $headWorkGroup=WorkGroup::find($id)->id;
        $workGroupsIds=WorkGroup::where('parent_id',$headWorkGroup)->get()->pluck('id')->toArray();
        $workGroupsIds[]=$headWorkGroup;
        $advertisement['auctions']=Auction::whereHas('workgroups', function ($q) use ($workGroupsIds) {
            $q->whereIn('work_group_id', $workGroupsIds);
        })->whereDate('free_date', '<', Carbon::today()->toDateString())->paginate(5)->toArray();
        $advertisement['inquiries']=Inquiry::whereHas('workgroups', function ($q) use ($workGroupsIds) {
            $q->whereIn('work_group_id', $workGroupsIds);
        })->whereDate('free_date', '<', Carbon::today()->toDateString())->paginate(5)->toArray();
        $advertisement['tenders']=Tender::whereHas('workgroups', function ($q) use ($workGroupsIds) {
            $q->whereIn('work_group_id', $workGroupsIds);
        })->whereDate('free_date', '<', Carbon::today()->toDateString())->paginate(5)->toArray();
        $sa = Auction::whereHas('workgroups', function ($q) use ($workGroupsIds) {
            $q->whereIn('work_group_id', $workGroupsIds);
        })->whereDate('free_date', '>', Carbon::today()->toDateString())->paginate(5)->toArray()['data'];
        $summarize_auctions = [];
        foreach ($sa as $item) {
            array_push($summarize_auctions, ['id' => $item['id'],'type' => 'auction','asantender' => $item['asantender'], 'title' => $item['title'], 'slug' => $item['slug'],'city' => $item['city'], 'initial_publish_date' => $item['initial_publish_date'], 'created_at' => $item['created_at']]);
        }
        $st = Tender::whereHas('workgroups', function ($q) use ($workGroupsIds) {
            $q->whereIn('work_group_id', $workGroupsIds);
        })->whereDate('free_date', '>', Carbon::today()->toDateString())->paginate(5)->toArray()['data'];
        $summarize_tenders = [];
        foreach ($st as $item) {
            array_push($summarize_tenders, ['id' => $item['id'],'type' => 'tender','asantender' => $item['asantender'], 'title' => $item['title'], 'slug' => $item['slug'],'city' => $item['city'], 'initial_publish_date' => $item['initial_publish_date'],'created_at' => $item['created_at']]);
        }
        $si = Inquiry::whereHas('workgroups', function ($q) use ($workGroupsIds) {
            $q->whereIn('work_group_id', $workGroupsIds);
        })->whereDate('free_date', '>', Carbon::today()->toDateString())->paginate(5)->toArray()['data'];
        $summarize_inquiries = [];
        foreach ($si as $item) {
            array_push($summarize_inquiries, ['id' => $item['id'],'type' => 'inquiry','asantender' => $item['asantender'], 'title' => $item['title'], 'slug' => $item['slug'], 'city' => $item['city'], 'initial_publish_date' => $item['initial_publish_date'],'created_at' => $item['created_at']]);
        }
        $all_advertisement= array_merge($summarize_auctions,$summarize_inquiries,$summarize_tenders,$advertisement['tenders']['data'],$advertisement['auctions']['data'],$advertisement['inquiries']['data']);
        $all_advertisement = collect($all_advertisement)->sortBy('created_at')->reverse()->toArray();
        $all_advertisement=array_values($all_advertisement);
        foreach($all_advertisement as $key => $adv){
            if(isset($adv['invitation_date'])){
                $all_advertisement[$key]['invitation_date']=$all_advertisement[$key]['invitation_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['invitation_date'])->format('%B %d، %Y');
            }
            if(isset($adv['document_deadline_date'])){
                $all_advertisement[$key]['document_deadline_date']=$all_advertisement[$key]['document_deadline_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['document_deadline_date'])->format('%B %d، %Y');
            }
            if(isset($adv['request_deadline_date'])){
                $all_advertisement[$key]['request_deadline_date']=$all_advertisement[$key]['request_deadline_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['request_deadline_date'])->format('%B %d، %Y');
            }
            if(isset($adv['winner_announced_date'])){
                $all_advertisement[$key]['winner_announced_date']=$all_advertisement[$key]['winner_announced_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['winner_announced_date'])->format('%B %d، %Y');
            }
            if(isset($adv['initial_publish_date'])){
                $all_advertisement[$key]['initial_publish_date']=$all_advertisement[$key]['initial_publish_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['initial_publish_date'])->format('%B %d، %Y');
            }
            if(isset($adv['publish_date'])){
                $all_advertisement[$key]['publish_date']=$all_advertisement[$key]['publish_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['publish_date'])->format('%B %d، %Y');
            }
            if(isset($adv['free_date'])){
                $all_advertisement[$key]['free_date']=$all_advertisement[$key]['free_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['free_date'])->format('%B %d، %Y');
            }
            if(isset($adv['view_deadline_date'])){
                $all_advertisement[$key]['view_deadline_date']=$all_advertisement[$key]['view_deadline_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['view_deadline_date'])->format('%B %d، %Y');
            }
            if(isset($adv['submission_deadline_date'])){
                $all_advertisement[$key]['submission_deadline_date']=$all_advertisement[$key]['submission_deadline_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['submission_deadline_date'])->format('%B %d، %Y');
            }
            if(isset($adv['minimum_price_validity_date'])){
                $all_advertisement[$key]['minimum_price_validity_date']=$all_advertisement[$key]['minimum_price_validity_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['minimum_price_validity_date'])->format('%B %d، %Y');
            }
            if(isset($adv['requirement_date'])){
                $all_advertisement[$key]['requirement_date']=$all_advertisement[$key]['requirement_date'] == '' ? null : Jalalian::forge($all_advertisement[$key]['requirement_date'])->format('%B %d، %Y');
            }
        }
        return $this->successResponse(200,$all_advertisement,200);
    }
}
