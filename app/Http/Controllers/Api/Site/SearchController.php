<?php

namespace App\Http\Controllers\Api\Site;

use App\Filters\EndDate;
use App\Filters\Province;
use App\Filters\Setad;
use App\Filters\StartDate;
use App\Http\Controllers\Controller;
use App\Models\Auction;
use App\Models\Inquiry;
use App\Models\Tender;
use App\Models\WorkGroup;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use App\Traits\Response;
use Morilog\Jalali\Jalalian;

class SearchController extends Controller
{
    use Response;

    public function tokenize($text, $stopwords = [])
    {
        if (is_string($text)) {
            $text = mb_strtolower($text);
            $split = preg_split($this->getPattern(), $text, -1, PREG_SPLIT_NO_EMPTY);
            return array_diff($split, $stopwords);
        } else {
            $text = boolval($text);
            $text = mb_strtolower($text);
            $split = preg_split($this->getPattern(), $text, -1, PREG_SPLIT_NO_EMPTY);
            return array_diff($split, $stopwords);
        }

    }

    public function search(Request $request)
    {
        if (isset($request->workGroups)){
            $work_groups_id=[];
            $child_work_groups=WorkGroup::whereIn('parent_id',$request->workGroups)->pluck('id')->toArray();
            $work_groups_id=array_merge($child_work_groups,$request->workGroups);
        }
        if (isset($request->start_date)) {
            $start_date = $request->start_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->start_date)->toCarbon()->toDateString();
        }
        if (isset($request->end_date)) {
            $end_date = $request->end_date == '' ? null : Jalalian::fromFormat('Y/m/d', $request->end_date)->toCarbon()->toDateString();
        }
        switch ($request->type) {
            case 'Tender':
                $searchedData = Tender::whereHas('provinces', function ($q) use ($request) {
                    $q->whereIn('province_id', $request->provinces);
                })->whereHas('workgroups' , function ($q) use ($work_groups_id){
                    $q->whereIn('work_group_id',$work_groups_id);
                })
                ->Where(function ($query) use ($request) {
                    $query
                        ->orwhere('title', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('city', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('address', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('invitation_code', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('title_advertiser', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('source_adv', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('body_adv', 'LIKE', '%' . $request->words . '%');
                })->whereDate($request->limit_time_type, '>=', $start_date)
                    ->whereDate($request->limit_time_type, '<=', $end_date)
                    ->whereDate('free_date', '<=', Carbon::today()->toDateString())
                    ->orderBy($request->orderBy)->paginate($request->number);
                if (count($searchedData)) {
                    foreach($searchedData as $key => $adv){
                        if(isset($adv['invitation_date'])){
                            $searchedData[$key]['invitation_date']=$searchedData[$key]['invitation_date'] == '' ? null : Jalalian::forge($searchedData[$key]['invitation_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['document_deadline_date'])){
                            $searchedData[$key]['document_deadline_date']=$searchedData[$key]['document_deadline_date'] == '' ? null : Jalalian::forge($searchedData[$key]['document_deadline_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['request_deadline_date'])){
                            $searchedData[$key]['request_deadline_date']=$searchedData[$key]['request_deadline_date'] == '' ? null : Jalalian::forge($searchedData[$key]['request_deadline_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['winner_announced_date'])){
                            $searchedData[$key]['winner_announced_date']=$searchedData[$key]['winner_announced_date'] == '' ? null : Jalalian::forge($searchedData[$key]['winner_announced_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['initial_publish_date'])){
                            $searchedData[$key]['initial_publish_date']=$searchedData[$key]['initial_publish_date'] == '' ? null : Jalalian::forge($searchedData[$key]['initial_publish_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['publish_date'])){
                            $searchedData[$key]['publish_date']=$searchedData[$key]['publish_date'] == '' ? null : Jalalian::forge($searchedData[$key]['publish_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['free_date'])){
                            $searchedData[$key]['free_date']=$searchedData[$key]['free_date'] == '' ? null : Jalalian::forge($searchedData[$key]['free_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['view_deadline_date'])){
                            $searchedData[$key]['view_deadline_date']=$searchedData[$key]['view_deadline_date'] == '' ? null : Jalalian::forge($searchedData[$key]['view_deadline_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['submission_deadline_date'])){
                            $searchedData[$key]['submission_deadline_date']=$searchedData[$key]['submission_deadline_date'] == '' ? null : Jalalian::forge($searchedData[$key]['submission_deadline_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['minimum_price_validity_date'])){
                            $searchedData[$key]['minimum_price_validity_date']=$searchedData[$key]['minimum_price_validity_date'] == '' ? null : Jalalian::forge($searchedData[$key]['minimum_price_validity_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['requirement_date'])){
                            $searchedData[$key]['requirement_date']=$searchedData[$key]['requirement_date'] == '' ? null : Jalalian::forge($searchedData[$key]['requirement_date'])->format('%B %d، %Y');
                        }
                    }
                    return $this->successResponse('search_successful', ['searchedData' => $searchedData], 200);
                } else {
                    return $this->errorResponse('not_search_successful', 400);
                }
            case 'Auction':
                $searchedData = Auction::whereHas('provinces', function ($q) use ($request) {
                    $q->whereIn('province_id', $request->provinces);
                })->whereHas('workgroups' , function ($q) use ($work_groups_id){
                    $q->whereIn('work_group_id',$work_groups_id);
                })->Where(function ($query) use ($request) {
                    $query
                        ->orwhere('title', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('city', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('address', 'LIKE', '%' . $request->words . '%')
//                        ->orWhere('invitation_code', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('title_advertiser', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('source_adv', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('body_adv', 'LIKE', '%' . $request->words . '%');
                })->whereDate($request->limit_time_type, '>=', $start_date)
                    ->whereDate($request->limit_time_type, '<=', $end_date)
                    ->whereDate('free_date', '<=', Carbon::today()->toDateString())
                    ->orderBy($request->orderBy)
                    ->paginate($request->number);
                if (count($searchedData)) {
                    foreach($searchedData as $key => $adv){
                        if(isset($adv['invitation_date'])){
                            $searchedData[$key]['invitation_date']=$searchedData[$key]['invitation_date'] == '' ? null : Jalalian::forge($searchedData[$key]['invitation_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['document_deadline_date'])){
                            $searchedData[$key]['document_deadline_date']=$searchedData[$key]['document_deadline_date'] == '' ? null : Jalalian::forge($searchedData[$key]['document_deadline_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['request_deadline_date'])){
                            $searchedData[$key]['request_deadline_date']=$searchedData[$key]['request_deadline_date'] == '' ? null : Jalalian::forge($searchedData[$key]['request_deadline_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['winner_announced_date'])){
                            $searchedData[$key]['winner_announced_date']=$searchedData[$key]['winner_announced_date'] == '' ? null : Jalalian::forge($searchedData[$key]['winner_announced_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['initial_publish_date'])){
                            $searchedData[$key]['initial_publish_date']=$searchedData[$key]['initial_publish_date'] == '' ? null : Jalalian::forge($searchedData[$key]['initial_publish_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['publish_date'])){
                            $searchedData[$key]['publish_date']=$searchedData[$key]['publish_date'] == '' ? null : Jalalian::forge($searchedData[$key]['publish_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['free_date'])){
                            $searchedData[$key]['free_date']=$searchedData[$key]['free_date'] == '' ? null : Jalalian::forge($searchedData[$key]['free_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['view_deadline_date'])){
                            $searchedData[$key]['view_deadline_date']=$searchedData[$key]['view_deadline_date'] == '' ? null : Jalalian::forge($searchedData[$key]['view_deadline_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['submission_deadline_date'])){
                            $searchedData[$key]['submission_deadline_date']=$searchedData[$key]['submission_deadline_date'] == '' ? null : Jalalian::forge($searchedData[$key]['submission_deadline_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['minimum_price_validity_date'])){
                            $searchedData[$key]['minimum_price_validity_date']=$searchedData[$key]['minimum_price_validity_date'] == '' ? null : Jalalian::forge($searchedData[$key]['minimum_price_validity_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['requirement_date'])){
                            $searchedData[$key]['requirement_date']=$searchedData[$key]['requirement_date'] == '' ? null : Jalalian::forge($searchedData[$key]['requirement_date'])->format('%B %d، %Y');
                        }
                    }
                    return $this->successResponse('search_successful', ['searchedData' => $searchedData], 200);
                } else {
                    return $this->errorResponse('not_search_successful', 400);
                }
            case 'Inquiry':
                $searchedData = Inquiry::whereHas('provinces', function ($q) use ($request) {
                    $q->whereIn('province_id', $request->provinces);
                })->whereHas('workgroups' , function ($q) use ($work_groups_id){
                    $q->whereIn('work_group_id',$work_groups_id);
                })
                    ->Where(function ($query) use ($request) {
                    $query
                        ->orwhere('title', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('city', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('address', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('invitation_code', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('title_advertiser', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('source_adv', 'LIKE', '%' . $request->words . '%')
                        ->orWhere('body_adv', 'LIKE', '%' . $request->words . '%');
                })->whereDate($request->limit_time_type, '>=', $start_date)
                    ->whereDate($request->limit_time_type, '<=', $end_date)
                    ->whereDate('free_date', '<=', Carbon::today()->toDateString())
                    ->orderBy($request->orderBy)
                    ->paginate($request->number);
                if (count($searchedData)) {
                    foreach($searchedData as $key => $adv){
                        if(isset($adv['invitation_date'])){
                            $searchedData[$key]['invitation_date']=$searchedData[$key]['invitation_date'] == '' ? null : Jalalian::forge($searchedData[$key]['invitation_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['document_deadline_date'])){
                            $searchedData[$key]['document_deadline_date']=$searchedData[$key]['document_deadline_date'] == '' ? null : Jalalian::forge($searchedData[$key]['document_deadline_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['request_deadline_date'])){
                            $searchedData[$key]['request_deadline_date']=$searchedData[$key]['request_deadline_date'] == '' ? null : Jalalian::forge($searchedData[$key]['request_deadline_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['winner_announced_date'])){
                            $searchedData[$key]['winner_announced_date']=$searchedData[$key]['winner_announced_date'] == '' ? null : Jalalian::forge($searchedData[$key]['winner_announced_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['initial_publish_date'])){
                            $searchedData[$key]['initial_publish_date']=$searchedData[$key]['initial_publish_date'] == '' ? null : Jalalian::forge($searchedData[$key]['initial_publish_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['publish_date'])){
                            $searchedData[$key]['publish_date']=$searchedData[$key]['publish_date'] == '' ? null : Jalalian::forge($searchedData[$key]['publish_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['free_date'])){
                            $searchedData[$key]['free_date']=$searchedData[$key]['free_date'] == '' ? null : Jalalian::forge($searchedData[$key]['free_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['view_deadline_date'])){
                            $searchedData[$key]['view_deadline_date']=$searchedData[$key]['view_deadline_date'] == '' ? null : Jalalian::forge($searchedData[$key]['view_deadline_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['submission_deadline_date'])){
                            $searchedData[$key]['submission_deadline_date']=$searchedData[$key]['submission_deadline_date'] == '' ? null : Jalalian::forge($searchedData[$key]['submission_deadline_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['minimum_price_validity_date'])){
                            $searchedData[$key]['minimum_price_validity_date']=$searchedData[$key]['minimum_price_validity_date'] == '' ? null : Jalalian::forge($searchedData[$key]['minimum_price_validity_date'])->format('%B %d، %Y');
                        }
                        if(isset($adv['requirement_date'])){
                            $searchedData[$key]['requirement_date']=$searchedData[$key]['requirement_date'] == '' ? null : Jalalian::forge($searchedData[$key]['requirement_date'])->format('%B %d، %Y');
                        }
                    }
                    return $this->successResponse('search_successful', ['searchedData' => $searchedData], 200);
                } else {
                    return $this->errorResponse('not_search_successful', 400);
                }
        }
    }

}

