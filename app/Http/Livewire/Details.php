<?php

namespace App\Http\Livewire;

use App\Models\Admin;
use App\Models\ConfigAdmin;
use App\Models\Order;
use App\Models\User;
use Livewire\Component;

class Details extends Component
{
    public $users=[];

    public function mount()
    {
        $this->users=User::with('plans','workgroups','tickets','orders')->get();
    }
    public function delete($id)
    {
        $admin = Admin::findorfail($id);
        $admin->delete();
        session()->flash('error','حساب کاربری مورد نظر با موفقیت حذف شد');
    }

    public function update($id)
    {
        $admin = Admin::findorfail($id);
        redirect()->route('AdminUpdate', ['admin' => $admin]);
    }

    public function render()
    {
        $configAdmin = ConfigAdmin::first();
        $admins = Admin::with('tenders','auctions','inquiries','roles')->orderBy('id','asc')->paginate($configAdmin->paginate_show_admins);
        $orders=Order::with('users','plans')->get();
        return view('livewire.details',['admins'=>$admins,'orders'=>$orders]);
    }
}
