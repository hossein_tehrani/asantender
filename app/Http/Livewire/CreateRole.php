<?php

namespace App\Http\Livewire;

use App\Models\Permission;
use App\Models\Role;
use Livewire\Component;

class CreateRole extends Component
{
    public $name , $label , $permissions;
    protected $rules = [
        'name' => 'required|string|min:1',
        'label' => 'string|max:255',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function createRole()
    {
        $role=Role::create($this->validate());
        $role->permissions()->sync($this->permissions);
    }
    public function render()
    {
        $allPermissions=Permission::all();
        $roles=Role::orderBy('id', 'DESC')->get();
        return view('livewire.create-role',['roles'=>$roles,'allPermissions'=>$allPermissions])->layout('layouts.master');
    }
}
