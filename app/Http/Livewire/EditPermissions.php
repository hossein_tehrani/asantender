<?php

namespace App\Http\Livewire;

use App\Models\Permission;
use App\Models\Role;
use Livewire\Component;

class EditPermissions extends Component
{
    public $role , $permissions ;

    public function updatePermission()
    {
        dd(Role::findorfail($this->role->id));
    }

    public function render()
    {
        $allRoles=Role::all();
        $allPermissions=Permission::all();
        return view('livewire.edit-permissions',['allPermissions'=>$allPermissions,'allRoles'=>$allRoles])->layout('layouts.master');
    }
}
