<?php

namespace App\Http\Livewire;

use App\Models\CentralOrgan;
use Livewire\Component;
use Livewire\WithPagination;

class CreateCentralOrgan extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $i = 1,
        $name, $status, $parent_id, $priority;
    protected $rules = [
        'name' => 'required|min:2|max:255',
        'status' => 'max:2',
        'parent_id' => 'max:4',
        'priority' => 'max:4',
    ];

    public function StoreCentralOrgan()
    {
        $this->validate();
        CentralOrgan::create([
            'name' => $this->name,
            'status' => $this->status,
            'parent_id' => $this->parent_id,
            'priority' => $this->priority,
        ]);
    }

    public function toggleActivation($id, $type)
    {
        $co = CentralOrgan::findOrFail($id);
        switch ($type) {
            case 'setActive':
                $co->update(['status' => 1]);
                session()->flash('success','دستگاه مرکزی مورد نظر با موفقیت فعال شد');
                break;
            case 'setDeactive' :
                $co->update(['status' => 0]);
                session()->flash('success','دستگاه مرکزی مورد نظر با موفقیت  غیر فعال شد');
                break;
        }
    }

    public function delete($id)
    {
        $co = CentralOrgan::findOrFail($id);
        $co->delete();
        session()->flash('success','دستگاه مرکزی مورد نظر با موفقیت حذف شد');
    }
    public function render()
    {
        $central_organs = CentralOrgan::paginate(5);
        return view('livewire.create-central-organ', ['central_organs' => $central_organs])->layout('layouts.master');
    }
}
