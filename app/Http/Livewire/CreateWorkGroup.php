<?php

namespace App\Http\Livewire;

use App\Models\WorkGroup;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class CreateWorkGroup extends Component
{
    use WithFileUploads,WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $i=1,
        $title, $image, $type, $status, $priority, $parent_id;
    protected $rules = [
        'title' => 'required|min:2|max:255',
        'type' => 'max:255',
        'status' => 'max:2',
        'priority' => 'max:4',
        'parent_id' => 'max:4',
    ];

    public function mount()
    {
    }

    public function StoreWorkGroup()
    {
        $validatedData = $this->validate();
        if (isset($this->image)) {
            $imageName = 'workgroup' . time() . $this->image->getClientOriginalExtension();
            $path = $this->image->storeAs('workgroup', $imageName);
            $validatedData['image'] = $path;
        }
        WorkGroup::create($validatedData);
        session()->flash('success', 'گروه کاری مورد نظر با موفقیت ساخته شد');
    }

    public function delete($id)
    {
        $swg=WorkGroup::findOrFail($id);
        $swg->delete();
        session()->flash('success','گروه کاری مورد نظر حذف گردید');
    }

    public function toggleActivation($id,$type)
    {
        $swg=WorkGroup::findOrFail($id);
        switch($type){
            case 'setActive' :
                $swg->update(['status' => 1]);
                session()->flash('success','گروه کاری مورد نظر فعال گردید');
                break;
            case 'setDeactive' :
                $swg->update(['status' => 0]);
                session()->flash('success','گروه کاری مورد نظر غیر فعال گردید');
        }
    }

    public function render()
    {
        $work_groups=WorkGroup::paginate(5);
        return view('livewire.create-work-group',['work_groups'=>$work_groups])
            ->layout('layouts.master');
    }
}
