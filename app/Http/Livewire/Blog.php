<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Blog extends Component
{
    public $posts;

    public function mount()
    {
        $this->posts=\App\Models\Blog::all();
    }
    public function render()
    {
        return view('livewire.blog');
    }
}
