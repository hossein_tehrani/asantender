<?php

namespace App\Http\Livewire;

use App\Exports\AuctionExport;
use App\Imports\AuctionImport;
use App\Models\CentralOrgan;
use App\Models\Config;
use App\Models\Province;
use App\Models\Auction;
use App\Models\WorkGroup;
use Carbon\Carbon;
use Excel;
use Illuminate\Support\Facades\Auth;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Livewire\Component;
use Morilog\Jalali\Jalalian;

class CreateAuction extends Component
{
    use withPagination, WithFileUploads;

    public $i=1, $admin_id, $source_adv, $title,$slug,$provinces = [], $city, $commodity_description, $source_auction_num, $base_price,
        $address, $commodity_num, $invitation_code, $auction_code, $title_advertiser, $invitation_date,
        $request_deadline_date, $winner_announced_date, $initial_publish_date,
        $view_deadline_date, $publish_date, $free_date, $status_adv, $img_url, $asantender, $link_url, $body_adv, $setad,
        $central_organ_id, $workGroups = [], $import_excel,$path,
        $search = [], $selected_provinces = [], $search_limit_time_start, $search_limit_time_end;
    protected $rules = [
        'title' => 'required|min:2|max:250',
        'city' => 'min:2|max:250',
        'address' => 'min:2|max:250',
        'auction_code' => 'required|min:1|max:250',
        'title_advertiser' => 'required|min:2|max:250',
        'source_adv' => 'required',
        'setad' => 'boolean',
        'commodity_num' => 'min:1',
        'commodity_description' => 'min:1',
        'base_price' => 'min:1',
        'source_auction_num' => 'sometimes',
        'body_adv' => 'min:5',
        'status_adv' => 'required|boolean',
        'img_url' => 'image|max:2048',
        'link_url' => 'max:255',
        'admin_id' => 'min:1|max:10',
        'central_organ_id' => 'min:1|max:4',
        'provinces' => 'sometimes'
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function mount()
    {
        $this->path = null;
        $this->admin_id = Auth::id();
        $this->status_adv = 0;
        $this->provinces = Province::all();
        $this->ap = Province::pluck('id')->toArray();
        $this->workGroups = [];
        $this->search['auctions'] = null;
        $this->search['limit_time_type'] = 'initial_publish_date';
        $this->search['number'] = 20;
        $this->search['active'] = [0, 1];
        for ($i = 1; $i <= 31; $i++) {
            array_push($this->selected_provinces, $i);
        }
        $this->search['words'] = null;
        $this->search['orderBy'] = 'initial_publish_date';
        $this->search_limit_time_start = '1350/01/01';
        $this->search_limit_time_end = '1499/01/01';
    }

    public function search()
    {
        $startDate = $this->search_limit_time_start == '' ? '1350/01/01' : Jalalian::fromFormat('Y/m/d', $this->search_limit_time_start)->toCarbon();
        $endDate = $this->search_limit_time_end == '' ? '1499/01/01' : Jalalian::fromFormat('Y/m/d', $this->search_limit_time_end)->toCarbon();
        if ($this->selected_provinces) {
            if ($this->selected_provinces[0] == 0) {
                for ($i = 1; $i <= 31; $i++) {
                    array_push($this->selected_provinces, $i);
                }
            }
        }
        switch ($this->search['active']) {
            case 1 :
                $this->search['active'] = [1];
                break;
            case 0 :
                $this->search['active'] = [0];
                break;
            case 2 :
                $this->search['active'] = [0, 1];
                break;
        }
        $this->search['auctions'] = Auction::whereHas('provinces', function ($q) {
            $q->whereIn('province_id', $this->selected_provinces);
        })->Where(function ($query) {
            $query
                ->orwhere('title', 'LIKE', '%' . $this->search['words'] . '%')
                ->orWhere('city', 'LIKE', '%' . $this->search['words'] . '%')
                ->orWhere('address', 'LIKE', '%' . $this->search['words'] . '%')
                ->orWhere('auction_code', 'LIKE', '%' . $this->search['words'] . '%')
                ->orWhere('title_advertiser', 'LIKE', '%' . $this->search['words'] . '%')
                ->orWhere('source_adv', 'LIKE', '%' . $this->search['words'] . '%')
                ->orWhere('body_adv', 'LIKE', '%' . $this->search['words'] . '%');
        })->whereIn('status_adv', $this->search['active'])->whereDate($this->search['limit_time_type'], '>=', $startDate)
            ->whereDate($this->search['limit_time_type'], '<=', $endDate)
            ->orderBy($this->search['orderBy'])->paginate($this->search['number'])->toArray();
    }

    public function ResetFilter()
    {
        $this->reset();
    }

    public function StoreAuction()
    {
        $this->validate();
        if (isset($this->img_url)) {
            $imageName = 'auction' . time() . $this->asantender . '.' . $this->img_url->getClientOriginalExtension();
            $this->path = $this->img_url->storeAs(
                'auction', $imageName
            );
        }
        $auction = Auction::create([
            'invitation_date' => $this->invitation_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->invitation_date)->toCarbon(),
            'view_deadline_date' => $this->view_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->view_deadline_date)->toCarbon(),
            'request_deadline_date' => $this->request_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->request_deadline_date)->toCarbon(),
            'winner_announced_date' => $this->winner_announced_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->winner_announced_date)->toCarbon(),
            'initial_publish_date' => $this->initial_publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->initial_publish_date)->toCarbon(),
            'publish_date' => $this->publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->publish_date)->toCarbon(),
            'free_date' => $this->free_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->free_date)->toCarbon(),
            'title' => $this->title,
//            'slug' => \Str::slug($this->title),
            'city' => $this->city,
            'address' => $this->address,
            'auction_code' => $this->auction_code,
            'title_advertiser' => $this->title_advertiser,
            'commodity_num' => $this->commodity_num,
            'commodity_description' => $this->commodity_description,
            'base_price' => $this->base_price,
            'source_auction_num' => $this->source_auction_num,
            'source_adv' => "$this->source_adv",
            'setad' => $this->setad,
            'status_adv' => $this->status_adv,
            'asantender' => rand(100, 999) . 'A' . Carbon::now()->format('YMDHms') . 'A',
            'body_adv' => $this->body_adv,
            'link_url' => $this->link_url,
            'admin_id' => $this->admin_id,
            'central_organ_id' => $this->central_organ_id,
            'img_url' => $this->path
        ]);
        if ( isset($this->provinces[0]) && $this->provinces[0] == 'all') {
            for ($i = 1; $i <= 31; $i++) {
                unset($this->provinces[0]);
                array_push($this->provinces, $i);
            }
        }
        $auction->workgroups()->sync($this->workGroups);
        $auction->provinces()->sync($this->provinces);
        session()->flash('success','مزایده شماره'.$auction->id.'با موفقیت ثبت شد');

    }

    public function toggleAuction($id, $type)
    {
        $auction = Auction::findOrFail($id);
        switch ($type) {
            case 'Active':
                $auction->update(['status_adv' => 1]);
                break;
            case 'Deactive':
                $auction->update(['status_adv' => 0]);
                break;
        }
    }

    public function delete($id)
    {
        $auction = Auction::findOrFail($id);
        $auction->delete();
        session()->flash('success', 'آگهی با عنوان ' . '"' . $auction->title_advertiser . '"' . ' حذف گردید');
    }

    public function StoreExcel()
    {
        $path1 = $this->import_excel->store('temp');
        $path = storage_path('app') . '/' . $path1;
        Excel::import(new AuctionImport, $path);
    }

    public function ExportExcel()
    {
        return Excel::download(new AuctionExport, 'AUCTIONS.xlsx');
    }

    public function render()
    {
        $config = Config::firstOrFail();
        $allProvinces = Province::all();
        $central_organs = CentralOrgan::all();
        $work_groups = WorkGroup::get()->groupBy('parent_id');
        $work_groups['root'] = $work_groups[''];
        unset($work_groups['']);
        $auctions = Auction::orderBy('created_at', 'desc')->paginate($config->pagination_advertise_admin);
        return view('livewire.create-auction', ['work_groups' => $work_groups, 'allProvinces' => $allProvinces, 'central_organs' => $central_organs, 'auctions' => $auctions, 'config' => $config])
            ->layout('layouts.master');
    }
}
