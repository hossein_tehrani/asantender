<?php

namespace App\Http\Livewire\User;

use App\Models\UserMessage;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Modules\Wallet\Entities\Transaction;
use Modules\Wallet\repo\Wallet\AccountDB;
use Modules\Wallet\repo\Wallet\TransactionDB;
use function App\Helper\toJalali;
use Livewire\WithPagination;

class User extends Component
{
    use WithPagination;
    public $user, $title, $body, $amount,
        $show_locked_button = true, $show_unlocked_button = false, $userDetail;
    protected $paginationTheme="bootstrap";
    protected $rules = [
        'title' => 'required|min:3|max:252',
        'body' => 'required|min:5|max:1578',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submitMessage($userId)
    {
        $this->validate();
        UserMessage::create([
            'title' => $this->title,
            'body' => $this->body,
            'user_id' => $userId
        ]);
        return session()->flash('success', ' پیام شما با موفقیت ارسال شد ');
    }

    public function transferCredit($userAccountId)
    {
        $accountDB = new AccountDB();
        $userAccount = $accountDB->getByUser($userAccountId, null, 'IRR');
        $complexAccount = $accountDB->getComplexAccount();
        (new TransactionDB)->changeCredit($userAccount->id, $complexAccount->id, $this->amount);
        $transactionInstance = Transaction::create([
            'type' => 'admin_transfer',
            'user_id' => $userAccountId,
            'from_account_id' => $complexAccount->id,
            'to_account_id' => $userAccount->id,
            'amount' => $this->amount,
            'admin_id' => Auth::id(),
            'discount_amount' => 0,
            'discount_id' => null,
            'description' => 'انتقال اعتبار توسط ادمین',
            'bank_portal_id' => 1,
            'active' => true,
        ]);
        $md = explode('/', toJalali(date('Y-h-d'))['date']);
        $transactionInstance->update([
            'factor_number' => 'Y' . $md[1] . $md[2] . $userAccountId . $transactionInstance->id,
            'paid_at' => NOW()
        ]);
        return session()->flash('success', ' مبلغ' . $this->amount . ' به کاربر' . $this->user->full_name . ' انتقال یافت  ');

    }

    public function blockUser($userId)
    {
        $user = \App\Models\User::where('id', $userId);
        if ($user->update(['locked' => true])) {
            $this->show_locked_button = false;
            $this->show_unlocked_button = true;
            return session()->flash('success', 'کاربر مورد نظر با موفقیت مسدود شد');
        } else {
            return session()->flash('error', 'مشکلی پیش آمده ، لطفا از ورودی ها اطمینان حاصل کنید');
        }
    }

    public function unblockUser($userId)
    {
        $user = \App\Models\User::where('id', $userId);
        if ($user->update(['locked' => false])) {
            $this->show_locked_button = true;
            $this->show_unlocked_button = false;
            session()->flash('success', 'حساب کاربر مورد نظر با موفقیت رفع مسدودیت شد');
            return redirect()->route('UserProfile', ['userId' => $userId]);
        } else {
            return session()->flash('error', 'مشکلی پیش آمده ، لطفا از ورودی ها اطمینان حاصل کنید');
        }
    }

    public function mount($userId)
    {
        $user = \App\Models\User::with(['account', 'transactions', 'detail', 'userPlans', 'roles'])->find($userId);
        $this->user = $user;

        $this->userDetail = $user->detail()->first();
    }

    public function render()
    {
        $transactions = $this->user->transactions()->paginate(2);
        $plans = $this->user->userPlans()->paginate(1);
        return view('livewire.user.user', ['transactions' => $transactions, 'plans' => $plans]);
    }

}
