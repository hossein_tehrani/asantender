<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use Livewire\Component;

class Users extends Component
{
    public $searchedUser, $content, $onlyOrganizationUser = false;

    public function showProfile($userId)
    {
        return redirect()->route('UserProfile', ['userId' => $userId]);
    }

    public function active($id)
    {
        User::find($id)->update([
           'active' => true
        ]);
    }

    public function inactive($id)
    {
        User::find($id)->update([
            'active' => false
        ]);
    }

    public function search()
    {
        $searchedData = User::search($this->content)->get();
        $serachIds = $searchedData->pluck('id');
        $this->searchedUser = User::whereId($serachIds)->get();

        return redirect();
    }

    public function render()
    {
        $users = User::with(['account', 'transactions', 'detail', 'userPlans', 'roles'])->get();
        return view('livewire.user.users', ['users' => $users]);
    }
}
