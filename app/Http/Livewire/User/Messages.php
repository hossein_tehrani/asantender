<?php

namespace App\Http\Livewire\User;

use App\Models\UserMessage;
use Livewire\Component;
use Livewire\WithPagination;

class Messages extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $user , $messages,$message ;

    public function remove($messageId)
    {
        $singlemessage=UserMessage::find($messageId);
        $singlemessage->delete();
        session()->flash('success','پیام'.$singlemessage->title.'با موفقیت حذف گردید');
    }

    public function show($messageId)
    {
        $this->message=UserMessage::find($messageId);
    }

    public function render()
    {
        $allMessages=UserMessage::paginate(20);
        return view('livewire.user.messages',['allMessages'=>$allMessages]);
    }
}
