<?php

namespace App\Http\Livewire;

use App\Exports\InquiryExport;
use App\Imports\InquiryImport;
use App\Models\CentralOrgan;
use App\Models\Config;
use App\Models\Inquiry;
use App\Models\Province;
use App\Models\WorkGroup;
use Carbon\Carbon;
use Excel;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Morilog\Jalali\Jalalian;

class CreateInquiry extends Component
{
    use WithFileUploads , WithPagination ;
    public $i=1 ,$title,$city,$provinces = [],$address,$invitation_code,$title_advertiser,$source_adv,$setad,$link_url,$img_url,$body_adv,$commodity_service,$partial_medium,
    $status_adv,$free_date,$invitation_date,$submission_deadline_date,$minimum_price_validity_date,$requirement_date,$initial_publish_date,$publish_date,$workGroups = [], $import_excel,
    $asantender,$central_organ_id,$admin_id,$search_limit_time_start,$search_limit_time_end,$path,
    $search=[],$selected_provinces=[];

    protected $rules = [
        'title' => 'required|min:2|max:250',
        'city' => 'min:2|max:250',
        'address' => 'min:2|max:250',
        'invitation_code' => 'required|min:1|max:250',
        'title_advertiser' => 'required|min:2|max:250',
        'source_adv' => 'sometimes',
        'setad' => 'required|boolean',
        'link_url' => 'sometimes',
        'img_url' => 'sometimes',
        'body_adv' => 'required|min:5',
        'commodity_service' => 'required|boolean',
        'partial_medium' => 'required|boolean',
        'status_adv' => 'required|boolean',
        'free_date' => 'sometimes',
        'invitation_date' => 'sometimes',
        'submission_deadline_date' => 'sometimes',
        'minimum_price_validity_date' => 'sometimes',
        'requirement_date' => 'sometimes',
        'initial_publish_date' => 'sometimes',
        'publish_date' => 'sometimes',
        'central_organ_id' => 'required',
        'admin_id' => 'min:1|max:9',
        'provinces' => 'sometimes'
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function mount()
    {
        $this->path=null;
        $this->setad=0;
        $this->commodity_service=0;
        $this->partial_medium=0;
        $this->status_adv=0;
        $this->admin_id = Auth::id();
        $this->status_adv = 0;
        $this->provinces = Province::all();
        $this->ap = Province::pluck('id')->toArray();
        $this->workGroups = [];
        $this->search['inquiries'] = null;
        $this->search['limit_time_type'] = 'initial_publish_date';
        $this->search['number'] = 20;
        $this->search['active'] = [0, 1];
        for ($i = 1; $i <= 31; $i++) {
            array_push($this->selected_provinces, $i);
        }
        $this->search['words'] = null;
        $this->search['orderBy'] = 'initial_publish_date';
        $this->search_limit_time_start = '1350/01/01';
        $this->search_limit_time_end = '1499/01/01';
    }

    public function search()
    {
        $startDate = $this->search_limit_time_start == '' ? '1350/01/01' : Jalalian::fromFormat('Y/m/d', $this->search_limit_time_start)->toCarbon();
        $endDate = $this->search_limit_time_end == '' ? '1499/01/01' : Jalalian::fromFormat('Y/m/d', $this->search_limit_time_end)->toCarbon();
        if ($this->selected_provinces) {
            if ($this->selected_provinces[0] == 0) {
                for ($i = 1; $i <= 31; $i++) {
                    array_push($this->selected_provinces, $i);
                }
            }
        }
        switch ($this->search['active']) {
            case 1 :
                $this->search['active'] = [1];
                break;
            case 0 :
                $this->search['active'] = [0];
                break;
            case 2 :
                $this->search['active'] = [0, 1];
                break;
        }
        $this->search['inquiries'] = Inquiry::whereHas('provinces', function ($q) {
            $q->whereIn('province_id', $this->selected_provinces);
        })->Where(function ($query) {
            $query
                ->orwhere('title', 'LIKE', '%' . $this->search['words'] . '%')
                ->orWhere('city', 'LIKE', '%' . $this->search['words'] . '%')
                ->orWhere('address', 'LIKE', '%' . $this->search['words'] . '%')
                ->orWhere('invitation_code', 'LIKE', '%' . $this->search['words'] . '%')
                ->orWhere('title_advertiser', 'LIKE', '%' . $this->search['words'] . '%')
                ->orWhere('source_adv', 'LIKE', '%' . $this->search['words'] . '%')
                ->orWhere('body_adv', 'LIKE', '%' . $this->search['words'] . '%');
        })->whereIn('status_adv', $this->search['active'])->whereDate($this->search['limit_time_type'], '>=', $startDate)
            ->whereDate($this->search['limit_time_type'], '<=', $endDate)
            ->orderBy($this->search['orderBy'])->paginate($this->search['number'])->toArray();
    }

    public function ResetFilter()
    {
        $this->reset();
    }

    public function StoreInquiry()
    {
        $this->validate();
        if (isset($this->img_url)) {
            $imageName = 'inquiry' . time() . $this->asantender . '.' . $this->img_url->getClientOriginalExtension();
            $this->path = $this->img_url->storeAs(
                'inquiry', $imageName
            );
        }

        $inquiry = Inquiry::create([
            'free_date' => $this->free_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->free_date)->toCarbon(),
            'invitation_date' => $this->invitation_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->invitation_date)->toCarbon(),
            'submission_deadline_date' => $this->submission_deadline_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->submission_deadline_date)->toCarbon(),
            'minimum_price_validity_date' => $this->minimum_price_validity_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->minimum_price_validity_date)->toCarbon(),
            'requirement_date' => $this->requirement_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->requirement_date)->toCarbon(),
            'initial_publish_date' => $this->initial_publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->initial_publish_date)->toCarbon(),
            'publish_date' => $this->publish_date == '' ? null : Jalalian::fromFormat('Y/m/d', $this->publish_date)->toCarbon(),
            'title' => $this->title,
            'city' => $this->city,
            'address' => $this->address,
            'invitation_code' => $this->invitation_code,
            'title_advertiser' => $this->title_advertiser,
            'source_adv' => $this->source_adv,
            'setad' => $this->setad,
            'status_adv' => $this->status_adv,
            'commodity_service' => $this->commodity_service,
            'partial_medium' => $this->partial_medium,
            'asantender' => rand(100, 999) . 'T' . Carbon::now()->format('YMDHms') . 'T',
            'body_adv' => $this->body_adv,
            'link_url' => $this->link_url,
            'admin_id' => $this->admin_id,
            'central_organ_id' => $this->central_organ_id,
            'img_url' => $this->path
        ]);
        if (isset($this->provinces[0]) && $this->provinces[0] == 'all') {
            for ($i = 1; $i <= 31; $i++) {
                unset($this->provinces[0]);
                array_push($this->provinces, $i);
            }
        }
        $inquiry->workgroups()->sync($this->workGroups);
        $inquiry->provinces()->sync($this->provinces);
        session()->flash('success','آگهی استعلام با عنوان'.$inquiry->title.'با موفقیت ثبت شد');

    }

    public function toggleInquiry($id, $type)
    {
        $inquiry = Inquiry::findOrFail($id);
        switch ($type) {
            case 'Active':
                $inquiry->update(['status_adv' => 1]);
                break;
            case 'Deactive':
                $inquiry->update(['status_adv' => 0]);
                break;
        }
    }

    public function delete($id)
    {
        $inquiry = Inquiry::findOrFail($id);
        $inquiry->delete();
        session()->flash('success', 'آگهی با عنوان ' . '"' . $inquiry->title_advertiser . '"' . ' حذف گردید');
    }

    public function StoreExcel()
    {
        $path1 = $this->import_excel->store('temp');
        $path = storage_path('app') . '/' . $path1;
        Excel::import(new InquiryImport, $path);
    }

    public function ExportExcel()
    {
        return Excel::download(new InquiryExport, 'INQUIRIES.xlsx');
    }

    public function render()
    {
        $config = Config::firstOrFail();
        $allProvinces = Province::all();
        $central_organs = CentralOrgan::all();
        $work_groups = WorkGroup::get()->groupBy('parent_id');
        $work_groups['root'] = $work_groups[''];
        unset($work_groups['']);
        $inquiries = Inquiry::orderBy('created_at', 'desc')->paginate($config->pagination_advertise_admin);
        return view('livewire.create-inquiry', ['work_groups' => $work_groups, 'central_organs' => $central_organs, 'inquiries' => $inquiries, 'allProvinces' => $allProvinces, 'config' => $config])
            ->layout('layouts.master');
    }

}
