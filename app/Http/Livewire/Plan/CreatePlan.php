<?php

namespace App\Http\Livewire\Plan;

use App\Models\Plan;
use Livewire\Component;
use Livewire\WithPagination;

class CreatePlan extends Component
{
    use WithPagination;

    public $i = 1 , $plan=[] ,$search=[];

    protected $rules = [
        'plan.title' => 'required|min:8',
        'plan.priority'=>'max:3' ,
        'plan.price'=> 'min:4|max:100',
        'plan.status'=> 'max:1',
        'plan.off_percent'=> 'max:2',
        'plan.work_group_limit'=> 'max:3',
        'plan.work_group_change_limit'=>'max:3' ,
        'plan.count'=> 'max:5',
        'plan.time'=> 'max:8',
    ];

    public function mount()
    {
        $this->search['plans']=null;
        $this->search['number']=10;
        $this->search['words']='';
        $this->search['orderBy']='title';
        $this->search['active']='all';
    }

    public function search()
    {
        switch($this->search['active']){
            case 'all':
                $this->search['plans']=Plan::where('title','LIKE','%'.$this->search['words'].'%')
                    ->orderBy($this->search['orderBy'])
                    ->paginate($this->search['number'])->toArray();
                break;
            case 'active':
                $this->search['plans']=Plan::where('status',1)->where('title','LIKE','%'.$this->search['words'].'%')
                    ->orderBy($this->search['orderBy'])
                    ->paginate($this->search['number'])->toArray();
                break;
            case 'deactive':
                $this->search['plans']=Plan::where('status',0)->where('title','LIKE','%'.$this->search['words'].'%')
                    ->orderBy($this->search['orderBy'])
                    ->paginate($this->search['number'])->toArray();
                break;
        }
    }
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function storePlan()
    {
        Plan::create($this->validate()['plan']);
        session()->flash('message','پلن موردنظر با موفقیت ساخته شد');
    }
    public function togglePlan($id, $type)
    {
        $plan = Plan::findOrFail($id);
        switch ($type) {
            case 'Active':
                $plan->update(['status' => 1]);
                break;
            case 'Deactive':
                $plan->update(['status' => 0]);
        }
    }

    public function delete($id)
    {
        $plan=Plan::find($id);
        $plan->delete();
        session()->flash('message','طرح اشتراکی به نام ' .$plan['title'].' با موفقیت حذف گردید');
    }

    public function render()
    {
        $plans = Plan::paginate(10);
        return view('livewire.plan.create-plan', ['plans' => $plans]);
    }
}
