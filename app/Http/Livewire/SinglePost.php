<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Blog;

class SinglePost extends Component
{
    public $post;
    public function mount($slug)
    {
        $this->post=Blog::Where('slug',$slug)->first();
    }
    public function render()
    {
        return view('livewire.single-post');
    }
}
