<?php

namespace App\Http\Livewire\Auth;

use App\Models\Admin;
use App\Models\ConfigAdmin;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


class AdminUpdate extends Component
{
    public $admin, $current_admin, $name, $mobile, $email, $type, $phone, $password, $address, $status_access, $last_login,
        $roles = [],$admin_roles=[];
    use AuthorizesRequests;

    protected $rules = [
        'name' => 'required|min:8|max:250',
        'mobile' => 'required|min:1|max:250',
        'email' => 'min:2|max:250|email',
        'phone' => 'required|min:1|max:250',
        'password' => 'required|min:2|max:250',
        'address' => 'min:1|max:255',
        'type' => 'required',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function mount($admin)
    {
        $this->current_admin = Admin::findorfail($admin);
        $this->name = $this->current_admin->name;
        $this->mobile = $this->current_admin->mobile;
        $this->email = $this->current_admin->email;
        $this->phone = $this->current_admin->phone;
        $this->address = $this->current_admin->address;
        $this->status_access = $this->current_admin->status_access;
        $this->last_login = $this->current_admin->last_login;
        $this->type = 'internal_staff';
        $this->roles=Role::all();
    }

    public function updateAdmin($id)
    {
        $validatedData=$this->validate();
        $admin = Admin::findorfail($id);
        $validatedData['password'] = Hash::make($this->password);
        $admin->update($validatedData);
        $admin->roles()->sync($this->admin_roles);
        session()->flash('success','حساب کاربر مورد نظر با موفقیت ویرایش شد ');
    }

    public function update($id)
    {
        $admin = Admin::findorfail($id);
        redirect()->route('AdminUpdate', ['admin' => $admin]);
    }

    public function delete($id)
    {
        $admin = Admin::findorfail($id);
        $admin->delete;
    }
    public function render()
    {
        $configAdmin = ConfigAdmin::first();
        $admins = Admin::with('roles')->orderby('id', 'desc')->paginate($configAdmin->paginate_show_admins);
        $admin = Auth()->guard('admin')->user();
        if ($this->authorize('manager', $admin)) {
            return view('livewire.auth.admin-update', ['admins' => $admins])->layout('layouts.master');
        }
    }
}
