<?php

namespace App\Http\Livewire\Auth;

use App\Models\Admin;
use App\Models\ConfigAdmin;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class
AdminRegister extends Component
{
    public $name, $mobile, $email, $phone, $password, $type, $address, $status_access, $last_login,$roles=[],$admin_roles=[];
    use AuthorizesRequests;

    protected $rules = [
        'name' => 'required|min:8|max:250',
        'mobile' => 'required|min:1|max:250',
        'email' => 'min:2|max:250|email',
        'phone' => 'required|min:1|max:250',
        'password' => 'required|min:2|max:250',
        'address' => 'min:1|max:255',
        'type' => 'min:1|max:255',
    ];

    public function mount()
    {
        $this->roles = Role::all();
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function addAdmin()
    {
        $validatedData=$this->validate();
        $admin = Admin::create($validatedData);
        $admin->roles()->sync($this->roles);
        session()->flash('success','حساب کاربری مورد نظر با موفقیت ساخته شد');
    }

    public function delete($id)
    {
        $admin = Admin::findorfail($id);
        $admin->delete();
        session()->flash('error','حساب کاربری مورد نظر با موفقیت حذف شد');
    }

    public function update($id)
    {
        $admin = Admin::findorfail($id);
        redirect()->route('AdminUpdate', ['admin' => $admin]);
    }

    public function render()
    {
        $configAdmin = ConfigAdmin::first();
        $admins = Admin::with('roles')->orderby('id', 'desc')->paginate($configAdmin->paginate_show_admins);
        $admin = Auth()->guard('admin')->user();
        if ($this->authorize('manager', $admin)) {
            return view('livewire.auth.admin-register', ['admins' => $admins])->layout('layouts.master');
        }
    }
}
