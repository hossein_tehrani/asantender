<?php

namespace App\Http\Livewire\Auth\Password;

use Livewire\Component;

class AdminEmail extends Component
{
    public function render()
    {
        return view('livewire.auth.password.admin-email');
    }
}
