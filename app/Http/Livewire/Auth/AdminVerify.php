<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;

class AdminVerify extends Component
{
    public function render()
    {
        return view('livewire.auth.admin-verify');
    }
}
