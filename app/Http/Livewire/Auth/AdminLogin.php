<?php

namespace App\Http\Livewire\Auth;

use App\Models\Admin;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class AdminLogin extends Component
{
    public $email, $password, $remember;


    public function login()
    {
        $credentials = ['email' => $this->email, 'password' => $this->password];
        Auth()->guard('admin')->attempt($credentials, $this->remember);
        if (Auth()->guard('admin')->user()) {
            Admin::where('id', Auth()->guard('admin')->user()->id)->update(['last_login' => Carbon::now()]);
            session()->flash('success',Auth()->guard('admin')->user()->name.' '.'خوش اومدی');
            return redirect('/');
        } else {
            session()->flash('error', 'نام کاربری/رمز عبور اشتباه می باشد');

        }
    }

    public function logout()
    {
        Auth()->guard('admin')->logout();
    }

    public function render()
    {
        return view('livewire.auth.admin-login')->layout('layouts.master');
    }
}
