<?php

namespace App\Http\Livewire;

use Auth;
use Livewire\Component;
use App\Models\Blog;
class Writer extends Component
{
    public $title,$keywords,$priority,$meta,$description,$body,$admin;

    public function mount()
    {
        $this->admin=Auth::guard('admin')->user();
    }
    public function create()
    {
        $post=Blog::create([
            'title'=>$this->title,
            'keywords'=>$this->keywords,
            'priority'=>$this->title,
            'meta'=>$this->meta,
            'description'=>$this->description,
            'body'=>$this->body,
            'admin_id'=>$this->admin->id,
        ]);
        session()->flash('success','پست شما با موفقیت ایجاد شد');
    }
    public function render()
    {
        return view('livewire.writer');
    }
}
