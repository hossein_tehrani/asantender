<?php

namespace App\Http\Livewire;

use Auth;
use Livewire\Component;

class AdminHeader extends Component
{
    protected $message = "", $admin;

    public function mount()
    {
        $this->admin = Auth::user()->toArray();
    }

    public function Details()
    {
        return redirect()->to('/details');
    }

    public function createTender()
    {
        return redirect()->to('create-tender');
    }

    public function createAuction()
    {
        return redirect()->to('/create-auction');
    }

    public function createInquiry()
    {
        return redirect()->to('/create-inquiry');
    }

    public function createWorkGroup()
    {
        return redirect()->to('/create-work-group');
    }

    public function createCentralOrgan()
    {
        return redirect()->to('/create-central-organ');
    }

    public function manageStaff()
    {
        return redirect()->to('/register');
    }
    public function manageBlog()
    {
        return redirect()->to('/writer');
    }

    public function managePlan()
    {
        return redirect()->to('/plan');
    }

    public function manageDiscount()
    {
        return redirect()->to('/discounts');
    }
    public function signout()
    {
        auth()->guard('admin')->logout();
        session()->flash('success', 'بدرود!');
        return redirect()->to('/');
    }

    public function render()
    {
        return view('livewire.admin-header', ['message' => $this->message]);
    }
}
