<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EditTender extends Component
{
    public function render()
    {
        return view('livewire.edit-tender');
    }
}
