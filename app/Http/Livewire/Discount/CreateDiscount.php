<?php

namespace App\Http\Livewire\Discount;

use App\Models\DiscountCode;
use App\Models\Plan;
use Livewire\Component;
use Livewire\WithPagination;

class CreateDiscount extends Component
{
    public $i = 1 ,$discount=[],$plans;
//name
//serial
//expire
//count
//percent
//work_groups_changes
//status
//plan_id
    use WithPagination;

    protected $rules = [
        'discount.name' => 'required|min:2|max:250',
        'discount.serial' => 'required|min:2|max:250',
        'discount.expire' => 'required|min:2|max:250',
        'discount.count' => 'min:1|max:8',
        'discount.percent' => 'required|max:2',
        'discount.work_groups_changes' => 'required',
        'discount.status' => 'boolean',
        'discount.plan_id' => 'required',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
    public function mount()
    {
        $this->plans=Plan::all()->toArray();
    }

    public function store()
    {
        DiscountCode::create($this->discount);
        session()->flash('success','کد تخفیف با موفقیت ایجاد گردید');
    }


    public function toggleDiscounts($id, $type)
    {
        $discount = DiscountCode::findOrFail($id);
        switch ($type) {
            case 'Active':
                $discount->update(['status' => 1]);
                break;
            case 'Deactive':
                $discount->update(['status' => 0]);
        }
    }

    public function delete($id)
    {
        $discount = DiscountCode::findOrFail($id);
        $discount->delete();
        session()->flash('message','کد تخفیف به نام ' .$discount['name'].' با موفقیت حذف گردید');

    }

    public function render()
    {
        $discounts = DiscountCode::paginate(10);
        return view('livewire.discount.create-discount', ['discounts' => $discounts]);
    }
}
