<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TenderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'asantender' => $this->asantender,
            'title' => $this->title,
            'province' => $this->province,
            'initial_publish_date' => $this->initial_publish_date,
        ];
    }
}
