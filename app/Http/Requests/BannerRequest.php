<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|min:1|max:255',
            'description'=>'required|min:1',
            'link'=>'min:1|max:255',
            'image_file'=>'min:1|max:255',
            'click_count'=>'min:1|max:255',
            'start_date'=>'required',
            'expire_date'=>'required',
            'hasButton'=>'required',
        ];
    }
}
