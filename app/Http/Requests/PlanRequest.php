<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|min:1|max:255',
            'priority'=>'min:1|max:255',
            'price'=>'required|min:3|max:255',
            'status'=>'required|min:1|max:2',
            'off_percent'=>'min:1|max:3',
            'work_group_limit'=>'min:1|max:3',
            'work_group_change_limit'=>'min:1|max:3',
            'count'=>'min:1|max:4',
            'time'=>'min:1|max:255',
        ];
    }
}
