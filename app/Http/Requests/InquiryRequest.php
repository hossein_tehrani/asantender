<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InquiryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'min:1',
            'title'=>'required|min:2|max:250',
            'province'=>'required|min:2|max:250',
            'city'=>'min:2|max:250',
            'address'=>'min:2|max:250',
            'invitation_code'=>'required|min:1|max:250',
            'title_advertiser'=>'required|min:2|max:250',
            'source_adv'=>'min:1|max:255',
            'setad'=>'boolean',
            'free_date'=>'min:1|max:10',
            'status_adv'=>'boolean',
            'body_adv'=>'min:5',
            'partial_medium'=>'boolean',
            'commodity_service'=>'boolean',
            'img_url'=>'max:255',
            'link_url'=>'max:255',
            'user_id'=>'min:1|max:10',
            'central_organ_id'=>'min:1|max:4',
        ];
    }
}
