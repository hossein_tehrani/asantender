<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'mobile' => 'required|min:10|max:11|unique:users',
            'email' => 'required|email|unique:users',
            'city' => 'sometimes',
            'address' => 'sometimes',
            'type' => 'required|in:NATURAL,LEGAL',
            'company_name' => 'sometimes',
            'status' => 'boolean',
            'mobile_verified_at' => 'sometimes',
            'subscription_date' => 'sometimes',
            'subscription_title' => 'sometimes',
            'subscription_count' => 'sometimes',
            'work_groups_changes' => 'sometimes',
            'password' =>'required|confirmed',
        ];
    }
}
