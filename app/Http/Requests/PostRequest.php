<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'meta_tag_description'=>'required|min:1|max:255',
            'meta_tag_keywords'=>'required|min:1|max:255',
            'title_tag'=>'required|min:1|max:255',
            'author'=>'required',
            'title'=>'required|min:1|max:255',
            'division'=>'required',
            'body'=>'required|min:3',
            'user_id'=>'required',
        ];
    }
}
