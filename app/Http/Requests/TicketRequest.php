<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:1|max:255',
            'title'=>'required|min:1|max:255',
            'division'=>'required|min:1|max:255',
            'body'=>'required|min:1',
            'answer'=>'required|min:1',
            'status'=>'required|min:1',
        ];
    }
}
