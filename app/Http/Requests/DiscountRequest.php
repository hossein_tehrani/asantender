<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiscountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'min:1|max:255|required',
            'serial'=>'min:1|max:255|required',
            'expire'=>'min:1|max:3',
            'count'=>'min:1|max:4',
            'work_groups_changes'=>'min:1|max:4',
            'percent'=>'min:1|max:4',
            'status'=>'boolean',
            'plan_id'=>'min:1|max:4'
        ];
    }
}
