<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CentralOrganRequest extends FormRequest
{
    /**
     * @var mixed
     */
    private $id;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'min:1',
            'name'=>'required|min:2|max:255',
            'status'=>'max:2',
            'parent_id'=>'max:4',
            'priority'=>'max:4',
        ];
    }
}
