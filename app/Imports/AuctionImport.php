<?php

namespace App\Imports;

use App\Models\Auction;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class AuctionImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {

            $auction=Auction::create([
                'title' => $row[1],
                'city' => $row[2],
                'address' => $row[3],
                'auction_code' => $row[4],
                'title_advertiser' => $row[5],
                'source_adv' => $row[6],
                'setad' => $row[7],
                'invitation_date' => $row[8],
                'view_deadline_date' => $row[9],
                'request_deadline_date' => $row[10],
                'winner_announced_date' => $row[11],
                'initial_publish_date' => $row[12],
                'publish_date' => $row[13],
                'free_date' => $row[14],
                'commodity_num' => $row[15],
                'commodity_description' => $row[16],
                'base_price' => $row[17],
                'source_auction_num' => $row[18],
                'body_adv' => $row[19],
                'status_adv' => $row[20],
                'img_url' => $row[21],
                'link_url' => $row[22],
                'asantender' => $row[23],
                'admin_id' => $row[24],
                'central_organ_id' => $row[25]
            ]);
            $auction->provinces()->sync(json_decode($row[26]));
            $auction->workgroups()->sync(json_decode($row[27]));
        }
    }
}
