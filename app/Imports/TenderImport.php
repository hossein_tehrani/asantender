<?php

namespace App\Imports;

use App\Models\Tender;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class TenderImport implements ToCollection
{
    /**
     * @param array $rows
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $tender=Tender::create([
                'title' => $row[1],//a
                'slug' => $row[2],//a
                'city' => $row[3],//b
                'address' => $row[4],//c
                'invitation_code' => $row[5],//d
                'title_advertiser' => $row[6],//e
                'source_adv' => $row[7],//f
                'setad' => $row[8],//g
                'invitation_date' => $row[9],//h
                'document_deadline_date' => $row[10],//i
                'request_deadline_date' => $row[11],
                'winner_announced_date' => $row[12],
                'initial_publish_date' => $row[13],
                'publish_date' => $row[14],
                'free_date' => $row[15],
                'body_adv' => $row[16],//o
                'status_adv' => $row[17],//p
                'img_url' => $row[18],//q
                'asantender' => $row[19],//r
                'link_url' => $row[20],//s
                'central_organ_id' => $row[21],//t
                'admin_id' => $row[22],//u
            ]);
            $tender->provinces()->sync(json_decode($row[23]));
            $tender->workgroups()->sync(json_decode($row[27]));
        }
    }
}
