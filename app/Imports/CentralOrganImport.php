<?php

namespace App\Imports;

use App\Models\CentralOrgan;
use Maatwebsite\Excel\Concerns\ToModel;

class CentralOrganImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new CentralOrgan([
            'name' => $row[0],
            'parent_id' => $row[1],
            'status' => $row[2],
            'priority' => $row[3],
        ]);
    }
}
