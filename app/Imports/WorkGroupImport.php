<?php

namespace App\Imports;

use App\Models\WorkGroup;
use Maatwebsite\Excel\Concerns\ToModel;

class WorkGroupImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new WorkGroup([
            'title' => $row[0],
            'image' => $row[1],
            'type' => $row[2],
            'status' => $row[3],
            'priority' => $row[4],
            'parent_id' => $row[5],
        ]);
    }
}
