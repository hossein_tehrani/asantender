<?php

namespace App\Imports;

use App\Models\Inquiry;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use PhpParser\ErrorHandler\Collecting;

class InquiryImport implements ToCollection
{

    public function collection(Collection $rows)
    {
        foreach($rows as $row) {
            $inquiry = Inquiry::create([
                'title' => $row[1],
                'city' => $row[2],
                'address' => $row[3],
                'invitation_code' => $row[4],
                'title_advertiser' => $row[5],
                'source_adv' => $row[6],
                'setad' => $row[7],
                'link_url' => $row[8],
                'img_url' => $row[9],
                'body_adv' => $row[10],
                'commodity_service' => $row[11],
                'partial_medium' => $row[12],
                'status_adv' => $row[13],
                'free_date' => $row[14],
                'invitation_date' => $row[15],
                'submission_deadline_date' => $row[16],
                'minimum_price_validity_date' => $row[17],
                'requirement_date' => $row[18],
                'initial_publish_date' => $row[19],
                'publish_date' => $row[20],
                'asantender' => $row[21],
                'central_organ_id' => $row[22],
                'admin_id' => $row[23],
            ]);
            $inquiry->provinces()->sync(json_decode($row[24]));
            $inquiry->workgroups()->sync(json_decode($row[25]));
        }
     }
}
