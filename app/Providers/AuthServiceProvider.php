<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use App\Models\Permission;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\Role;
use Laravel\Passport\Passport;
use Psy\Exception\ParseErrorException;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        Passport::routes();
        $this->registerPolicies();
        if (Schema::hasTable('roles')) {
            foreach ($this->getRole() as $role) {
                Gate::define($role->name, function ($admin) use ($role) {
                    return $admin->roles()->where('name',$role->name)->pluck('name')->toArray() == [$role->name];
                });
            }
        }
    }
    public function getRole()
    {
        if (Schema::hasTable('roles')) {
            return Role::all();
        }
        return true;
    }
}
