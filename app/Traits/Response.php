<?php


namespace App\Traits;


trait Response
{

    /**
     * json success response for api
     * @param $message
     * @param $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function successResponse($message, $data = null, $code = 200)
    {
        $res = [
            'message' => __("messages.{$message}"),
            'data' => $data
        ];
        if (empty($data)) {
            unset($res['data']);
        }
        return response()
            ->json($res, $code);
    }

    /**
     * json error response for api
     * @param $message
     * @param $error
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function errorResponse($message, $error, $code = 400)
    {
        return response()
            ->json([
                'message' => __("errors.{$message}"),
                'error' => $error
            ], $code);
    }
}
