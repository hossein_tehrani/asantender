<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;
    protected $fillable=[
        'name',
        'parent_id',
    ];
    public function tenders()
    {
        return $this->belongsToMany(Tender::class);
    }
    public function auctions()
    {
        return $this->belongsToMany(Auction::class);
    }
    public function inquiries()
    {
        return $this->belongsToMany(Inquiry::class);
    }
}
