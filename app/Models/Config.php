<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable=[
        'length_advertise_admin_show',
        'pagination_advertise_admin',
        'welcome_message_user_dashboard',
        'welcome_message_admin_dashboard',
        'url_header_user_site',
        'url_header_user_dashboard',
        'url_footer_user_site',
        'url_footer_user_dashboard',
        'initial_work_groups_change_user_subscribed',
    ];
    use HasFactory;
}
