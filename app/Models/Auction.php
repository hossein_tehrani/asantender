<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Pishran\LaravelPersianSlug\HasPersianSlug;
use Spatie\Sluggable\SlugOptions;

class Auction extends Model
{
    use HasFactory, Searchable, HasPersianSlug;

//31329825 PW OF MOBIN NET WIRELESS
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->slugsShouldBeNoLongerThan(100);
    }

    protected $fillable = [
        'title',
        'slug',
        'province',
        'city',
        'address',
        'auction_code',
        'title_advertiser',
        'setad',
        'invitation_date',
        'view_deadline_date',
        'request_deadline_date',
        'winner_announced_date',
        'free_date',
        'initial_publish_date',
        'publish_date',
        'commodity_num',
        'commodity_description',
        'base_price',
        'source_auction_num',
        'body_adv',
        'status_adv',
        'source_adv',
        'img_url',
        'link_url',
        'admin_id',
        'central_organ_id',
    ];

    public function workgroups()
    {
        return $this->belongsToMany(WorkGroup::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function provinces()
    {
        return $this->belongsToMany(Province::class);
    }

    public function centralorgan()
    {
        return $this->hasMany(CentralOrgan::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
