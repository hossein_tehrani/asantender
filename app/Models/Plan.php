<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;
    protected $fillable=[
        'title',
        'priority',
        'price',
        'status',
        'off_percent',
        'work_group_limit',
        'work_group_change_limit',
        'count',
        'time',
    ];





    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function discountcodes()
    {
        return $this->hasMany(DiscountCode::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
