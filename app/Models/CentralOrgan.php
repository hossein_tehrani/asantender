<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;


class CentralOrgan extends Model
{
    use HasFactory,Searchable;
    protected $fillable=[
        'name',
        'parent_id',
        'priority',
        'status',
        ];
    public function inquiry()
    {
        return $this->belongsTo(Inquiry::class);
    }
    public function tender()
    {
        return $this->belongsTo(Tender::class);
    }
    public function auction()
    {
        return $this->belongsTo(Auction::class);
    }
}
