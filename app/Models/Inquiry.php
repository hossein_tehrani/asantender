<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Pishran\LaravelPersianSlug\HasPersianSlug;
use Spatie\Sluggable\SlugOptions;
class Inquiry extends Model
{
    use HasFactory,Searchable,HasPersianSlug;
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->slugsShouldBeNoLongerThan(100);
    }
    protected $fillable=[
      'title',
      'city',
      'address',
      'invitation_code',
      'title_advertiser',
      'source_adv',
      'setad',
      'link_url',
      'img_url',
      'body_adv',
      'commodity_service',
      'partial_medium',
      'status_adv',
      'free_date',
      'invitation_date',
      'submission_deadline_date',
      'minimum_price_validity_date',
      'requirement_date',
      'initial_publish_date',
      'publish_date',
      'asantender',
      'central_organ_id',
      'admin_id',
    ];
    public function workgroups()
    {
        return $this->belongsToMany(WorkGroup::class);
    }
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
    public function provinces()
    {
        return $this->belongsToMany(Province::class);
    }
    public function centralorgan()
    {
        return $this->hasMany(CentralOrgan::class);
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
