<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiscountCode extends Model
{
    use HasFactory;
    protected $fillable=[
      'name',
      'serial',
      'expire',
      'count',
      'percent',
      'work_groups_changes',
      'status',
      'plan_id',
    ];

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }
}
