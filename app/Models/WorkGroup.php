<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\TypesTrait;


class WorkGroup extends Model
{
    use HasFactory,TypesTrait;
    const TYPEDEFINITION = ['AUCTION' => 'مزایده', 'TENDER' => 'مناقصه', 'INQUIRY' => 'استعلام'];
    const TYPES = ['AUCTION', 'TENDER', 'INQUIRY'];
    protected $fillable=[
      'title',
      'image',
      'status',
      'priority',
      'parent_id',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function inquiries()
    {
        return $this->belongsToMany(Inquiry::class);
    }

    public function tenders()
    {
        return $this->belongsToMany(Tender::class);
    }
    public function auctions()
    {
        return $this->belongsToMany(Auction::class);
    }

}
