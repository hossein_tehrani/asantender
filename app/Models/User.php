<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\TypesTrait;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use HasFactory, Notifiable,HasApiTokens;
    use TypesTrait;
    /* *********************
    *      Enum Types & Roles
    ************************/
    const TYPEDEFINITION = ['NATURAL' => 'حقیقی', 'LEGAL' => 'حقوقی'];
    const TYPES = ['NATURAL', 'LEGAL'];
    /**
     * The attributes that are mass assignable.
     *
     * @var arrayu
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'mobile',
        'phone',
        'city',
        'address',
        'type',
        'company_name',
    ];
    protected $guarded= [
        'status',
        'role',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'mobile_verified_at' => 'datetime',
    ];
    public function workgroups(){
        return $this->belongsToMany(WorkGroup::class);
    }
    public function plans()
    {
        return $this->belongsToMany(Plan::class);
    }
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
       public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
