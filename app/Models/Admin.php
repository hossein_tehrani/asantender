<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Admin extends  Authenticatable
{
    use HasFactory;
    /*protected $guard = 'admin';*/

    protected $fillable = [
        'name',
        'mobile',
        'email',
        'phone',
        'password',
        'address',
        'status_access',
        'type',
        'last_login',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    public function auctions()
    {
        return $this->hasMany(Auction::class);
    }
    public function inquiries()
    {
        return $this->hasMany(Inquiry::class);
    }
    public function tenders()
    {
        return $this->hasMany(Tender::class);
    }
    public function hasRole($role)
    {
        $admin=Auth()->guard('admin')->user();
        if(is_string($role)){
            return $this->roles()->where('name',$role)->get();
        }
        return !! $role->intersect($this->roles);
    }



}
