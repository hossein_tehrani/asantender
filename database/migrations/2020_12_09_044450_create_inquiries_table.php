<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiries', function (Blueprint $table) {
            $table->id();
            $table->string('title');//عنوان استعلام
            $table->string('type')->default('inquiry');
            $table->string('slug')->nullable();//link slug
            $table->string('city')->nullable();//شهر
            $table->string('address')->nullable();//ادرس
            $table->string('invitation_code')->unique();//کد فراخوان
            $table->string('title_advertiser');//عنوان آگهی گذار
            $table->string('source_adv')->nullable();//منبع آگهی
            $table->boolean('setad');//ستادی یا غیر ستادی
            $table->string('link_url')->nullable();//آدرس لینک آگهی
            $table->longText('img_url')->nullable();//آدرس عکس آگهی
            $table->longText('body_adv');//شرح آگهی
            $table->boolean('commodity_service');//خدمات یا کالا
            $table->boolean('partial_medium');//جزیی یا متوسط
            $table->boolean('status_adv');//وضعیت آگهی
            $table->date('free_date');//تاریخ رایگان
            $table->date('invitation_date');//تاریخ فراخوان
            $table->date('submission_deadline_date')->nullable();//تاریخ مهلت ارسال پاسخ
            $table->date('minimum_price_validity_date')->nullable();//تاریخ حداقل اعتبار قیمت
            $table->date('requirement_date')->nullable();//تاریخ نیاز
            $table->date('initial_publish_date');//تاریخ ثبت در آسان تندر
            $table->date('publish_date');//تاریخ انتشار
            $table->string('asantender')->nullable();//کد آسان تندر
            $table->timestamps();
            $table->foreignId('central_organ_id')->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('admin_id')->constrained()->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiries');
    }
}
