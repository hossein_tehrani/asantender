<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->id();
            $table->string('name');//نام و نام خانوادگی
            $table->string('mobile')->unique();//شماره همراه
            $table->string('email')->unique();//ایمیل
            $table->string('phone')->nullable();//شماره ثابت
            $table->string('password');//رمز عبور
            $table->string('status')->default('free');//وضعیت
            $table->string('address')->nullable();//آدرس
            $table->enum('type',['logical_staff','internal_staff'])->nullable();//نوع کاربری
            $table->boolean('status_access')->default(true);//وضعیت دسترسی
            $table->dateTime('last_login')->nullable();//اخرین ورود
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
