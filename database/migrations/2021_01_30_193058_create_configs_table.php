<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->id();
            $table->string('length_advertise_admin_show');
            $table->string('pagination_advertise_admin');
            $table->string('welcome_message_user_dashboard');
            $table->string('welcome_message_admin_dashboard');
            $table->string('url_header_user_site');
            $table->string('url_header_user_dashboard');
            $table->string('url_footer_user_site');
            $table->string('url_footer_user_dashboard');
            $table->string('initial_work_groups_change_user_subscribed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
