<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');//نام
            $table->string('mobile')->unique();//شماره همراه
            $table->string('email')->unique()->nullable();//ایمیل
            $table->string('city')->nullable();//شهر
            $table->string('address')->nullable();//آدرس
            $table->enum('type', User::types());//نوع کاربری
            $table->string('company_name')->nullable();//نام شرکت
            $table->boolean('status')->default(1);
            $table->timestamp('mobile_verified_at')->nullable();
            $table->date('subscription_date')->nullable();//تاریخ خرید آخرین طرح عضویت
            $table->string('subscription_title')->nullable();//عنوان آخرین طرح عضویت
            $table->unsignedInteger('subscription_count')->default(0); //تعداد طرح های عضویت خریداری شده
            $table->unsignedInteger('work_groups_changes')->default(0);//تعداد تغییرات گروه کاری
            $table->unsignedInteger('count_adv')->default(0);//تعداد  مجاز بازدید آگهی
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
