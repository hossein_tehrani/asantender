<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateAuctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auctions', function (Blueprint $table) {
            $table->id();
            $table->longText('title');//عنوان مزایده
            $table->string('type')->default('auction');
            $table->string('slug')->nullable();//link slug
            $table->string('city')->nullable();//شهر
            $table->string('address')->nullable();//ادرس
            $table->string('auction_code');//کد مزایده
            $table->string('title_advertiser');//عنوان آگهی گذار
            $table->string('source_adv')->nullable();//منبع آگهی
            $table->boolean('setad');//ستادی یا غیر ستادی
            $table->date('invitation_date');//تاریخ فراخوان
            $table->date('view_deadline_date')->nullable();//تاریخ مهلت بازدید
            $table->date('request_deadline_date')->nullable();//تاریخ مهلت ارسال پیشنهاد
            $table->date('winner_announced_date')->nullable();//تاریخ اعلام برنده
            $table->date('initial_publish_date');//تاریخ ثبت در آسان تندر
            $table->date('publish_date');//تاریخ انتشار
            $table->date('free_date');//تاریخ رایگان
            $table->string('commodity_num')->nullable();//شماره مال
            $table->string('commodity_description')->nullable();//شرح مال
            $table->string('base_price')->nullable();//قیمت پایه
            $table->string('source_auction_num')->nullable();//شماره مزایده مرجع
            $table->longText('body_adv');//شرح آگهی
            $table->boolean('status_adv');//وضعیت آگهی
            $table->text('img_url')->nullable();//آدرس عکس آگهی
            $table->string('link_url')->nullable();//آدرس لینک آگهی
            $table->string('asantender')->nullable();//کد آسان تندر
            $table->timestamps();
            $table->foreignId('central_organ_id')->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('admin_id')->constrained()->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auctions');
    }
}
