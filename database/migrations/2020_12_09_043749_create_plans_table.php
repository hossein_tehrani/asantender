<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->string('title');//عنوان پلن
            $table->integer('priority');//اولویت در نمایش
            $table->integer('price');//قیمت طرح
            $table->boolean('status');//نمایش یا عدم نمایش پلن
            $table->tinyInteger('off_percent')->nullable();// درصد تخفیف
            $table->integer('work_group_limit');//تعداد گروه کاری قابل انتخاب
            $table->integer('work_group_change_limit');//تعداد مجاز تغییر گروه کاری
            $table->integer('count')->nullable();// تعداد مجاز  دیدن آگهی
            $table->integer('time')->nullable();// زمان مجاز دیدن آگهی
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
