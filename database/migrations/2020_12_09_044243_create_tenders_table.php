<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenders', function (Blueprint $table) {
            $table->id();
            $table->string('title');//عنوان مناقصه
            $table->string('type')->default('tender');
            $table->string('slug')->nullable();//link slug
            $table->string('city')->nullable();//شهر
            $table->string('address')->nullable();//ادرس
            $table->string('invitation_code')->unique();//کد فراخوان
            $table->string('title_advertiser');//عنوان آگهی گذار
            $table->string('source_adv')->nullable();//manbae agahi
            $table->boolean('setad');//ستادی یا غیر ستادی
            $table->date('invitation_date');//تاریخ فراخوان
            $table->date('document_deadline_date')->nullable();//تاریخ مهلت دریافت اسناد
            $table->date('request_deadline_date')->nullable();//تاریخ مهلت ارسال پیشنهاد
            $table->date('winner_announced_date')->nullable();//تاریخ اعلام برنده
            $table->date('initial_publish_date');//تاریخ ثبت در آسان تندر
            $table->date('publish_date');//تاریخ انتشار
            $table->date('free_date');//تاریخ رایگان
            $table->text('body_adv');//شرح آگهی
            $table->boolean('status_adv');//وضعیت آگهی
            $table->text('img_url')->nullable();//آدرس عکس آگهی
            $table->string('asantender')->nullable();//کد آسان تندر
            $table->text('link_url')->nullable();//آدرس لینک آگهی
            $table->timestamps();
            $table->foreignId('central_organ_id')->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('admin_id')->constrained()->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenders');
    }
}
