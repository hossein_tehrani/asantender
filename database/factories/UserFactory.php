<?php

namespace Database\Factories;

use App\Models\User;
use DB;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user=DB::table('users')->first();
        if($user){
            return[
                'name' => $this->faker->name,
                'mobile' => $this->faker->numberBetween(1111111111,99999999999),
                'email' => $this->faker->safeEmail,
                'city' => $this->faker->city,
                'address' => $this->faker->address,
                'type'=>'LEGAL',
                'company_name'=>$this->faker->company,
                'status'=>0,
                'password' => '$2y$10$er.BEswVw5srTulhCLGMKOJGwiLnydpf.j4B3ssAHIvd90wEfIg0W',
            ];
        }
        else{
            return [
                'name' => 'Hossein Tehrani',
                'mobile' => '09121539485',
                'email' => 'h.tehrani1994@gmail.com',
                'city' => 'Tehran',
                'address' => 'sout janatAbad bahonar',
                'type'=>'LEGAL',
                'company_name'=>'ASANTENDER',
                'status'=>1,
                'password' => \Hash::make('123123123'),
            ];
        }
    }
}
