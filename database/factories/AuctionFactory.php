<?php

namespace Database\Factories;

use App\Models\Auction;
use Illuminate\Database\Eloquent\Factories\Factory;
use Ybazli\Faker\Facades\Faker;

class AuctionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Auction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'=>$this->faker->sentence,
            'city'=>$this->faker->city,
            'address'=>$this->faker->address,
            'auction_code'=>$this->faker->numberBetween(100000,9999999),
            'title_advertiser'=>$this->faker->company,
            'source_adv'=>'ستاد ایران',
            'setad'=>$this->faker->numberBetween(0,1),
            'invitation_date'=>$this->faker->date(),
            'view_deadline_date'=>$this->faker->date(),
            'request_deadline_date'=>$this->faker->date(),
            'winner_announced_date'=>$this->faker->date(),
            'initial_publish_date'=>$this->faker->date(),
            'publish_date'=>$this->faker->date(),
            'free_date'=>$this->faker->date(),
            'commodity_num'=>$this->faker->numberBetween(152369547,152369547254),
            'commodity_description'=>$this->faker->sentence(),
            'base_price'=>$this->faker->numberBetween(152369547,152369547254),
            'source_auction_num'=>'4',
            'body_adv'=>$this->faker->address.$this->faker->address,
            'status_adv'=>1,
            'asantender'=>$this->faker->numberBetween(1000,100000),
            'img_url'=>'abcdefghijklmnopqrstuvwxyz',
            'link_url'=>'abcdefghijkl',
            'admin_id'=>1,
            'central_organ_id'=>$this->faker->numberBetween(1,38),
        ];
    }
}
