<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class AdminRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'manager',
                'label' => 'مدیر اصلی',
                'type' => 'internal',
            ],
            [
                'name' => 'writer',
                'label' => 'نویسنده',
                'type' => 'internal',
            ],
            [
                'name' => 'owner',
                'label' => 'مالک',
                'type' => 'internal',
            ],
            [
                'name' => 'accountants',
                'label' => 'حسابدار',
                'type' => 'internal',
            ],
            [
                'name' => 'technical_support ',
                'label' => 'پشتیبانی فنی',
                'type' => 'internal',
            ],
            [
                'name' => 'maintainer',
                'label' => 'پشتیبانی سایت',
                'type' => 'internal',
            ],
        ];
        foreach ($data as $item) {
            Role::create($item);
        }
    }
}
