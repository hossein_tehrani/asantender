<?php

namespace Database\Seeders;

use App\Models\Province;
use Illuminate\Database\Seeder;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinces = [
            [
                'name' => 'تهران',
                'parent_id' => null,
            ],
            [
                'name' => 'خراسان رضوی',
                'parent_id' => null,
            ],
            [
                'name' => 'اصفهان',
                'parent_id' => null,
            ],
            [
                'name' => 'فارس',
                'parent_id' => null,
            ],
            [
                'name' => 'خوزستان',
                'parent_id' => null,
            ],
            [
                'name' => 'آذربایجان شرقی',
                'parent_id' => null,
            ],
            [
                'name' => 'مازندران',
                'parent_id' => null,
            ],
            [
                'name' => 'آذربایجان غربی',
                'parent_id' => null,
            ],
            [
                'name' => 'کرمان',
                'parent_id' => null,
            ],
            [
                'name' => 'سیستان و بلوچستان',
                'parent_id' => null,
            ],
            [
                'name' => 'البرز',
                'parent_id' => null,
            ],
            [
                'name' => 'گیلان',
                'parent_id' => null,
            ],
            [
                'name' => 'کرمانشاه',
                'parent_id' => null,
            ],
            [
                'name' => 'گلستان',
                'parent_id' => null,
            ],
            [
                'name' => 'هرمزگان',
                'parent_id' => null,
            ],
            [
                'name' => 'لرستان',
                'parent_id' => null,
            ],
            [
                'name' => 'همدان',
                'parent_id' => null,
            ],
            [
                'name' => 'کردستان',
                'parent_id' => null,
            ],
            [
                'name' => 'مرکزی',
                'parent_id' => null,
            ],
            [
                'name' => 'قم',
                'parent_id' => null,
            ],
            [
                'name' => 'قزوین',
                'parent_id' => null,
            ],
            [
                'name' => 'اردبیل',
                'parent_id' => null,
            ],
            [
                'name' => 'بوشهر',
                'parent_id' => null,
            ],
            [
                'name' => 'یزد',
                'parent_id' => null,
            ],
            [
                'name' => 'زنجان',
                'parent_id' => null,
            ],
            [
                'name' => 'چهارمحال و بختیاری',
                'parent_id' => null,
            ],
            [
                'name' => 'خراسان شمالی',
                'parent_id' => null,
            ],
            [
                'name' => 'خراسان جنوبی',
                'parent_id' => null,
            ],
            [
                'name' => 'کهگیلویه و بویراحمد',
                'parent_id' => null,
            ],
            [
                'name' => 'سمنان',
                'parent_id' => null,
            ],
            [
                'name' => 'ایلام',
                'parent_id' => null,
            ],

        ];
        foreach ($provinces as $province) {
            Province::create($province);
        }
    }
}
