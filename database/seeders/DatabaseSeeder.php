<?php

namespace Database\Seeders;

use App\Models\Auction;
use App\Models\Province;
use Database\Factories\ProvinceFactory;
use Illuminate\Database\Seeder;
use App\Models\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminConfigSeeder::class);
        $this->call(AdminRoleSeeder::class);
        $this->call(ProvinceSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(PlanSeeder::class);
        $this->call(DiscountSeeder::class);
//        User::factory()
//            ->times(1)
//            ->create();
        $this->call(CentralOrganSeeder::class);
        $this->call(WorkGroupSeeder::class);
//        Auction::factory()
//            ->has(Province::factory()->count(4))
//            ->times(100)
//            ->create();
        $this->call(TenderSeeder::class);
        $this->call(InquirySeeder::class);
        $this->call(ConfigSeeder::class);
        $this->call(BlogSeeder::class);
    }
}
