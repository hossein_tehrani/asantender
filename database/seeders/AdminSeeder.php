<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admins = [
            [
                'name' => 'Hossein tehrani',
                'mobile' => '09121539485',
                'email' => 'h.tehrani1994@gmail.com',
                'phone' => '09121539485',
                'password' => Hash::make(123123123),
                'status' => 'free',
                'address' => 'tehran South janatabad',
                'type' => 'internal_staff',
                'status_access' => '1'
            ]
            ,
            [
                'name' => 'test panel',
                'mobile' => '09151539485',
                'email' => 'test@test.test',
                'phone' => '09121539485',
                'password' => Hash::make(123123123),
                'status' => 'free',
                'address' => 'tehran South janatabad',
                'type' => 'internal_staff',
                'status_access' => '1'
            ]
            ,
            [
                'name' => 'role test',
                'mobile' => '09141539485',
                'email' => 'test@@test.test',
                'phone' => '09121532485',
                'password' => Hash::make(123123123),
                'status' => 'free',
                'address' => 'tehran South janatabad',
                'type' => 'internal_staff',
                'status_access' => '1'
            ]
            ,

        ];
        foreach($admins as $admin){
            $admin=Admin::create($admin);
            $admin->roles()->sync([1,2,3,4,5,6]);
        }
    }
}
