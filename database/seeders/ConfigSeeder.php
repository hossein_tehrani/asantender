<?php

namespace Database\Seeders;

use App\Models\Config;
use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Config::create([
            'length_advertise_admin_show'=>'20',
            'pagination_advertise_admin'=>'5',
            'welcome_message_user_dashboard'=>'welcome',
            'welcome_message_admin_dashboard'=>'welcome',
            'url_header_user_site'=>'URL',
            'url_header_user_dashboard'=>'URL',
            'url_footer_user_site'=>'URL',
            'url_footer_user_dashboard'=>'URL',
            'initial_work_groups_change_user_subscribed'=>'2',
        ]);
    }
}
