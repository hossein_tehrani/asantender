<?php

namespace Database\Seeders;

use App\Models\DiscountCode;
use Illuminate\Database\Seeder;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $discounts=[
            [
                'name'=>'DiscountCOde1',
                'serial'=>'QwErTy55YY2RB',
                'expire'=>'34',
                'count'=>'190',
                'percent'=>'50',
                'work_groups_changes'=>'12',
                'status'=>'1',
                'plan_id'=>'1',
            ],
            [
                'name'=>'DiscountCOde2',
                'serial'=>'QwErTysd55YY2RB',
                'expire'=>'37',
                'count'=>'123',
                'percent'=>'33',
                'work_groups_changes'=>'345',
                'status'=>'1',
                'plan_id'=>'2',
            ]
        ];
        foreach($discounts as $discount){
            DiscountCode::create($discount);
        }
    }
}
