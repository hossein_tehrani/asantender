<?php

namespace Database\Seeders;

use App\Models\ConfigAdmin;
use Illuminate\Database\Seeder;

class AdminConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ConfigAdmin::create([
            'paginate_show_admins'=>5
        ]);
    }
}
