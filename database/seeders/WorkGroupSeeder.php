<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\WorkGroup;

class WorkGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workGroups=[
            [
                'title'=>'آب و فاضلاب','type'=>'TENDER','priority'=>'1','parent_id'=>'','image'=>'/storage/workgroup/1.png'
            ],
            [
                'title'=>'ابنیه و ساختمان','type'=>'TENDER','priority'=>'2','parent_id'=>'','image'=>'/storage/workgroup/2.png'
            ],
            [
                'title'=>'راهسازی، شهرسازی و راه آهن','type'=>'TENDER','priority'=>'3','parent_id'=>'','image'=>'/storage/workgroup/3.png'
            ],
            [
                'title'=>'برق ، نیرو و انرژی','type'=>'TENDER','priority'=>'4','parent_id'=>'','image'=>'/storage/workgroup/4.png'
            ],
            [
                'title'=>'مخابرات و الکترونیک','type'=>'TENDER','priority'=>'5','parent_id'=>'','image'=>'/storage/workgroup/5.png'
            ],
            [
                'title'=>'کامپیوتر و فن آوری اطلاعات','type'=>'TENDER','priority'=>'6','parent_id'=>'','image'=>'/storage/workgroup/6.png'
            ],
            [
                'title'=>'ابزارآلات، یراق آلات و تجهیزات','type'=>'TENDER','priority'=>'7','parent_id'=>'','image'=>'/storage/workgroup/7.png'
            ],
            [
                'title'=>'اتوماسیون صنعتی','type'=>'TENDER','priority'=>'8','parent_id'=>'','image'=>'/storage/workgroup/8.png'
            ],
            [
                'title'=>'صنایع فلزی ، ماشین افزار و خدمات صنعتی','type'=>'TENDER','priority'=>'9','parent_id'=>'','image'=>'/storage/workgroup/9.png'
            ],
            [
                'title'=>'فلزات و اقلام فلزی','type'=>'TENDER','priority'=>'10','parent_id'=>'','image'=>'/storage/workgroup/10.png'
            ],
            [
                'title'=>'ماشین آلات صنعتی','type'=>'TENDER','priority'=>'11','parent_id'=>'','image'=>'/storage/workgroup/11.png'
            ],
            [
                'title'=>'تاسیسات الکتریکی و مکانیکی، برودتی و حرارتی','type'=>'TENDER','priority'=>'12','parent_id'=>'','image'=>'/storage/workgroup/12.png'
            ],
            [
                'title'=>'لوله، اتصالات ،شیرآلات، دریچه و منهول','type'=>'TENDER','priority'=>'13','parent_id'=>'','image'=>'/storage/workgroup/13.png'
            ],
            [
                'title'=>'آتش نشانی و حفاظت الکترونیک','type'=>'TENDER','priority'=>'14','parent_id'=>'','image'=>'/storage/workgroup/14.png'
            ],
            [
                'title'=>'معدن،مواد نسوز ، شیشه و چوب','type'=>'TENDER','priority'=>'15','parent_id'=>'','image'=>'/storage/workgroup/15.png'
            ],
            [
                'title'=>'مواد شیمیایی، اقلام پلیمری، لاستیکی، پلاستیکی، رنگ و عایق','type'=>'TENDER','priority'=>'16','parent_id'=>'','image'=>'/storage/workgroup/16.png'
            ],
            [
                'title'=>'خودرو، قطعات و لوازم و تجهیزات جانبی','type'=>'TENDER','priority'=>'17','parent_id'=>'','image'=>'/storage/workgroup/17.png'
            ],
            [
                'title'=>'پزشکی ، درمانی و بهداشتی','type'=>'TENDER','priority'=>'18','parent_id'=>'','image'=>'/storage/workgroup/18.png'
            ],
            [
                'title'=>'کشاورزی، دامپروری و شیلات','type'=>'TENDER','priority'=>'19','parent_id'=>'','image'=>'/storage/workgroup/19.png'
            ],
            [
                'title'=>'مواد غذایی و خوراکی','type'=>'TENDER','priority'=>'20','parent_id'=>'','image'=>'/storage/workgroup/20.png'
            ],
            [
                'title'=>'منسوجات، پوشاک، فرش و چرم','type'=>'TENDER','priority'=>'21','parent_id'=>'','image'=>'/storage/workgroup/21.png'
            ],
            [
                'title'=>'دکوراسیون، ملزومات اداری و مبلمان','type'=>'TENDER','priority'=>'22','parent_id'=>'','image'=>'/storage/workgroup/22.png'
            ],
            [
                'title'=>'لوازم خانگی، آشپزخانه، رستوان، رختشورخانه و هتلینگ','type'=>'TENDER','priority'=>'23','parent_id'=>'','image'=>'/storage/workgroup/23.png'
            ],
            [
                'title'=>'تجهیزات، خدمات ورزشی ،تفریحی و مبلمان شهری','type'=>'TENDER','priority'=>'24','parent_id'=>'','image'=>'/storage/workgroup/24.png'
            ],
            [
                'title'=>'مدیریت ،بیمه، امور حقوقی، مالی و پژوهشی','type'=>'TENDER','priority'=>'25','parent_id'=>'','image'=>'/storage/workgroup/25.png'
            ],
            [
                'title'=>'محیط زیست ، زمین شناسی و نقشه برداری','type'=>'TENDER','priority'=>'26','parent_id'=>'','image'=>'/storage/workgroup/26.png'
            ],
            [
                'title'=>'خدمات و نیروی انسانی','type'=>'TENDER','priority'=>'27','parent_id'=>'','image'=>'/storage/workgroup/27.png'
            ],
            [
                'title'=>'حمل و نقل، انبارداری ، پخش و توزیع','type'=>'TENDER','priority'=>'28','parent_id'=>'','image'=>'/storage/workgroup/28.png'
            ],
            [
                'title'=>'چاپ، تبلیغات، امور فرهنگی و کاغذ','type'=>'TENDER','priority'=>'29','parent_id'=>'','image'=>'/storage/workgroup/29.png'
            ],
            [
                'title'=>'صنایع دریایی، هوایی و خدمات وابسته','type'=>'TENDER','priority'=>'30','parent_id'=>'','image'=>'/storage/workgroup/30.png'
            ],
            [
                'title'=>'نفت و گاز و پتروشیمی','type'=>'TENDER','priority'=>'31','parent_id'=>'','image'=>'/storage/workgroup/31.png'
            ],
            [
                'title'=>'بین المللی داخلی، سرمایه گذاری و مشارکت','type'=>'TENDER','priority'=>'32','parent_id'=>'','image'=>'/storage/workgroup/32.png'
            ],
            [
                'title'=>'کالا','type'=>'INQUIRY','priority'=>'33','parent_id'=>'','image'=>'/storage/workgroup/33.png'
            ],
            [
                'title'=>'خدمات','type'=>'INQUIRY','priority'=>'34','parent_id'=>'','image'=>'/storage/workgroup/34.png'
            ],
            [
                'title'=>'املاک و مستقلات','type'=>'AUCTION','priority'=>'35','parent_id'=>'','image'=>'/storage/workgroup/35.png'
            ],
            [
                'title'=>'اقلام ساختمانی و تاسیسات','type'=>'AUCTION','priority'=>'36','parent_id'=>'','image'=>'/storage/workgroup/36.png'
            ],
            [
                'title'=>'ابزارآلات و ماشین آلات صنعتی','type'=>'AUCTION','priority'=>'37','parent_id'=>'','image'=>'/storage/workgroup/37.png'
            ],
            [
                'title'=>'آب و فاضلاب، نفت و گاز','type'=>'AUCTION','priority'=>'38','parent_id'=>'','image'=>'/storage/workgroup/38.png'
            ],
            [
                'title'=>'برق و الکترونیک و انرژی','type'=>'AUCTION','priority'=>'39','parent_id'=>'','image'=>'/storage/workgroup/39.png'
            ],
            [
                'title'=>'کامپیوتر، ماشین های اداری،تجهیزات سمعی و بصری','type'=>'AUCTION','priority'=>'40','parent_id'=>'','image'=>'/storage/workgroup/40.png'
            ],
            [
                'title'=>'مخابرات و الکترونیک','type'=>'AUCTION','priority'=>'41','parent_id'=>'','image'=>'/storage/workgroup/41.png'
            ],
            [
                'title'=>'پزشکی ،آزمایشگاهی، بهداشتی و آرایشی','type'=>'AUCTION','priority'=>'42','parent_id'=>'','image'=>'/storage/workgroup/42.png'
            ],
            [
                'title'=>'پوشاک، منسوجات ، فرش و چرم','type'=>'AUCTION','priority'=>'43','parent_id'=>'','image'=>'/storage/workgroup/43.png'
            ],
            [
                'title'=>'چوب و کاغذ و تبلیغات','type'=>'AUCTION','priority'=>'44','parent_id'=>'','image'=>'/storage/workgroup/44.png'
            ],
            [
                'title'=>'خدمات شهری ،مدیریت پسماند، زباله و بازیافت','type'=>'AUCTION','priority'=>'45','parent_id'=>'','image'=>'/storage/workgroup/45.png'
            ],
            [
                'title'=>'خودرو ، قطعات و لوازم یدکی','type'=>'AUCTION','priority'=>'46','parent_id'=>'','image'=>'/storage/workgroup/46.png'
            ],
            [
                'title'=>'فلزات، اقلام و سازه های فلزی و مخازن','type'=>'AUCTION','priority'=>'47','parent_id'=>'','image'=>'/storage/workgroup/47.png'
            ],
            [
                'title'=>'تجهیزات ایمنی، آتش نشانی و حفاظت الکترونیک','type'=>'AUCTION','priority'=>'48','parent_id'=>'','image'=>'/storage/workgroup/48.png'
            ],
            [
                'title'=>'کشاورزی، دام و طیور و شیلات','type'=>'AUCTION','priority'=>'49','parent_id'=>'','image'=>'/storage/workgroup/49.png'
            ],
            [
                'title'=>'مواد غذایی و خوراکی','type'=>'AUCTION','priority'=>'50','parent_id'=>'','image'=>'/storage/workgroup/50.png'
            ],
            [
                'title'=>'لوازم خانگی و اداری تفریحی و ورزشی','type'=>'AUCTION','priority'=>'51','parent_id'=>'','image'=>'/storage/workgroup/51.png'
            ],
            [
                'title'=>'مواد شیمیایی، پلاستیکی،لاستیکی، پلیمری، رنگ و عایق','type'=>'AUCTION','priority'=>'52','parent_id'=>'','image'=>'/storage/workgroup/52.png'
            ],
            [
                'title'=>'معدن،  فراورده های معدنی و مواد نسوز','type'=>'AUCTION','priority'=>'53','parent_id'=>'','image'=>'/storage/workgroup/53.png'
            ],
            [
                'title'=>'صنایع دریایی و هوایی','type'=>'AUCTION','priority'=>'54','parent_id'=>'','image'=>'/storage/workgroup/54.png'
            ],
            [
                'title'=>'سهام، نمایندگی و حق امتیاز','type'=>'AUCTION','priority'=>'55','parent_id'=>'','image'=>'/storage/workgroup/55.png'
            ],
            [
                'title'=>' پروژه های طراحی، مهندسی و مشاوره آب و فاضلاب','type'=>'TENDER','priority'=>'1','parent_id'=>'1'
            ],
            [
                'title'=>' خدمات تعمیر، نگهداری و ساماندهی آب و فاضلاب','type'=>'TENDER','priority'=>'2','parent_id'=>'1'
            ],
            [
                'title'=>' پیمانکاری آبیاری، زهکشی، آبخیزداری و کنترل سیل','type'=>'TENDER','priority'=>'3','parent_id'=>'1'
            ],
            [
                'title'=>' احداث و راهبری تصفیه خانه و آب شیرین کن','type'=>'TENDER','priority'=>'4','parent_id'=>'1'
            ],
            [
                'title'=>' پیمانکاری شبکه های انتقال آب و فاضلاب','type'=>'TENDER','priority'=>'5','parent_id'=>'1'
            ],
            [
                'title'=>' حفاری و تجهیز چاه و مخازن بتنی','type'=>'TENDER','priority'=>'6','parent_id'=>'1'
            ],
            [
                'title'=>' سدسازی','type'=>'TENDER','priority'=>'7','parent_id'=>'1'
            ],
            [
                'title'=>' پمپ، الکتروپمپ و کنتور آب','type'=>'TENDER','priority'=>'8','parent_id'=>'1'
            ],
            [
                'title'=>' تجهیزات تصفیه ( کلرزنی، فیلتر شنی و ... )','type'=>'TENDER','priority'=>'9','parent_id'=>'1'
            ],
            [
                'title'=>' خريد و تامين آب','type'=>'TENDER','priority'=>'10','parent_id'=>'1'
            ],
            [
                'title'=>' خرید کالاهای گروه آب و فاضلاب','type'=>'TENDER','priority'=>'11','parent_id'=>'1'
            ],
            [
                'title'=>' پروژه های طراحی، مهندسی و مشاوره ساختمان و ابنیه','type'=>'TENDER','priority'=>'1','parent_id'=>'2'
            ],
            [
                'title'=>' نازک کاری، مقاوم سازی، بازسازی، تعمیرات و نگهداری ساختمان و ابنیه','type'=>'TENDER','priority'=>'2','parent_id'=>'2'
            ],
            [
                'title'=>' پیمانکاری ساختمان، ابنیه، اماکن مسکونی، ورزشی، رفاهی، تجاری و اداری','type'=>'TENDER','priority'=>'3','parent_id'=>'2'
            ],
            [
                'title'=>' احداث پارک، محوطه سازی، حصارکشی و نوسازی اراضی','type'=>'TENDER','priority'=>'4','parent_id'=>'2'
            ],
            [
                'title'=>' احداث سوله و اسکلت فلزی','type'=>'TENDER','priority'=>'5','parent_id'=>'2'
            ],
            [
                'title'=>' اقلام پیش ساخته','type'=>'TENDER','priority'=>'6','parent_id'=>'2'
            ],
            [
                'title'=>' درب و پنجره و چهارچوب','type'=>'TENDER','priority'=>'7','parent_id'=>'2'
            ],
            [
                'title'=>' مصالح ساختمانی','type'=>'TENDER','priority'=>'8','parent_id'=>'2'
            ],
            [
                'title'=>' تخريب ساختمان، گودبرداري و خاکبرداري','type'=>'TENDER','priority'=>'9','parent_id'=>'2'
            ],
            [
                'title'=>' خرید کالاهای گروه ساختمان و ابنیه','type'=>'TENDER','priority'=>'10','parent_id'=>'2'
            ],
            [
                'title'=>' مرمت و نگهداری آثار تاریخی و ساخت تندیس و سردر','type'=>'TENDER','priority'=>'11','parent_id'=>'2'
            ],
            [
                'title'=>' خدمات آرامستان و ساخت قبور','type'=>'TENDER','priority'=>'12','parent_id'=>'2'
            ],
            [
                'title'=>' خريد و اجاره ساختمان','type'=>'TENDER','priority'=>'13','parent_id'=>'2'
            ],
            [
                'title'=>' پروژه های طراحی، مهندسی و مشاوره راهسازی و شهرسازی','type'=>'TENDER','priority'=>'1','parent_id'=>'3'
            ],
            [
                'title'=>' پیمانکاری راهسازی و شهرسازی، خیابان سازی و آسفالت','type'=>'TENDER','priority'=>'2','parent_id'=>'3'
            ],
            [
                'title'=>' احداث پل و تونل','type'=>'TENDER','priority'=>'3','parent_id'=>'3'
            ],
            [
                'title'=>' خرید آسفالت و قیر','type'=>'TENDER','priority'=>'4','parent_id'=>'3'
            ],
            [
                'title'=>' تجهیزات ترافیکی و تابلو های هشداری','type'=>'TENDER','priority'=>'5','parent_id'=>'3'
            ],
            [
                'title'=>' مصالح راهسازی','type'=>'TENDER','priority'=>'6','parent_id'=>'3'
            ],
            [
                'title'=>' خرید کالاهای گروه راهسازی و شهرسازی','type'=>'TENDER','priority'=>'7','parent_id'=>'3'
            ],
            [
                'title'=>' عملیات راهداری، جدولگذاری، ترمیم، پیاده رو سازی و تسطیح اراضی','type'=>'TENDER','priority'=>'8','parent_id'=>'3'
            ],
            [
                'title'=>' خدمات ترافیک شهری، رنگ آمیزی و خطکشی معابر، المان های شهری','type'=>'TENDER','priority'=>'9','parent_id'=>'3'
            ],
            [
                'title'=>' خدمات جانبی گروه راهسازی و شهرسازی','type'=>'TENDER','priority'=>'10','parent_id'=>'3'
            ],
            [
                'title'=>' پیمانکاری خطوط ریلی و راه آهن','type'=>'TENDER','priority'=>'11','parent_id'=>'3'
            ],
            [
                'title'=>' خرید تجهیزات، واگن و قطعات خطوط ریلی','type'=>'TENDER','priority'=>'12','parent_id'=>'3'
            ],
            [
                'title'=>' پروژه های طراحی، مهندسی و مشاوره راه آهن و خطوط ریلی','type'=>'TENDER','priority'=>'13','parent_id'=>'3'
            ],
            [
                'title'=>' خدمات سیر و حرکت، راهبری، تعمیر و نگهداری خطوط ریلی و راه آهن','type'=>'TENDER','priority'=>'14','parent_id'=>'3'
            ],
            [
                'title'=>' پروژه های طراحی، مهندسی و مشاوره برق و نیرو','type'=>'TENDER','priority'=>'1','parent_id'=>'4'
            ],
            [
                'title'=>' خدمات تعمیر، نگهداری و ساماندهی برق و نیرو','type'=>'TENDER','priority'=>'2','parent_id'=>'4'
            ],
            [
                'title'=>' پیمانکاری شبکه های انتقال، توزیع و روشنایی','type'=>'TENDER','priority'=>'3','parent_id'=>'4'
            ],
            [
                'title'=>' نیروگاه و توربینهای برقی','type'=>'TENDER','priority'=>'4','parent_id'=>'4'
            ],
            [
                'title'=>' خرید سیم و کابل برق به همراه متعلقات','type'=>'TENDER','priority'=>'5','parent_id'=>'4'
            ],
            [
                'title'=>' خرید تابلو برق، ترانسفورماتور ، پست های برق و کنتور','type'=>'TENDER','priority'=>'6','parent_id'=>'4'
            ],
            [
                'title'=>' خرید لوازم روشنایی و کلیدهای جریان ( پروژکتور، چراغ و ... )','type'=>'TENDER','priority'=>'7','parent_id'=>'4'
            ],
            [
                'title'=>' خرید مولد های برقی- انواع ژنراتور و الکتروموتور ','type'=>'TENDER','priority'=>'8','parent_id'=>'4'
            ],
            [
                'title'=>' خرید باطری، منابع تغذیه و UPS','type'=>'TENDER','priority'=>'9','parent_id'=>'4'
            ],
            [
                'title'=>' خرید  دیگر اقلام گروه برق و نیرو','type'=>'TENDER','priority'=>'10','parent_id'=>'4'
            ],
            [
                'title'=>' انرژی','type'=>'TENDER','priority'=>'11','parent_id'=>'4'
            ],
            [
                'title'=>' پروژه های طراحی، مهندسی و مشاوره مخابرات و الکترونیک','type'=>'TENDER','priority'=>'1','parent_id'=>'5'
            ],
            [
                'title'=>' خدمات تعمیر، نگهداری و ساماندهی مخابرات و الکترونیک','type'=>'TENDER','priority'=>'2','parent_id'=>'5'
            ],
            [
                'title'=>' عملیات اجرائی و پیمانکاری مخابرات و الکترونیک','type'=>'TENDER','priority'=>'3','parent_id'=>'5'
            ],
            [
                'title'=>'  مراکز تلفن و سانترال','type'=>'TENDER','priority'=>'4','parent_id'=>'5'
            ],
            [
                'title'=>' خرید تجهیزات رادیویی، استودیو و ماهواره','type'=>'TENDER','priority'=>'5','parent_id'=>'5'
            ],
            [
                'title'=>' خرید سیم و کابل مخابرات به همراه متعلقات','type'=>'TENDER','priority'=>'6','parent_id'=>'5'
            ],
            [
                'title'=>' خرید دکل و تير مخابرات','type'=>'TENDER','priority'=>'7','parent_id'=>'5'
            ],
            [
                'title'=>' خرید تابلو های دیجیتالی و تلویزیون های شهری','type'=>'TENDER','priority'=>'8','parent_id'=>'5'
            ],
            [
                'title'=>' خرید کالاهای گروه مخابرات و الکترونیک','type'=>'TENDER','priority'=>'9','parent_id'=>'5'
            ],
            [
                'title'=>' خرید و پشتیبانی کامپیوتر، سخت افزار و ماشین های اداری','type'=>'TENDER','priority'=>'1','parent_id'=>'6'
            ],
            [
                'title'=>' خرید، طراحی و پشتیبانی نرم افزار و اتوماسیون اداری','type'=>'TENDER','priority'=>'2','parent_id'=>'6'
            ],
            [
                'title'=>' خرید، طراحی و خدمات شبکه، پهنای باند و سرور','type'=>'TENDER','priority'=>'3','parent_id'=>'6'
            ],
            [
                'title'=>' خرید تجهیزات و لوازم بانکی و فروشگاهی','type'=>'TENDER','priority'=>'4','parent_id'=>'6'
            ],
            [
                'title'=>' هوشمند سازي و سيستم هاي سمعي و بصري ','type'=>'TENDER','priority'=>'5','parent_id'=>'6'
            ],
            [
                'title'=>' ورود اطلاعات ، آرشيو الکترونيکي اسناد و داده آمايي','type'=>'TENDER','priority'=>'6','parent_id'=>'6'
            ],
            [
                'title'=>' خرید نوت بوک و لپ تاپ','type'=>'TENDER','priority'=>'7','parent_id'=>'6'
            ],
            [
                'title'=>' خدمات پشتيباني ، تعمير و نگهداري','type'=>'TENDER','priority'=>'8','parent_id'=>'6'
            ],
            [
                'title'=>' تجهیزات انتقال ( نوارنقاله، کانوایر و ... )','type'=>'TENDER','priority'=>'1','parent_id'=>'7'
            ],
            [
                'title'=>' تجهیزات توزین ( باسکول و ترازو )','type'=>'TENDER','priority'=>'2','parent_id'=>'7'
            ],
            [
                'title'=>' اقلام مهندسی و کارگاه های آموزشی','type'=>'TENDER','priority'=>'3','parent_id'=>'7'
            ],
            [
                'title'=>' ابزار، یراق، تجهیزات و قطعات صنعتی','type'=>'TENDER','priority'=>'4','parent_id'=>'7'
            ],
            [
                'title'=>' بیرینگ، بلبرینگ، رولبرینگ و اورینگ','type'=>'TENDER','priority'=>'5','parent_id'=>'7'
            ],
            [
                'title'=>' گیربکس و متعلقات','type'=>'TENDER','priority'=>'6','parent_id'=>'7'
            ],
            [
                'title'=>' کمپرسور و متعلقات','type'=>'TENDER','priority'=>'7','parent_id'=>'7'
            ],
            [
                'title'=>' تجهیزات اتصالات ( ابزار و لوازم جوش ، پیچ ،مهره و...)','type'=>'TENDER','priority'=>'8','parent_id'=>'7'
            ],
            [
                'title'=>' اتوماسيون صنعتي و مونيتورينگ (طراحي و مشاوره و اجرا ...)','type'=>'TENDER','priority'=>'1','parent_id'=>'8'
            ],
            [
                'title'=>' ابزار دقيق و ادوات اندازه گيري','type'=>'TENDER','priority'=>'2','parent_id'=>'8'
            ],
            [
                'title'=>' تست و بازرسي فني ، کنترل خوردگي ، آزمونهاي مخرب و غير مخرب','type'=>'TENDER','priority'=>'3','parent_id'=>'8'
            ],
            [
                'title'=>' کنترل صنعتي ،تست و آزمايشگاهها ( کنترلر ، استاتر ، عيب ياب و .. )','type'=>'TENDER','priority'=>'4','parent_id'=>'8'
            ],
            [
                'title'=>' سيستمهاي تله متري و تله کنترل','type'=>'TENDER','priority'=>'5','parent_id'=>'8'
            ],
            [
                'title'=>' تجهيزات مهندسي و نقشه برداري','type'=>'TENDER','priority'=>'6','parent_id'=>'8'
            ],
            [
                'title'=>' آهنگری، ورق کاری و کانالسازی','type'=>'TENDER','priority'=>'1','parent_id'=>'9'
            ],
            [
                'title'=>' جوشکاری و خدمات جوش','type'=>'TENDER','priority'=>'2','parent_id'=>'9'
            ],
            [
                'title'=>' ریخته گری، قالبسازی، ماشین کاری، تراشکاری و برشکاری','type'=>'TENDER','priority'=>'3','parent_id'=>'9'
            ],
            [
                'title'=>' پوشش دهی فلزات ( گالوانیزه کردن، آبکاری و لعابکاری )','type'=>'TENDER','priority'=>'4','parent_id'=>'9'
            ],
            [
                'title'=>' تعمير و نگهداري و نظافت صنايع ، ماشين آلات ، دستگاه ها وPM ','type'=>'TENDER','priority'=>'5','parent_id'=>'9'
            ],
            [
                'title'=>' طراحی، احداث، راه اندازی و خدمات صنایع و کارخانجات','type'=>'TENDER','priority'=>'6','parent_id'=>'9'
            ],
            [
                'title'=>' آلومینیوم، پروفیل و فرآورده های وابسته','type'=>'TENDER','priority'=>'1','parent_id'=>'10'
            ],
            [
                'title'=>' آهن، فولاد، چدن و فرآورده های وابسته','type'=>'TENDER','priority'=>'2','parent_id'=>'10'
            ],
            [
                'title'=>' دکل، تیر، پایه فلزی و برج روشنایی','type'=>'TENDER','priority'=>'3','parent_id'=>'10'
            ],
            [
                'title'=>' کانکس، کاروان، کانتینر و خدمات جانبی','type'=>'TENDER','priority'=>'4','parent_id'=>'10'
            ],
            [
                'title'=>' پل عابر، سایه بان، ایستگاه، نرده، راه پله','type'=>'TENDER','priority'=>'5','parent_id'=>'10'
            ],
            [
                'title'=>' پاتیل، بشکه، پالت و سایر ظروف فلزی','type'=>'TENDER','priority'=>'6','parent_id'=>'10'
            ],
            [
                'title'=>' سرب، روی، قلع، نیکل، مس و برنج','type'=>'TENDER','priority'=>'7','parent_id'=>'10'
            ],
            [
                'title'=>' سوله، غرفه، داربست،سازه و استراکچر فلزی','type'=>'TENDER','priority'=>'8','parent_id'=>'10'
            ],
            [
                'title'=>' استیل، استنلس استیل و فرآورده های وابسته','type'=>'TENDER','priority'=>'9','parent_id'=>'10'
            ],
            [
                'title'=>' مخازن فلزی، تحت فشار، میعانات و منبع های هوایی','type'=>'TENDER','priority'=>'10','parent_id'=>'10'
            ],
            [
                'title'=>' مفتول، فنس، توری، سیم بکسل، طناب، فنر، فویل، قوطی و ...','type'=>'TENDER','priority'=>'11','parent_id'=>'10'
            ],
            [
                'title'=>' ماشین آلات صنعتی، ماشین افزار','type'=>'TENDER','priority'=>'1','parent_id'=>'11'
            ],
            [
                'title'=>' جرثقیل، بالابر و لیفتراک','type'=>'TENDER','priority'=>'2','parent_id'=>'11'
            ],
            [
                'title'=>' ماشین آلات ساختمانی، راهسازی، حفاری و معدن','type'=>'TENDER','priority'=>'3','parent_id'=>'11'
            ],
            [
                'title'=>' ماشین آلات خدمات شهری، نظافتی و بازیافت','type'=>'TENDER','priority'=>'4','parent_id'=>'11'
            ],
            [
                'title'=>' ماشین آلات صنایع غذایی،کشاورزی و دامپروری','type'=>'TENDER','priority'=>'5','parent_id'=>'11'
            ],
            [
                'title'=>' ماشین آلات صنایع چوبی، نساجی، چاپ و بسته بندی','type'=>'TENDER','priority'=>'6','parent_id'=>'11'
            ],
            [
                'title'=>' طراحی، خرید، اجرا و نگهداری تاسیسات ساختمانی و موتورخانه','type'=>'TENDER','priority'=>'1','parent_id'=>'12'
            ],
            [
                'title'=>' سیستم های گرمایشی، حرارتی، شوفاژ، هیتر و مشعل','type'=>'TENDER','priority'=>'2','parent_id'=>'12'
            ],
            [
                'title'=>' سیستم های سرمایشی، تهویه، سردخانه، هواساز و کولر','type'=>'TENDER','priority'=>'3','parent_id'=>'12'
            ],
            [
                'title'=>' طراحی، خرید، اجرا و نگهداری آسانسور و پله برقی','type'=>'TENDER','priority'=>'4','parent_id'=>'12'
            ],
            [
                'title'=>' لوله هاي پلي اتيلن ، پلي پروپيلن ، کاروگيت و دوجداره','type'=>'TENDER','priority'=>'1','parent_id'=>'13'
            ],
            [
                'title'=>' لوله هاي فلزي ، فولادي ، چدني ، گالوانيزه ، مانسيمان ، داکتيل و درزدار','type'=>'TENDER','priority'=>'2','parent_id'=>'13'
            ],
            [
                'title'=>' لوله هاي پليکا ، UPVC ، PVC','type'=>'TENDER','priority'=>'3','parent_id'=>'13'
            ],
            [
                'title'=>' لوله هاي فايبرگلاس ، GRP ، GRE ، FRP','type'=>'TENDER','priority'=>'4','parent_id'=>'13'
            ],
            [
                'title'=>' لوله هاي سيماني بتني و آزبست','type'=>'TENDER','priority'=>'5','parent_id'=>'13'
            ],
            [
                'title'=>' فلنج و اتصالات فلنج دار','type'=>'TENDER','priority'=>'6','parent_id'=>'13'
            ],
            [
                'title'=>' منهول ، دريچه و محفظه','type'=>'TENDER','priority'=>'7','parent_id'=>'13'
            ],
            [
                'title'=>' شيرآلات ولو (کشويي ، پروانه اي ، توپي ، يکطرفه و پلاگ )','type'=>'TENDER','priority'=>'8','parent_id'=>'13'
            ],
            [
                'title'=>' طراحی، اجرا و خدمات حفاظت الکترونیک و کنترل تردد','type'=>'TENDER','priority'=>'1','parent_id'=>'14'
            ],
            [
                'title'=>' خرید تجهیزات و اقلام حفاظت الکترونیک و کنترل تردد','type'=>'TENDER','priority'=>'2','parent_id'=>'14'
            ],
            [
                'title'=>' طراحی، اجرا و خدمات آتش نشانی و اطفای حریق','type'=>'TENDER','priority'=>'3','parent_id'=>'14'
            ],
            [
                'title'=>' خرید تجهیزات  و اقلام آتش نشانی و اطفای حریق','type'=>'TENDER','priority'=>'4','parent_id'=>'14'
            ],
            [
                'title'=>' طراحی، خدمات و پیمانکاری معادن','type'=>'TENDER','priority'=>'1','parent_id'=>'15'
            ],
            [
                'title'=>' مواد و فرآورده های معدنی','type'=>'TENDER','priority'=>'2','parent_id'=>'15'
            ],
            [
                'title'=>' مواد و فرآورده های نسوز','type'=>'TENDER','priority'=>'3','parent_id'=>'15'
            ],
            [
                'title'=>' شيشه و فرآورده هاي آن','type'=>'TENDER','priority'=>'4','parent_id'=>'15'
            ],
            [
                'title'=>' چوب و فرآورده های وابسته','type'=>'TENDER','priority'=>'5','parent_id'=>'15'
            ],
            [
                'title'=>' رنگ، رزین و چسب','type'=>'TENDER','priority'=>'1','parent_id'=>'16'
            ],
            [
                'title'=>' مواد شیمیایی آلی و معدنی، گازهای طبی و صنعتی','type'=>'TENDER','priority'=>'2','parent_id'=>'16'
            ],
            [
                'title'=>' اقلام پلیمری، پلی اتیلن، GRP، کامپوزیت، PVC و فایبرگلاس','type'=>'TENDER','priority'=>'3','parent_id'=>'16'
            ],
            [
                'title'=>' ژئوممبران و ژئوتکستایل','type'=>'TENDER','priority'=>'4','parent_id'=>'16'
            ],
            [
                'title'=>' کیسه های پلیمری، نایلون و نایلکس','type'=>'TENDER','priority'=>'5','parent_id'=>'16'
            ],
            [
                'title'=>' ظروف یکبار مصرف','type'=>'TENDER','priority'=>'6','parent_id'=>'16'
            ],
            [
                'title'=>' انواع عایق، ایزوگام، اپوکسی و خدمات پوشش دهی','type'=>'TENDER','priority'=>'7','parent_id'=>'16'
            ],
            [
                'title'=>' اقلام لاستیکی، تایر، تیوپ و ...','type'=>'TENDER','priority'=>'8','parent_id'=>'16'
            ],
            [
                'title'=>' اقلام پلاستیکی، گالن، پالت و ...','type'=>'TENDER','priority'=>'9','parent_id'=>'16'
            ],
            [
                'title'=>' مواد شوينده  و پاک کننده','type'=>'TENDER','priority'=>'10','parent_id'=>'16'
            ],
            [
                'title'=>' خرید، اجاره و راهبری خودروهای نیمه سنگین و سنگین','type'=>'TENDER','priority'=>'1','parent_id'=>'17'
            ],
            [
                'title'=>' خرید، اجاره و راهبری موتورسیکلت و دوچرخه','type'=>'TENDER','priority'=>'2','parent_id'=>'17'
            ],
            [
                'title'=>' خرید، اجاره و راهبری خودروهای سبک','type'=>'TENDER','priority'=>'3','parent_id'=>'17'
            ],
            [
                'title'=>' ساخت وخربد قطعات يدکي، لوازم و تجهيزات جانبي خودرو','type'=>'TENDER','priority'=>'4','parent_id'=>'17'
            ],
            [
                'title'=>' کالا و خدمات پزشکی، بیمارستانی، اورژانس و داروخانه','type'=>'TENDER','priority'=>'1','parent_id'=>'18'
            ],
            [
                'title'=>' خرید کالا و خدمات آزمایشگاهی','type'=>'TENDER','priority'=>'2','parent_id'=>'18'
            ],
            [
                'title'=>' خرید کالا و خدمات دامپزشکی','type'=>'TENDER','priority'=>'3','parent_id'=>'18'
            ],
            [
                'title'=>' آرایشی، بهداشتی و محصولات سلولزی','type'=>'TENDER','priority'=>'4','parent_id'=>'18'
            ],
            [
                'title'=>' احداث، راه اندازی و خدمات آسیاب و سیلو','type'=>'TENDER','priority'=>'1','parent_id'=>'19'
            ],
            [
                'title'=>' کشاورزی، جنگلداری و غلات','type'=>'TENDER','priority'=>'2','parent_id'=>'19'
            ],
            [
                'title'=>' دامپروری، شیلات و خوراک طیور','type'=>'TENDER','priority'=>'3','parent_id'=>'19'
            ],
            [
                'title'=>' کود، سموم و خدمات وابسته','type'=>'TENDER','priority'=>'4','parent_id'=>'19'
            ],
            [
                'title'=>' میوه، سبزی و صیفی جات','type'=>'TENDER','priority'=>'1','parent_id'=>'20'
            ],
            [
                'title'=>' برنج ایرانی و خارجی','type'=>'TENDER','priority'=>'2','parent_id'=>'20'
            ],
            [
                'title'=>' مواد پروتئيني (گوشت و تخم مرغ و...)','type'=>'TENDER','priority'=>'3','parent_id'=>'20'
            ],
            [
                'title'=>' لبنیات و فرآورده های لبنی','type'=>'TENDER','priority'=>'4','parent_id'=>'20'
            ],
            [
                'title'=>' حبوبات و خشکبار','type'=>'TENDER','priority'=>'5','parent_id'=>'20'
            ],
            [
                'title'=>' انواع کنسرو','type'=>'TENDER','priority'=>'6','parent_id'=>'20'
            ],
            [
                'title'=>' نمک ، قند و شکر، ادویه، چای و مربا ','type'=>'TENDER','priority'=>'7','parent_id'=>'20'
            ],
            [
                'title'=>' سایرمواد غذایی','type'=>'TENDER','priority'=>'8','parent_id'=>'20'
            ],
            [
                'title'=>' پوشاک و البسه','type'=>'TENDER','priority'=>'1','parent_id'=>'21'
            ],
            [
                'title'=>' منسوجات ، پارچه ، پرده و...','type'=>'TENDER','priority'=>'2','parent_id'=>'21'
            ],
            [
                'title'=>' فرش و سجاده و موکت و...','type'=>'TENDER','priority'=>'3','parent_id'=>'21'
            ],
            [
                'title'=>' کالاي خواب (تشک ، لحاف ، پتو ، بالشت و ...)','type'=>'TENDER','priority'=>'4','parent_id'=>'21'
            ],
            [
                'title'=>' چادرهاي اسکان و کاورهاي پوششي','type'=>'TENDER','priority'=>'5','parent_id'=>'21'
            ],
            [
                'title'=>' کيف ، کفش ، پوتين و کالاهاي چرمي','type'=>'TENDER','priority'=>'6','parent_id'=>'21'
            ],
            [
                'title'=>' طراحی، تجهیز و اجرای دکوراسیون داخلی','type'=>'TENDER','priority'=>'1','parent_id'=>'22'
            ],
            [
                'title'=>' کابینت، پارتیشن، کفپوش و دیوارپوش','type'=>'TENDER','priority'=>'2','parent_id'=>'22'
            ],
            [
                'title'=>' طراحی، تجهیز و اجرای سالن های جمعی ( آمفی تئاتر و اجتماعات )','type'=>'TENDER','priority'=>'3','parent_id'=>'22'
            ],
            [
                'title'=>' تجهیزات و لوازم اداری، آموزشی، کمک آموزشی و تحصیلی','type'=>'TENDER','priority'=>'4','parent_id'=>'22'
            ],
            [
                'title'=>' لوازم التحریر و نوشت افزار','type'=>'TENDER','priority'=>'5','parent_id'=>'22'
            ],
            [
                'title'=>' مبلمان اداری','type'=>'TENDER','priority'=>'6','parent_id'=>'22'
            ],
            [
                'title'=>' لوازم خانگی','type'=>'TENDER','priority'=>'1','parent_id'=>'23'
            ],
            [
                'title'=>' تجهیزات آشپزخانه صنعتی، رستوران و سلف سرویس','type'=>'TENDER','priority'=>'2','parent_id'=>'23'
            ],
            [
                'title'=>' تجهیزات رختشویخانه و هتلینگ','type'=>'TENDER','priority'=>'3','parent_id'=>'23'
            ],
            [
                'title'=>' تجهیزات و خدمات ورزشی','type'=>'TENDER','priority'=>'1','parent_id'=>'24'
            ],
            [
                'title'=>' تجهیزات و خدمات تفریحی و بازی','type'=>'TENDER','priority'=>'2','parent_id'=>'24'
            ],
            [
                'title'=>' مبلمان پارکی و ادوات شهری','type'=>'TENDER','priority'=>'3','parent_id'=>'24'
            ],
            [
                'title'=>' چمن مصنوعی، کفپوش های ورزشی و تارتان','type'=>'TENDER','priority'=>'4','parent_id'=>'24'
            ],
            [
                'title'=>' مدیریت، استاندارد، آمار، کنترل پروژه و کیفیت','type'=>'TENDER','priority'=>'1','parent_id'=>'25'
            ],
            [
                'title'=>' خدمات بیمه','type'=>'TENDER','priority'=>'2','parent_id'=>'25'
            ],
            [
                'title'=>' خدمات بازرگانی','type'=>'TENDER','priority'=>'3','parent_id'=>'25'
            ],
            [
                'title'=>' امور مالی، مالیات، حسابداری و حسابرسی','type'=>'TENDER','priority'=>'4','parent_id'=>'25'
            ],
            [
                'title'=>' پژوهش، تحقیق و توسعه','type'=>'TENDER','priority'=>'5','parent_id'=>'25'
            ],
            [
                'title'=>' خدمات حقوقی و وکالت','type'=>'TENDER','priority'=>'6','parent_id'=>'25'
            ],
            [
                'title'=>' بهداشت، محیط زیست و کنترل آلاینده ها','type'=>'TENDER','priority'=>'1','parent_id'=>'26'
            ],
            [
                'title'=>' ایستگاه های آب و هوا شناسی','type'=>'TENDER','priority'=>'2','parent_id'=>'26'
            ],
            [
                'title'=>' مدیریت پسماند و بازیافت','type'=>'TENDER','priority'=>'3','parent_id'=>'26'
            ],
            [
                'title'=>' ممیزی انرژی و استانداردهای مرتبط','type'=>'TENDER','priority'=>'4','parent_id'=>'26'
            ],
            [
                'title'=>' نقشه برداری و نقشه کشی','type'=>'TENDER','priority'=>'5','parent_id'=>'26'
            ],
            [
                'title'=>' زمین شناسی، ژئوتکنیک، ژئوفیزیک، مقاومت مصالح، اطلاعات جغرافیایی و GIS','type'=>'TENDER','priority'=>'6','parent_id'=>'26'
            ],
            [
                'title'=>' ایاب و ذهاب، راهبری و خدمات نقلیه','type'=>'TENDER','priority'=>'1','parent_id'=>'27'
            ],
            [
                'title'=>' خدمات شهری و نظافت','type'=>'TENDER','priority'=>'2','parent_id'=>'27'
            ],
            [
                'title'=>' تهیه و طبخ غذا، خدمات رستوران و سلف سرویس','type'=>'TENDER','priority'=>'3','parent_id'=>'27'
            ],
            [
                'title'=>' حفاظت فیزیکی، اراضی و نگهبانی','type'=>'TENDER','priority'=>'4','parent_id'=>'27'
            ],
            [
                'title'=>' فضای سبز و باغبانی','type'=>'TENDER','priority'=>'5','parent_id'=>'27'
            ],
            [
                'title'=>' تامین نیروی انسانی و خدماتی','type'=>'TENDER','priority'=>'6','parent_id'=>'27'
            ],
            [
                'title'=>' خدمات اداری و دفتری، تشریفات و آبدارخانه','type'=>'TENDER','priority'=>'7','parent_id'=>'27'
            ],
            [
                'title'=>' خدمات عمومی، پشتیبانی و نگهداری','type'=>'TENDER','priority'=>'8','parent_id'=>'27'
            ],
            [
                'title'=>' امور مشترکین ( قرائت، توزیع،وصول مطالبات و اپراتوری )','type'=>'TENDER','priority'=>'9','parent_id'=>'27'
            ],
            [
                'title'=>' امور گمرکی، ترخیص، انبارداری و بازرسی','type'=>'TENDER','priority'=>'1','parent_id'=>'28'
            ],
            [
                'title'=>' تورهای مسافرتی و گردشگری','type'=>'TENDER','priority'=>'2','parent_id'=>'28'
            ],
            [
                'title'=>' خدمات پستی','type'=>'TENDER','priority'=>'3','parent_id'=>'28'
            ],
            [
                'title'=>' بارگیری، تخلیه ، پخش و توزیع','type'=>'TENDER','priority'=>'4','parent_id'=>'28'
            ],
            [
                'title'=>' حمل و نقل هوایی','type'=>'TENDER','priority'=>'5','parent_id'=>'28'
            ],
            [
                'title'=>' حمل و نقل دریایی','type'=>'TENDER','priority'=>'6','parent_id'=>'28'
            ],
            [
                'title'=>' ماکت های صنعتی، معماری و آموزشی','type'=>'TENDER','priority'=>'1','parent_id'=>'29'
            ],
            [
                'title'=>' خرید کالا و خدمات تبلیغاتی و نمایشگاهی','type'=>'TENDER','priority'=>'2','parent_id'=>'29'
            ],
            [
                'title'=>' چاپ، بسته بندی و صحافی','type'=>'TENDER','priority'=>'3','parent_id'=>'29'
            ],
            [
                'title'=>' کاغذ، مقوا و فرآورده های وابسته','type'=>'TENDER','priority'=>'4','parent_id'=>'29'
            ],
            [
                'title'=>' تامین کتاب و مجله','type'=>'TENDER','priority'=>'5','parent_id'=>'29'
            ],
            [
                'title'=>' برگزاری نمایشگاه، همایش، سمینار و دوره های آموزشی','type'=>'TENDER','priority'=>'6','parent_id'=>'29'
            ],
            [
                'title'=>' خدمات فرهنگی، هنری و موسیقی','type'=>'TENDER','priority'=>'7','parent_id'=>'29'
            ],
            [
                'title'=>' خرید تجهیزات، اجرا و خدمات فیلم سازی، تصویربرداری و ساخت تیزر','type'=>'TENDER','priority'=>'8','parent_id'=>'29'
            ],
            [
                'title'=>' طراحی، خرید، اجرا و خدمات صنایع هوایی','type'=>'TENDER','priority'=>'1','parent_id'=>'30'
            ],
            [
                'title'=>' طراحی، خرید، اجرا و خدمات صنایع دریایی','type'=>'TENDER','priority'=>'2','parent_id'=>'30'
            ],
            [
                'title'=>' پروژه های طراحی، مهندسی و مشاوره نفت، گاز و پتروشیمی','type'=>'TENDER','priority'=>'1','parent_id'=>'31'
            ],
            [
                'title'=>' پیمانکاری شبکه های انتقال و توزیع نفت، گاز، پتروشیمی و پالایشگاه','type'=>'TENDER','priority'=>'2','parent_id'=>'31'
            ],
            [
                'title'=>' روان کننده ها، روغن، گریس، ضد یخ و ضد جوش','type'=>'TENDER','priority'=>'3','parent_id'=>'31'
            ],
            [
                'title'=>' ایستگاه های تقلیل و افزایش فشار ( CGS – TBS )','type'=>'TENDER','priority'=>'4','parent_id'=>'31'
            ],
            [
                'title'=>' احداث و خدمات جایگاه های سوخت و فرآورده های نفتی','type'=>'TENDER','priority'=>'5','parent_id'=>'31'
            ],
            [
                'title'=>' پمپ، الکتروپمپ، کنتور، رگولاتور و توربین','type'=>'TENDER','priority'=>'6','parent_id'=>'31'
            ],
            [
                'title'=>' انواع کاتالیست، سنجش و آنالیز کننده ها','type'=>'TENDER','priority'=>'7','parent_id'=>'31'
            ],
            [
                'title'=>' خرید کالاهای گروه نفت، گاز و پتروشیمی','type'=>'TENDER','priority'=>'8','parent_id'=>'31'
            ],
            [
                'title'=>' خدمات، تعمیر، نگهداری و حفاظت از شبکه، لوله و انشعابات نفت، گاز و پتروشیمی','type'=>'TENDER','priority'=>'9','parent_id'=>'31'
            ],
            [
                'title'=>'بین المللی داخل ایران','type'=>'TENDER','priority'=>'1','parent_id'=>'32'
            ],
            [
                'title'=>'سرمایه گذاری و مشارکتی','type'=>'TENDER','priority'=>'2','parent_id'=>'32'
            ],
            [
                'title'=>'ابزارآلات','type'=>'INQUIRY','priority'=>'1','parent_id'=>'33'
            ],
            [
                'title'=>'ابزارآلات الکتریکی','type'=>'INQUIRY','priority'=>'2','parent_id'=>'33'
            ],
            [
                'title'=>'ابزارآلات اندازه گیری','type'=>'INQUIRY','priority'=>'3','parent_id'=>'33'
            ],
            [
                'title'=>'اتومبیل وموتورسیکلت','type'=>'INQUIRY','priority'=>'4','parent_id'=>'33'
            ],
            [
                'title'=>'اسباب بازی و سرگرمی','type'=>'INQUIRY','priority'=>'5','parent_id'=>'33'
            ],
            [
                'title'=>'امور تاسیساتی','type'=>'INQUIRY','priority'=>'6','parent_id'=>'33'
            ],
            [
                'title'=>'آب رسانی؛ مدیریت پسماند، فاضلاب و فعالیت های تصفیه','type'=>'INQUIRY','priority'=>'7','parent_id'=>'33'
            ],
            [
                'title'=>'پلاستیک و ملامین','type'=>'INQUIRY','priority'=>'8','parent_id'=>'33'
            ],
            [
                'title'=>'پوشاک و کفش','type'=>'INQUIRY','priority'=>'9','parent_id'=>'33'
            ],
            [
                'title'=>'تاسیسات و مصالح ساختمانی','type'=>'INQUIRY','priority'=>'10','parent_id'=>'33'
            ],
            [
                'title'=>'تجهیزات امنیتی','type'=>'INQUIRY','priority'=>'11','parent_id'=>'33'
            ],
            [
                'title'=>'تجهیزات خانگی، اداری و صنعتی','type'=>'INQUIRY','priority'=>'12','parent_id'=>'33'
            ],
            [
                'title'=>'تجهیزات و وسایل تحصیلی واداری','type'=>'INQUIRY','priority'=>'13','parent_id'=>'33'
            ],
            [
                'title'=>'تعمیر و نگهداری','type'=>'INQUIRY','priority'=>'14','parent_id'=>'33'
            ],
            [
                'title'=>'تولید و فرآوری صنعتی(سفارشی)','type'=>'INQUIRY','priority'=>'15','parent_id'=>'33'
            ],
            [
                'title'=>'چمدان و ساک و کیف','type'=>'INQUIRY','priority'=>'16','parent_id'=>'33'
            ],
            [
                'title'=>'دارو، پزشکی و سلامت','type'=>'INQUIRY','priority'=>'17','parent_id'=>'33'
            ],
            [
                'title'=>'زیورآلات و ساعت و عینک','type'=>'INQUIRY','priority'=>'18','parent_id'=>'33'
            ],
            [
                'title'=>'سازمان ها و کلوپ ها','type'=>'INQUIRY','priority'=>'19','parent_id'=>'33'
            ],
            [
                'title'=>'صوتی و تصویر،دوربین','type'=>'INQUIRY','priority'=>'20','parent_id'=>'33'
            ],
            [
                'title'=>'فراورده های دامی','type'=>'INQUIRY','priority'=>'21','parent_id'=>'33'
            ],
            [
                'title'=>'فرش، گلیم و موکت','type'=>'INQUIRY','priority'=>'22','parent_id'=>'33'
            ],
            [
                'title'=>'فن آوریهای ساخت و تولید','type'=>'INQUIRY','priority'=>'23','parent_id'=>'33'
            ],
            [
                'title'=>'کادووتزئینات','type'=>'INQUIRY','priority'=>'24','parent_id'=>'33'
            ],
            [
                'title'=>'کامپیوتر و فناوری اطلاعات-سخت افزار','type'=>'INQUIRY','priority'=>'25','parent_id'=>'33'
            ],
            [
                'title'=>'کامپیوتر و فناوری اطلاعات-نرم افزار','type'=>'INQUIRY','priority'=>'26','parent_id'=>'33'
            ],
            [
                'title'=>'لامپ و تجهیزات روشنائی','type'=>'INQUIRY','priority'=>'27','parent_id'=>'33'
            ],
            [
                'title'=>'لوازم خودرو و ماشین آلات','type'=>'INQUIRY','priority'=>'28','parent_id'=>'33'
            ],
            [
                'title'=>'لوازم و تجهیزات امدادی','type'=>'INQUIRY','priority'=>'29','parent_id'=>'33'
            ],
            [
                'title'=>'ماشین آلات تولیدی','type'=>'INQUIRY','priority'=>'30','parent_id'=>'33'
            ],
            [
                'title'=>'مالی و بیمه','type'=>'INQUIRY','priority'=>'31','parent_id'=>'33'
            ],
            [
                'title'=>'مبلمان و دکوراسیون','type'=>'INQUIRY','priority'=>'32','parent_id'=>'33'
            ],
            [
                'title'=>'محصولات الکترونیکی','type'=>'INQUIRY','priority'=>'33','parent_id'=>'33'
            ],
            [
                'title'=>'محصولات آرایشی و بهداشتی','type'=>'INQUIRY','priority'=>'34','parent_id'=>'33'
            ],
            [
                'title'=>'محصولات شیمیائی','type'=>'INQUIRY','priority'=>'35','parent_id'=>'33'
            ],
            [
                'title'=>'محصولات کشاورزی  ','type'=>'INQUIRY','priority'=>'36','parent_id'=>'33'
            ],
            [
                'title'=>'مخابراتی و ارتباطی','type'=>'INQUIRY','priority'=>'37','parent_id'=>'33'
            ],
            [
                'title'=>'منسوجات و چرم','type'=>'INQUIRY','priority'=>'38','parent_id'=>'33'
            ],
            [
                'title'=>'مواد غذائی و نوشیدنی','type'=>'INQUIRY','priority'=>'39','parent_id'=>'33'
            ],
            [
                'title'=>'نگهداری و خدمات فضای سبز','type'=>'INQUIRY','priority'=>'40','parent_id'=>'33'
            ],
            [
                'title'=>'ورزشی','type'=>'INQUIRY','priority'=>'41','parent_id'=>'33'
            ],
            [
                'title'=>'وسایل بسته بندی  ونقاشی ','type'=>'INQUIRY','priority'=>'42','parent_id'=>'33'
            ],
            [
                'title'=>'وسایل خانه','type'=>'INQUIRY','priority'=>'43','parent_id'=>'33'
            ],
            [
                'title'=>'اداره امور عمومی و دفاع ، تامین اجتماعی ','type'=>'INQUIRY','priority'=>'1','parent_id'=>'34'
            ],
            [
                'title'=>'استخراج معدن','type'=>'INQUIRY','priority'=>'2','parent_id'=>'34'
            ],
            [
                'title'=>'اطلاعات و ارتباطات','type'=>'INQUIRY','priority'=>'3','parent_id'=>'34'
            ],
            [
                'title'=>'آب رسانی، مدیریت پسماند، فاضلاب و فعالیت های تصفیه','type'=>'INQUIRY','priority'=>'4','parent_id'=>'34'
            ],
            [
                'title'=>'آموزش','type'=>'INQUIRY','priority'=>'5','parent_id'=>'34'
            ],
            [
                'title'=>'تامین برق، گاز، بخار و تهویه هوا','type'=>'INQUIRY','priority'=>'6','parent_id'=>'34'
            ],
            [
                'title'=>'تولید انرژیهای تجدید پذیر','type'=>'INQUIRY','priority'=>'7','parent_id'=>'34'
            ],
            [
                'title'=>'تولید صنعتی (ساخت)','type'=>'INQUIRY','priority'=>'8','parent_id'=>'34'
            ],
            [
                'title'=>'حفاری چاه','type'=>'INQUIRY','priority'=>'9','parent_id'=>'34'
            ],
            [
                'title'=>'حمل و نقل و انبارداری','type'=>'INQUIRY','priority'=>'10','parent_id'=>'34'
            ],
            [
                'title'=>'ساختمان','type'=>'INQUIRY','priority'=>'11','parent_id'=>'34'
            ],
            [
                'title'=>'سایر فعالیت‌های خدماتی','type'=>'INQUIRY','priority'=>'12','parent_id'=>'34'
            ],
            [
                'title'=>'عمده فروشی و خرده فروشی ؛ تعمیر وسایل نقلیه موتوری و موتور سیکلت','type'=>'INQUIRY','priority'=>'13','parent_id'=>'34'
            ],
            [
                'title'=>'فعالیت های خدماتی مربوط به تامین جا و غذا','type'=>'INQUIRY','priority'=>'14','parent_id'=>'34'
            ],
            [
                'title'=>'فعالیت‌های  املاک و مستغلات','type'=>'INQUIRY','priority'=>'15','parent_id'=>'34'
            ],
            [
                'title'=>'فعالیت‌های اداری و خدمات پشتیبانی','type'=>'INQUIRY','priority'=>'16','parent_id'=>'34'
            ],
            [
                'title'=>'فعالیت‌های حرفه‌ای، علمی و فنی','type'=>'INQUIRY','priority'=>'17','parent_id'=>'34'
            ],
            [
                'title'=>'فعالیت‌‌های مالی و بیمه','type'=>'INQUIRY','priority'=>'18','parent_id'=>'34'
            ],
            [
                'title'=>'فعالیت‌های مربوط به سلامت انسان و مددکاری اجتماعی','type'=>'INQUIRY','priority'=>'19','parent_id'=>'34'
            ],
            [
                'title'=>'فن آوری اطلاعات','type'=>'INQUIRY','priority'=>'20','parent_id'=>'34'
            ],
            [
                'title'=>'کشاورزی، جنگلداری و ماهیگیری','type'=>'INQUIRY','priority'=>'21','parent_id'=>'34'
            ],
            [
                'title'=>'هنر، سرگرمی و تفریح','type'=>'INQUIRY','priority'=>'22','parent_id'=>'34'
            ],
            [
                'title'=>'مسکونی','type'=>'AUCTION','priority'=>'1','parent_id'=>'35'
            ],
            [
                'title'=>'تجاری - اداری','type'=>'AUCTION','priority'=>'2','parent_id'=>'35'
            ],
            [
                'title'=>'زمین ','type'=>'AUCTION','priority'=>'3','parent_id'=>'35'
            ],
            [
                'title'=>' کشاورزی، دامپروری، شیلات و کشتارگاه','type'=>'AUCTION','priority'=>'4','parent_id'=>'35'
            ],
            [
                'title'=>' کارگاه،کارخانه، تولیدی - صنعتی','type'=>'AUCTION','priority'=>'5','parent_id'=>'35'
            ],
            [
                'title'=>'پارکینگ، امکان عمومی  و خدماتی','type'=>'AUCTION','priority'=>'6','parent_id'=>'35'
            ],
            [
                'title'=>'بازار ،بازارچه،غرفه و بوفه','type'=>'AUCTION','priority'=>'7','parent_id'=>'35'
            ],
            [
                'title'=>'فرهنگی،آموزشی، ورزشی، تفریحی ،پذیرایی','type'=>'AUCTION','priority'=>'8','parent_id'=>'35'
            ],
            [
                'title'=>'درمانی و بهداشتی','type'=>'AUCTION','priority'=>'9','parent_id'=>'35'
            ],
            [
                'title'=>'انبار و سیلو و سوله','type'=>'AUCTION','priority'=>'10','parent_id'=>'35'
            ],
            [
                'title'=>'مصالح ساختمانی','type'=>'AUCTION','priority'=>'1','parent_id'=>'36'
            ],
            [
                'title'=>'اقلام پیش ساخته(سقف کاذب و ساندویچ پانل)','type'=>'AUCTION','priority'=>'2','parent_id'=>'36'
            ],
            [
                'title'=>'قطعات بتنی','type'=>'AUCTION','priority'=>'3','parent_id'=>'36'
            ],
            [
                'title'=>'اسانسور،پله برقی و تجهیزات جانبی','type'=>'AUCTION','priority'=>'4','parent_id'=>'36'
            ],
            [
                'title'=>'سیستم های گرمایشی، حرارتی، شوفاژ، هیتر، مشعل','type'=>'AUCTION','priority'=>'5','parent_id'=>'36'
            ],
            [
                'title'=>'سیستم های سرمایشی، تهویه، سردخانه، هواساز و کولر','type'=>'AUCTION','priority'=>'6','parent_id'=>'36'
            ],
            [
                'title'=>' ماشین آلات ساخت و تولید ( ماشین افزار ، ریخته گری و ..)','type'=>'AUCTION','priority'=>'1','parent_id'=>'37'
            ],
            [
                'title'=>' جرثقیل، بالابر و لیفتراک','type'=>'AUCTION','priority'=>'2','parent_id'=>'37'
            ],
            [
                'title'=>' ماشین آلات ساختمانی، راهسازی، حفاری و معدن','type'=>'AUCTION','priority'=>'3','parent_id'=>'37'
            ],
            [
                'title'=>' ماشین آلات خدمات شهری، نظافتی و بازیافت','type'=>'AUCTION','priority'=>'4','parent_id'=>'37'
            ],
            [
                'title'=>' ماشین آلات صنایع غذایی،کشاورزی و دامپروری','type'=>'AUCTION','priority'=>'5','parent_id'=>'37'
            ],
            [
                'title'=>' ماشین آلات صنایع چوبی، نساجی، چاپ و بسته بندی','type'=>'AUCTION','priority'=>'6','parent_id'=>'37'
            ],
            [
                'title'=>' تجهیزات توزین ، انتقال و اتصال','type'=>'AUCTION','priority'=>'7','parent_id'=>'37'
            ],
            [
                'title'=>' ابزارر الات کارگاهی ، یراق  و قطعات صنعتی','type'=>'AUCTION','priority'=>'8','parent_id'=>'37'
            ],
            [
                'title'=>'لوله و اتصالات و شیرآلات','type'=>'AUCTION','priority'=>'1','parent_id'=>'38'
            ],
            [
                'title'=>'اقلام و موارد گروه آب و فاضلاب','type'=>'AUCTION','priority'=>'2','parent_id'=>'38'
            ],
            [
                'title'=>'اقلام و موارد گروه نفت، گاز و پتروشیمی','type'=>'AUCTION','priority'=>'3','parent_id'=>'38'
            ],
            [
                'title'=>'سیم و کابل برقی','type'=>'AUCTION','priority'=>'1','parent_id'=>'39'
            ],
            [
                'title'=>'تابلو برق و ترانسفورماتور، کنتور برق','type'=>'AUCTION','priority'=>'2','parent_id'=>'39'
            ],
            [
                'title'=>'مولدهای برق-انواع ژنراتور و الکتروموتور و پمپ','type'=>'AUCTION','priority'=>'3','parent_id'=>'39'
            ],
            [
                'title'=>'لوازم روشنایی و کلیدهای جریان','type'=>'AUCTION','priority'=>'4','parent_id'=>'39'
            ],
            [
                'title'=>'باطری، منابع تغذیه و UPS','type'=>'AUCTION','priority'=>'5','parent_id'=>'39'
            ],
            [
                'title'=>'اقلام و مواد گروه برق و نیروگاه','type'=>'AUCTION','priority'=>'6','parent_id'=>'39'
            ],
            [
                'title'=>'اقلام کامپیوتری و ماشین های اداری','type'=>'AUCTION','priority'=>'1','parent_id'=>'40'
            ],
            [
                'title'=>'اقلام و تجهیزات بانکی و فروشگاهی','type'=>'AUCTION','priority'=>'2','parent_id'=>'40'
            ],
            [
                'title'=>'اقلام و تجهیزات سمعی و بصری','type'=>'AUCTION','priority'=>'3','parent_id'=>'40'
            ],
            [
                'title'=>'سیم و کابل مخابرات','type'=>'AUCTION','priority'=>'1','parent_id'=>'41'
            ],
            [
                'title'=>'تجهیزات رادیویی،استودیو، ماهواره','type'=>'AUCTION','priority'=>'2','parent_id'=>'41'
            ],
            [
                'title'=>'تابلوهای دیجیتالی و تلوزیون های شهری','type'=>'AUCTION','priority'=>'3','parent_id'=>'41'
            ],
            [
                'title'=>'شماره تلفن و سیم کارت','type'=>'AUCTION','priority'=>'4','parent_id'=>'41'
            ],
            [
                'title'=>'انواع گوشی تلفن','type'=>'AUCTION','priority'=>'5','parent_id'=>'41'
            ],
            [
                'title'=>'اقلام و موارد گروه مخابرات و الکترونیک','type'=>'AUCTION','priority'=>'6','parent_id'=>'41'
            ],
            [
                'title'=>'اقلام و موارد گروه پزشکی و بیمارستانی','type'=>'AUCTION','priority'=>'1','parent_id'=>'42'
            ],
            [
                'title'=>'اقلام و موارد گروه آزمایشی','type'=>'AUCTION','priority'=>'2','parent_id'=>'42'
            ],
            [
                'title'=>'اقلام و موارد گروه آرایشی و بهداشتی','type'=>'AUCTION','priority'=>'3','parent_id'=>'42'
            ],
            [
                'title'=>'پوشاک و البسه','type'=>'AUCTION','priority'=>'1','parent_id'=>'43'
            ],
            [
                'title'=>'منسوجات، پارچه و پرده و..','type'=>'AUCTION','priority'=>'2','parent_id'=>'43'
            ],
            [
                'title'=>'فرش و سجاده و موکت و..','type'=>'AUCTION','priority'=>'3','parent_id'=>'43'
            ],
            [
                'title'=>'کالای خواب(تشک، لحاف،پتو،بالشت و..)','type'=>'AUCTION','priority'=>'4','parent_id'=>'43'
            ],
            [
                'title'=>'کیف ، کفش، پوتین و کالاهای چرمی','type'=>'AUCTION','priority'=>'5','parent_id'=>'43'
            ],
            [
                'title'=>'اقلام چوبی','type'=>'AUCTION','priority'=>'1','parent_id'=>'44'
            ],
            [
                'title'=>'خدمات چاپ، نشر و تایپ و تکثیر','type'=>'AUCTION','priority'=>'2','parent_id'=>'44'
            ],
            [
                'title'=>'خدمات تبلیغات، بازاریابی و تابلوهای تبلیغات','type'=>'AUCTION','priority'=>'3','parent_id'=>'44'
            ],
            [
                'title'=>'جمع آوری و بازیافت زباله','type'=>'AUCTION','priority'=>'4','parent_id'=>'45'
            ],
            [
                'title'=>'خدمات شهری و شهرداری','type'=>'AUCTION','priority'=>'5','parent_id'=>'45'
            ],
            [
                'title'=>'واگذاری امکان بازیافت و تفکیک','type'=>'AUCTION','priority'=>'6','parent_id'=>'45'
            ],
            [
                'title'=>'اموال اسقاطی و مازاد','type'=>'AUCTION','priority'=>'7','parent_id'=>'45'
            ],
            [
                'title'=>'خودروهای سبک','type'=>'AUCTION','priority'=>'1','parent_id'=>'46'
            ],
            [
                'title'=>'خودرو های سنگین و نیمه سنگین','type'=>'AUCTION','priority'=>'2','parent_id'=>'46'
            ],
            [
                'title'=>'موتور سیکلت و دوچرخه ','type'=>'AUCTION','priority'=>'3','parent_id'=>'46'
            ],
            [
                'title'=>'خودرو های امدادی(آمبولانس و آتش نشانی)','type'=>'AUCTION','priority'=>'4','parent_id'=>'46'
            ],
            [
                'title'=>'تجهیزات، قطعات و لوازم یدکی انواع وسایل نقلیه','type'=>'AUCTION','priority'=>'5','parent_id'=>'46'
            ],
            [
                'title'=>'اقلام و موارد فلزی(آهن،فولاد،چدن،آلومینیوم و استیل)','type'=>'AUCTION','priority'=>'1','parent_id'=>'47'
            ],
            [
                'title'=>'سوله، داربست،غرفه، سازه و استراکچر فلزی','type'=>'AUCTION','priority'=>'2','parent_id'=>'47'
            ],
            [
                'title'=>'مخازن فلزی تحت فشار، میعانات و منبع های هوایی','type'=>'AUCTION','priority'=>'3','parent_id'=>'47'
            ],
            [
                'title'=>'کانکس، کانتینر،کیوسک و کاروان','type'=>'AUCTION','priority'=>'4','parent_id'=>'47'
            ],
            [
                'title'=>'ضایعات فلزی','type'=>'AUCTION','priority'=>'5','parent_id'=>'47'
            ],
            [
                'title'=>'سیستم های ایمنی،حفاظت الکترونیک و کنترل تردد','type'=>'AUCTION','priority'=>'1','parent_id'=>'48'
            ],
            [
                'title'=>'سیستم های ایمنی آتش نشانی و اطفائ حریق','type'=>'AUCTION','priority'=>'2','parent_id'=>'48'
            ],
            [
                'title'=>'تجهیزات آتش نشانی و اطفاو حریق','type'=>'AUCTION','priority'=>'3','parent_id'=>'48'
            ],
            [
                'title'=>'درخت، نهال، گل و گیاه','type'=>'AUCTION','priority'=>'1','parent_id'=>'49'
            ],
            [
                'title'=>'سموم و کود','type'=>'AUCTION','priority'=>'2','parent_id'=>'49'
            ],
            [
                'title'=>'خوراک دام و طیور ','type'=>'AUCTION','priority'=>'3','parent_id'=>'49'
            ],
            [
                'title'=>'دام و طیور و شیلات','type'=>'AUCTION','priority'=>'4','parent_id'=>'49'
            ],
            [
                'title'=>' میوه، سبزی و صیفی جات','type'=>'AUCTION','priority'=>'1','parent_id'=>'50'
            ],
            [
                'title'=>' برنج ایرانی و خارجی','type'=>'AUCTION','priority'=>'2','parent_id'=>'50'
            ],
            [
                'title'=>' مواد پروتئيني (گوشت و تخم مرغ و...)','type'=>'AUCTION','priority'=>'3','parent_id'=>'50'
            ],
            [
                'title'=>' لبنیات و فرآورده های لبنی','type'=>'AUCTION','priority'=>'4','parent_id'=>'50'
            ],
            [
                'title'=> 'حبوبات و خشکبار','type'=>'AUCTION','priority'=>'5','parent_id'=>'50'
            ],
            [
                'title'=>'نمک ، قند و شکر، ادویه، چای و مربا ','type'=>'AUCTION','priority'=>'6','parent_id'=>'50'
            ],
            [
                'title'=>'انواع کنسرو','type'=>'AUCTION','priority'=>'7','parent_id'=>'50'
            ],
            [
                'title'=>' سایر مواد غذایی','type'=>'AUCTION','priority'=>'8','parent_id'=>'50'
            ],
            [
                'title'=>'مبلمان اداری','type'=>'AUCTION','priority'=>'1','parent_id'=>'51'
            ],
            [
                'title'=>'تجهیزات و لوازم اداری، آموزشی ','type'=>'AUCTION','priority'=>'2','parent_id'=>'51'
            ],
            [
                'title'=>'لوازم خانگی، آشپزخانه و هتلینگ','type'=>'AUCTION','priority'=>'3','parent_id'=>'51'
            ],
            [
                'title'=>'لوازم ورزشی،تفریحی و بازی','type'=>'AUCTION','priority'=>'4','parent_id'=>'51'
            ],
            [
                'title'=>'مواد شیمیایی، گازهای صنعتی و طبی','type'=>'AUCTION','priority'=>'1','parent_id'=>'52'
            ],
            [
                'title'=>'اقلام پلاستیکی ، ظروف یکبارمصرف، دبه، گالن ، مخزن و..','type'=>'AUCTION','priority'=>'2','parent_id'=>'52'
            ],
            [
                'title'=>'روغن، روان کننده، گریس و ضدجوش، ضدیخ','type'=>'AUCTION','priority'=>'3','parent_id'=>'52'
            ],
            [
                'title'=>'انواع عایق و ایزوگام','type'=>'AUCTION','priority'=>'4','parent_id'=>'52'
            ],
            [
                'title'=>'اقلام لاستیکی تایر، تیوپ،SBR و..','type'=>'AUCTION','priority'=>'5','parent_id'=>'52'
            ],
            [
                'title'=>'رنگ و رزین و چسب','type'=>'AUCTION','priority'=>'6','parent_id'=>'52'
            ],
            [
                'title'=>'مواد شوینده و پاک کننده','type'=>'AUCTION','priority'=>'7','parent_id'=>'52'
            ],
            [
                'title'=>'اقلام پلیمری، نایلون، فایبرگلاس، نایلکس، پلی اتیلن، ژئوممبران و ..','type'=>'AUCTION','priority'=>'8','parent_id'=>'52'
            ],
            [
                'title'=>'سایر فراورده های نفت، پتروشیمی، ','type'=>'AUCTION','priority'=>'9','parent_id'=>'52'
            ],
            [
                'title'=>'واگذاری و بهره برداری از معادن','type'=>'AUCTION','priority'=>'1','parent_id'=>'53'
            ],
            [
                'title'=>'مواد و فراورده های معدنی و نسوز','type'=>'AUCTION','priority'=>'2','parent_id'=>'53'
            ],
            [
                'title'=>'اقلام و مواد گرانبها(طلا،جواهر و..)','type'=>'AUCTION','priority'=>'3','parent_id'=>'53'
            ],
            [
                'title'=>'شیشه و فراورده های آن','type'=>'AUCTION','priority'=>'4','parent_id'=>'53'
            ],
            [
                'title'=>'فروش و اجاره تجهیزات دریایی','type'=>'AUCTION','priority'=>'1','parent_id'=>'54'
            ],
            [
                'title'=>'فروش و اجاره تجهیزات هوایی','type'=>'AUCTION','priority'=>'2','parent_id'=>'54'
            ],
            [
                'title'=>'سهام و حق السهم','type'=>'AUCTION','priority'=>'1','parent_id'=>'55'
            ],
            [
                'title'=>'پروانه و نمایندگی','type'=>'AUCTION','priority'=>'2','parent_id'=>'55'
            ],
            [
                'title'=>'حق امتیاز','type'=>'AUCTION','priority'=>'3','parent_id'=>'55'
            ],
            [
                'title'=>'مشاوره کلیه صنوف','type'=>'TENDER','priority'=>'1','parent_id'=>'25'
            ],

        ];
        foreach ($workGroups as $workGroup) {
            WorkGroup::create($workGroup);
        }
    }
}
