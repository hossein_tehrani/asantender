<?php

namespace Database\Seeders;

use App\Models\Plan;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            [
                'title' => 'پلن طلایی',
                'priority' => 1,
                'price' => 1000,
                'status' => 1,
                'off_percent' => 50,
                'work_group_limit' => 100,
                'work_group_change_limit' => 4,
                'count' => 1000,
                'time' => 365,
            ],
            [
                'title' => 'پلن نقره ای',
                'priority' => 2,
                'price' => 500,
                'status' => 1,
                'off_percent' => 40,
                'work_group_limit' => 100,
                'work_group_change_limit' => 3,
                'count' => 500,
                'time' => 200,
            ],
            [
                'title' => 'پلن برنزی',
                'priority' => 3,
                'price' => 250,
                'status' => 0,
                'off_percent' => 25,
                'work_group_limit' => 100,
                'work_group_change_limit' => 1,
                'count' => 100,
                'time' => 100,
            ]
        ];
        foreach ($plans as $plan) {
             Plan::create($plan);
        }
    }
}
