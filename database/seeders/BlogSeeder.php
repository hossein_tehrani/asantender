<?php

namespace Database\Seeders;

use App\Models\Blog;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blogs=[
            [
                'title'=> 'مناقصه چیست ؟‌',
                'keywords'=> 'مناقصه مزایده اتسعلام',
                'priority'=> '1',
                'meta'=> '<meta charset="utf-8">',
                'description'=> 'about tender',
                'body'=> 'مناقصه مناقصهمناقصه مناقصهمناقصه مناقصه مناقصه مناقصه مناقصه مناقصهمناقصه مناقصه',
                'admin_id'=> '1',
            ],
            [
                'title'=> 'استعلام چیست ؟‌',
                'keywords'=> 'استعلام استعلام اتسعلام',
                'priority'=> '1',
                'meta'=> '<meta charset="utf-8">',
                'description'=> 'about tender',
                'body'=> 'استعلام استعلام استعلام استعلام استعلام استعلام استعلام استعلام استعلام',
                'admin_id'=> '1',
            ],
            [
                'title'=> 'مزایده چیست ؟‌',
                'keywords'=> 'مناقصه مزایده اتسعلام',
                'priority'=> '1',
                'meta'=> '<meta charset="utf-8">',
                'description'=> 'about tender',
                'body'=> 'مزایده مزایده مزایده مزایده مزایده مزایده استعلام مزایده مزایده',
                'admin_id'=> '1',
            ],
        ];
        foreach ($blogs as $blog){
            Blog::create($blog);
        }
    }
}
